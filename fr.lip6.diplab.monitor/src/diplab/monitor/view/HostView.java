package diplab.monitor.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import diplab.core.experiment.reporting.logevent.LogStartCallEvent;
import diplab.monitor.model.CallStack;
import diplab.monitor.model.Host;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;

public class HostView extends Parent {
	
	private final double radius = 25.0;
	
	private Ellipse ellipse = new Ellipse(radius, radius);
	
	private final Host host;
	
	private final HBox root = new HBox(10.0);
	
	public HostView(Host host) {
		this.host=host;
		
		getChildren().add(root);
				
		makeCirclePart();
		makeStatePart();
		makeCallStakPart();
		
	}
	
	
	private List<Text> freetextpool = new ArrayList<>();
	private Map<CallStack,Text> affectedtext = new HashMap<>();
	private synchronized Text bindText(CallStack c) {
		if(freetextpool.isEmpty()){
			freetextpool.add(new Text());
		}
		Text t = freetextpool.remove(0);
		t.setVisible(true);
		affectedtext.put(c, t);
		callstacks.getChildren().add(t);
		c.isRunningProperty().addListener((obs,o,n)->{
			updateColorText(t, n.booleanValue());
		});
		c.valueProperty().addListener((obs,o,n)->{
			updateText(t,c);
		});
		updateColorText(t, true);
		return t;
	}
	
	private synchronized void releaseText(CallStack c) {
		Text t = affectedtext.remove(c);
		t.setVisible(false);
		freetextpool.add(t);
		callstacks.getChildren().remove(t);
	}
	private HBox callstacks = new HBox(10.0);
	
	private void makeCallStakPart() {
		//call stack part		
		
		host.getCallStacksProperty().addListener(new ListChangeListener<CallStack>() {
			@Override
			public void onChanged(Change<? extends CallStack> change) {
				synchronized(HostView.this) {
					while(change.next()) {
						if(change.wasAdded()) {
							for(CallStack c : change.getAddedSubList()) {
								Text t = bindText(c);
								updateText(t,c);
							}
						}
						if(change.wasRemoved()) {
							for(CallStack c : change.getRemoved()) {
								releaseText(c);
							}
						}
					}
					
				}
				
				
			}
		});
		
		//host.getCallStacksProperty().addListener((obs,o,n)->update(callstacks));
		root.getChildren().add(callstacks);
		
	}
	
	private void updateColorText(Text t,boolean val) {
		if(val) {
			t.setFill(Color.GREEN);
		}else {
			t.setFill(Color.RED);
		}
	}
	
	private void updateText(Text t, CallStack c) {
		StringBuilder sb = new StringBuilder();
		for(LogStartCallEvent e:c.value()) {
			sb.append(e.getNodeProtoName()+"."+e.getNameprimitive()+"(");
			for(Object arg : e.getArgs()) {
				sb.append(arg.toString());
			}
			sb.append(") \n");
		}
		t.setText(sb.toString());
	}
	
	
	
	
	private void makeStatePart() {
		// state part
		VBox states = new VBox(20.0);
		for(String nameproto : host.getProtoNames()) {
			HBox item = new HBox(5.0);
			
			item.getChildren().add(new Text(nameproto));
			
			Text valstate = new Text();
			//VBox valstate = new VBox(5.0);
			
			host.getCurStateProperty(nameproto).addListener((obs,o,n)->{
				StringBuilder sb = new StringBuilder();
				for(Entry<String, Object> e : n.entrySet()) {				
					sb.append(e.getKey()+" = "+e.getValue()+"\n");
				}
				valstate.setText(sb.toString());
			});
			
			
			item.getChildren().add(valstate);
			states.getChildren().add(item);
		}
		
		root.getChildren().add(states);
	}

	private void makeCirclePart() {
		//circle part	
		StackPane circle = new StackPane();
		host.getActive().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				synchronized(HostView.this) {
					if(newValue) {
						ellipse.setFill(Color.WHITE);
					}else {
						ellipse.setFill(Color.ORANGE);
					}
				}
			}
		});
		ellipse.setStroke(Color.BLACK);
		ellipse.setFill(Color.WHITE);
		Text namenode = new Text("N"+host.getId().getVal());
		circle.getChildren().addAll(ellipse,namenode);
		BorderPane border = new BorderPane();
		border.setTop(circle);
		root.getChildren().add(border);
		
	}

	
	
	
	public double getRadius() {
		return radius;
	}
	
}
