package diplab.monitor.view;

import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class Arrow extends Parent {

	public Arrow(double startX, double startY, double endX, double endY, Color color) {
		 double width=4.5;
		
		 // get the slope of the line and find its angle
	      double slope = (startY - endY) / (startX - endX);
	      double lineAngle = Math.atan(slope);

	      double arrowAngle = startX > endX ? Math.toRadians(45) : -Math.toRadians(225);

	      Line line = new Line(startX, startY, endX, endY);
	      line.setStroke(color);
	      line.setStrokeWidth(width);
	      
	      double lineLength = Math.sqrt(Math.pow(startX - endX, 2) + Math.pow(startY - endY, 2));
	      double arrowLength =  lineLength * 0.05;

	      // create the arrow legs
	      Line arrow1 = new Line();
	      arrow1.setStartX(line.getEndX());
	      arrow1.setStartY(line.getEndY());
	      arrow1.setEndX(line.getEndX() + arrowLength * Math.cos((lineAngle - arrowAngle)*1.0));
	      arrow1.setEndY(line.getEndY() + arrowLength * Math.sin((lineAngle - arrowAngle)*1.0));
	      arrow1.setStroke(color);
	      arrow1.setStrokeWidth(width);
	      
	      Line arrow2 = new Line();
	      arrow2.setStartX(line.getEndX());
	      arrow2.setStartY(line.getEndY());
	      arrow2.setEndX(line.getEndX() + arrowLength * Math.cos((lineAngle + arrowAngle)*1.0));
	      arrow2.setEndY(line.getEndY() + arrowLength * Math.sin((lineAngle + arrowAngle)*1.0));
	      arrow2.setStroke(color);
	      arrow2.setStrokeWidth(width);
	      
	      getChildren().addAll(line, arrow1, arrow2);
	}
	
}
