package diplab.monitor.view;

public final class Position {

	private final double x;
	private final double y;

	public Position(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	/**
	 * Compute the distance between two Positions
	 * @param other the other position
	 * @return the distance between two Positions
	 */
	public double distance(Position other) {
		double tmpX = other.x - x;
		double tmpY = other.y - y;
		double distance = Math.sqrt(tmpX * tmpX + tmpY * tmpY);
		return distance;
	}

	/**
	 * Calcul d'une nouvelle position à partir d'un module et d'un angle depuis la
	 * position courante.
	 * 
	 * @param module,
	 *            distance de puis la position cournate
	 * @param angle,
	 *            en radian, 0 indique le nord, pi/2 indique l'est, pi indique le
	 *            sud, 3pi/2 indique l'ouest
	 * @return une nouvelle position à partir de la position courante
	 */
	public Position getNewPositionWith(double module, double angle) {

		double new_x = Math.sin(angle) * module + x;
		double new_y = Math.cos(angle) * module + y;
		return new Position(new_x, new_y);
	}

	@Override
	public String toString() {
		return "( " + x + " , " + y + " )";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Position))
			return false;
		Position other = (Position) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}

	public Position bound(double minX, double minY, double maxX, double maxY) {
		double x_res = x;
		double y_res = y;
		if (x_res - minX < 0.0) {
			x_res = minX;
		}
		if (y_res - minY < 0.0) {
			y_res = minY;
		}
		if (x_res - maxX > 0.0) {
			x_res = maxX;
		}
		if (y_res - maxY > 0.0) {
			y_res = maxY;
		}
		return new Position(x_res, y_res);
	}

}
