package diplab.monitor.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import diplab.core.IDNode;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.Reports;
import diplab.core.experiment.reporting.logevent.LogCrashEvent;
import diplab.core.experiment.reporting.logevent.LogEndCallEvent;
import diplab.core.experiment.reporting.logevent.LogEndInterruptionCallEvent;
import diplab.core.experiment.reporting.logevent.LogEvent;
import diplab.core.experiment.reporting.logevent.LogMessageReceiveEvent;
import diplab.core.experiment.reporting.logevent.LogMessageSentEvent;
import diplab.core.experiment.reporting.logevent.LogRecoverEvent;
import diplab.core.experiment.reporting.logevent.LogStartCallEvent;
import diplab.core.experiment.reporting.logevent.LogStartInterruptionCallEvent;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;
import diplab.core.messaging.Message;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;

public class Simulation {

	private final Map<IDNode,Host> nodes;
	private final Experiment experiment;	
	private final ListProperty<Message<?>> network;
	private final List<LogEvent> execution;
	
	private final IntegerProperty nextstep = new SimpleIntegerProperty(0);//the nextstep to execute
	
	public Simulation(Map<IDNode,Report> reports) {
		
				
		Report all = Reports.merge(reports.values());
		this.experiment= all.getExperiment();
		execution=all.toList();	
		nodes = new HashMap<>();
		network = new SimpleListProperty<>(FXCollections.observableArrayList());
		
		for(int i = 0 ; i < experiment.getNbNode() ; i++) {
			IDNode id = IDNode.get(i);
						
			List<String> nameprotos = new ArrayList<>();
			for(String protoname : experiment.getNodeProtoClasses().keySet()) {
				if(!experiment.getDeploymentmap().containsKey(protoname) 
						|| experiment.getDeploymentmap().get(protoname).contains(id) ) {
					nameprotos.add(protoname);
				}
			}
			Host host = new Host(id,nameprotos, reports.get(id).toList());
			nodes.put(id, host);
		}
		
		
	}
	
	public List<Host> getHosts(){
		List<Host> res = new ArrayList<>(nodes.values());
		res.sort((h1,h2) -> Long.compare(h1.getId().getVal(), h2.getId().getVal()));
		return res;
	}
		
	public Host getHost(IDNode id) {
		return nodes.get(id);
	}
	
	public List<LogEvent> getListLogEvent(){
		return execution;
	}
	
	public ReadOnlyIntegerProperty getNextstep() {
		return nextstep;
	}
	
	public List<LogEvent> getExecution(){
		return Collections.unmodifiableList(execution);
	}
	
	public ReadOnlyListProperty<Message<?>> getNetwork(){
		return network;
	}
	
	public void forward(){
		forward(1);
	}
	
	public void backward(){
		backward(1);
	}
	
	public void forward(int time) {
		goToStep(nextstep.get()+time);
	}
	
	public void backward(int time) {
		goToStep(nextstep.get()-time);
	}
	
	
	public void goToStep(int index) {
		int i=index;
		if(index <0) i=0;
		if(i > execution.size()) i = execution.size();
		if(i == nextstep.get()) return;
		if(i > nextstep.get()) {//go ahead to the future
			while(i!=nextstep.get()) {
				apply(execution.get(nextstep.get()));
				nextstep.set(nextstep.get()+1);
			}
		}else if(i < nextstep.get()) {// go back to the past
			while(i!=nextstep.get()) {
				nextstep.set(nextstep.get()-1);
				cancel(execution.get(nextstep.get()));	
			}
		}		
	}

	private LogEventVisitor forward = new LogEventVisitor() {
			
		public void visit(LogCrashEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).off();
		}
		public void visit(LogRecoverEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).on();			
		}
		
		public void visit(LogStartCallEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).apply(e);
		}
		public void visit(LogEndCallEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).apply(e);			
		}
		
		public void visit(LogStartInterruptionCallEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).apply(e);			
		}
		public void visit(LogEndInterruptionCallEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).apply(e);
		}
		
		public void visit(LogMessageSentEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).assertActive();
			network.add(e.getMessage());
		}
		public void visit(LogMessageReceiveEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).assertActive();
			network.remove(e.getMessage());	
		}
	};
	
	private LogEventVisitor backward = new LogEventVisitor() {
		public void visit(LogCrashEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).on();			
		}
		public void visit(LogRecoverEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).off();			
		}
		
		public void visit(LogStartCallEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).cancel(e);		
		}
		public void visit(LogEndCallEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).cancel(e);
		}
		
		public void visit(LogStartInterruptionCallEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).cancel(e);
		}
		public void visit(LogEndInterruptionCallEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).cancel(e);
		}
		
		public void visit(LogMessageSentEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).assertActive();
			network.remove(e.getMessage());
		}
		public void visit(LogMessageReceiveEvent e) {
			IDNode id = e.getID().getNode();
			nodes.get(id).assertActive();
			network.add(e.getMessage());
		}
	};
	
	
	private void cancel(LogEvent logEvent) {
		logEvent.accept(backward);		
	}

	private void apply(LogEvent logEvent) {
		logEvent.accept(forward);
	}
	
	public int getSize() {
		return execution.size();
	}
}
