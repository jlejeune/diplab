package diplab.monitor.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import diplab.core.IDNode;
import diplab.core.experiment.reporting.logevent.LogCallEvent;
import diplab.core.experiment.reporting.logevent.LogEndCallEvent;
import diplab.core.experiment.reporting.logevent.LogEndInterruptionCallEvent;
import diplab.core.experiment.reporting.logevent.LogEvent;
import diplab.core.experiment.reporting.logevent.LogStartCallEvent;
import diplab.core.experiment.reporting.logevent.LogStartInterruptionCallEvent;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyListProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;

public class Host {

	private final IDNode id;
	private BooleanProperty active= new SimpleBooleanProperty(true);
	
	private final ListProperty<CallStack> callstacks = new SimpleListProperty<>(FXCollections.observableList(new LinkedList<>()));
	
	private final Map<LogEndCallEvent,LogStartCallEvent> calleventmatching = new HashMap<>();
	private final Map<LogEndInterruptionCallEvent,LogStartInterruptionCallEvent> intercalleventmatching = new HashMap<>(); 
	private Map<String,ObjectProperty<Map<String,Object>>> curstates = new HashMap<>();
	
	private final List<LogEvent> myexecution;
	
	public Host(IDNode id, List<String> protos, List<LogEvent> myexecution) {
		this.id=id;
		this.myexecution=myexecution;
		for(String s : protos) {
			curstates.put(s,new SimpleObjectProperty<>(Collections.emptyMap()));		
		}
	}

	public Set<String> getProtoNames(){
		return curstates.keySet();
	}

	public IDNode getId() {
		return id;
	}
		
	public Map<String,Object> getCurState(String nameproto){
		return curstates.get(nameproto).getValue();
	}
	
	public ReadOnlyObjectProperty<Map<String,Object>> getCurStateProperty(String nameproto){
		return curstates.get(nameproto);
	}
	
	public CallStack getCurCallStack() {
		for(CallStack c : callstacks) {
			if(c.isRunning()) return c;
		}	
		return null;
	}
	
	public List<CallStack> getCallStacks(){
		return Collections.unmodifiableList(callstacks);
	}
	
	public ReadOnlyListProperty<CallStack> getCallStacksProperty(){
		return callstacks;
	}
		
	public void on() {
		active.set(true);;
	}
	
	public void off() {
		active.set(false);
	}

	
	public boolean isActive() {
		return active.get();
	}

	public ReadOnlyBooleanProperty getActive() {
		return active;
	}
	
	
	public void assertActive() {
		if(!active.get()) throw new IllegalStateException("try to apply an NodeProtoevent but the node is not active");		
	}
	
	public void apply(LogStartCallEvent e) {
		assertActive();
		CallStack cur = getCurCallStack();
		if(cur == null) {
			cur = new CallStack();
			callstacks.add(cur);
		}
		cur.push(e);
		newStateOf(e);
	}

	private void newStateOf(LogCallEvent e) {
		Map<String,Object> state = e.getState();
		if(state == null) {
			state=Collections.emptyMap();
		}
		curstates.get(e.getNodeProtoName()).set(state);
	}
	
	private void retrieveStateBefore(LogCallEvent e) {

		int i=myexecution.indexOf(e);
		if(i<0) throw new IllegalStateException("a given LogCallEvent is not in the execution trace");
		i--;
		Map<String,Object> state = null;
		while(i>=0) {
			LogEvent event = myexecution.get(i);
			if(event instanceof LogCallEvent lce) {
				if(lce.getNodeProtoName().equals(e.getNodeProtoName())) {
					state = lce.getState();
					break;
				}
			}
			i--;
		}
		if(state == null) {
			state=Collections.emptyMap();
		}
		curstates.get(e.getNodeProtoName()).set(state);
	}
	


	void apply(LogEndCallEvent e) {
		assertActive();
		CallStack cur = getCurCallStack();
		if(cur == null) {
			throw new IllegalStateException("try to apply a LogEndCallEvent but the current call stack does not exist");
		}
		LogStartCallEvent startcallevent= cur.pop(Optional.of(e));
		if(startcallevent == null || ! startcallevent.matchWith(e) ) throw new IllegalStateException("Poping a LogStartCallEvent which does not match with an application of a LogEndCallEvent");
		if(!calleventmatching.containsKey(e))calleventmatching.put(e, startcallevent);
		if(cur.isEmpty()) callstacks.remove(cur);
		
		newStateOf(e);
	}


	

	void apply(LogEndInterruptionCallEvent e) {
		assertActive();
		CallStack cur = getCurCallStack();
		if(cur != null) {
			throw new IllegalStateException("try to apply a LogEndInterruptionCallEvent but the current call exists");
		}
		CallStack callstack=null;
		LogStartInterruptionCallEvent intstart=null;
		for(CallStack s : new ArrayList<>(callstacks)) {
			intstart = s.getInterruption();
			if(intstart == null) throw new IllegalStateException("Found a interrupted callStack with no LogStartInterruptionCallEvent");
			if(intstart.matchWith(e)) {
				callstack=s;
				break;
			}
		}
		if(callstack == null) throw new IllegalStateException();
		if(!intercalleventmatching.containsKey(e)) intercalleventmatching.put(e, intstart);
		callstack.running();		
		newStateOf(e);
		
	}

	
	

	public void cancel(LogStartCallEvent e) {
		assertActive();
		CallStack cur = getCurCallStack();
		if(cur == null) {
			throw new IllegalStateException("try to cancel a LogStartCallEvent but the current call stack does not exist");
		}
		LogStartCallEvent startcallevent= cur.pop(Optional.empty());
		if(! e.equals(startcallevent)) throw new IllegalStateException("Try to cancel a LogStartCallEvent but the last stacked LogStartCallEvent is different");
		if(cur.isEmpty()) callstacks.remove(cur);
		
		retrieveStateBefore(e);
	}


	public void cancel(LogEndCallEvent e) {
		assertActive();
		CallStack cur = getCurCallStack();
		if(cur == null) {
			cur = new CallStack();
			callstacks.add(cur);
		}
		LogStartCallEvent startcallevent = calleventmatching.get(e);
		if(startcallevent==null || !startcallevent.matchWith(e)) throw new IllegalStateException("matching problem between a canceling LogEndCallEvent and its supposed LogStartCallEvent");
		cur.push(startcallevent);
		retrieveStateBefore(e);
	}

	
	public void apply(LogStartInterruptionCallEvent e) {
		assertActive();
		CallStack cur = getCurCallStack();
		if(cur == null) {
			throw new IllegalStateException("try to apply a LogStartInterruptionCallEvent but the current call stack does not exist");
		}
		LogStartCallEvent startcallevent=cur.top();
		if(startcallevent == null || ! startcallevent.matchWith(e) ) throw new IllegalStateException("try to apply a  LogStartInterruptionCallEvent which does not match with the last added LogStartCallEvent");
		cur.interrupt(e);
		
		newStateOf(e);
	}
	
	public void cancel(LogStartInterruptionCallEvent e) {
		assertActive();
		CallStack cur = getCurCallStack();
		if(cur != null) {
			throw new IllegalStateException("try tocancel a LogStartInterruptionCallEvent but the current call stack exists");
		}
		CallStack callstack=null;
		for(CallStack s : new ArrayList<>(callstacks)) {
			LogStartInterruptionCallEvent intstart = s.getInterruption();
			if(intstart.equals(e)) {
				callstack=s;
				break;
			}
		}
		if(callstack==null) throw new IllegalStateException("Cannot found the corresponding interupted callstack when canceling a LogStartInterruptionCallEvent");
		
		callstack.running();			
		retrieveStateBefore(e);
	}


	public void cancel(LogEndInterruptionCallEvent e) {
		assertActive();
		CallStack cur = getCurCallStack();
		if(cur == null) {
			throw new IllegalStateException("try to cancel a LogEndInterruptionCallEvent but the current call stack does not exist");
		}
		LogStartInterruptionCallEvent instart = intercalleventmatching.get(e);
		if(instart==null || !instart.matchWith(e)) throw new IllegalStateException("matching problem between a canceling LogEndInterruptionCallEvent and its supposed LogStartInterruptionCallEvent");
		
		cur.interrupt(instart);		
		retrieveStateBefore(e);
	}
	
}
