package diplab.monitor.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import diplab.core.experiment.reporting.logevent.LogEndCallEvent;
import diplab.core.experiment.reporting.logevent.LogStartCallEvent;
import diplab.core.experiment.reporting.logevent.LogStartInterruptionCallEvent;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;

public class CallStack {

	private final ListProperty<LogStartCallEvent> stack = new SimpleListProperty<>(FXCollections.observableList(new LinkedList<>()));
	private Optional<LogStartInterruptionCallEvent>  interuption=Optional.empty();
	private BooleanProperty running = new SimpleBooleanProperty(true);
	//private Object lastreturned = null;
	
	
	public void push(LogStartCallEvent e) {
		//lastreturned = null;
		stack.add(0,e);
	}

	public LogStartCallEvent pop(Optional<LogEndCallEvent> e) {
		if(stack.isEmpty()) throw new IllegalStateException("try to pop a empty CallStack");
//		if(e.isEmpty()) {
//			lastreturned=null;
//		}else {
//			lastreturned=e.get().getResult();
//		}
		
		return stack.remove(0);
	}

	public LogStartCallEvent top() {
		if(stack.isEmpty()) throw new IllegalStateException("try to top a empty CallStack");
		return stack.get(0);
	}

	public ReadOnlyBooleanProperty isRunningProperty() {
		return running;
	}
	
	public void interrupt(LogStartInterruptionCallEvent e) {
		if(interuption.isPresent()) throw new IllegalStateException("declare an interuption but it already exists");
		this.interuption=Optional.of(e);
		running.set(false);
	}

	public LogStartInterruptionCallEvent getInterruption() {
		return interuption.orElse(null);
	}

	public boolean isEmpty() {
		return stack.isEmpty();
	}
	
	public int size() {
		return stack.size();
	}

	
	public ListProperty<LogStartCallEvent> valueProperty(){
		return stack;
	}
	
	public List<LogStartCallEvent> value(){
		if(stack.isEmpty()) return Collections.emptyList();
		return Collections.unmodifiableList(stack);
	}
	
	public boolean isRunning() {
		return running.get();
	}
	
	public void running() {
		running.set(true);
		interuption=Optional.empty();	
	}

}
