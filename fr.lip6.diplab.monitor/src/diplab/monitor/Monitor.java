package diplab.monitor;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import diplab.core.IDNode;
import diplab.core.experiment.reporting.Report;
import diplab.core.messaging.Message;
import diplab.monitor.model.Host;
import diplab.monitor.model.Simulation;
import diplab.monitor.view.Arrow;
import diplab.monitor.view.HostView;
import diplab.monitor.view.Position;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Pair;

public class Monitor extends Application {
	
	private Map<IDNode,Report> reports;
	private  Simulation simu;
	private  Group root;
	private  Scene scene;
	private final Map<IDNode,HostView> mappinghost = new HashMap<>();
	private final Map<String,Color> messagecolor = new HashMap<>();
	
	
	private static List<Color> colors = Arrays.asList(
			Color.BLUE,
			Color.RED,
			Color.GREEN,
			Color.MEDIUMPURPLE,
			Color.YELLOWGREEN,
			Color.CHOCOLATE,
			Color.CHARTREUSE,
			Color.BLUEVIOLET,
			Color.CADETBLUE,
			Color.DARKSALMON,
			Color.LIGHTSEAGREEN,
			Color.TURQUOISE
			);
	private static int i=0;
	private static Color getNewColor() {
		int tmp=i;
		i=(i+1)%colors.size();
		return colors.get(tmp);
		
	}	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public void start(Stage primaryStage) throws Exception {
		List<String> args =  this.getParameters().getRaw();
		if(args.size() < 1) {
			System.err.println("No report file specified");
			System.exit(1);
		}
		String name_file = args.get(0);
		
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(name_file))){
			 reports  = (Map<IDNode,Report>) ois.readObject();
		}	
		simu = new Simulation(reports);
		root = new Group();
				
		Rectangle2D screenBounds = Screen.getPrimary().getBounds();
		scene = new Scene(root, screenBounds.getWidth(), screenBounds.getHeight(), Color.WHITE);
		scene.setOnKeyTyped(e->{
			switch(e.getCode()) {
			case RIGHT:
				simu.forward();
				break;
			case LEFT:
				simu.backward();
				break;
			default:
				break;
			}
		});
		
		scene.setOnMouseClicked(e->{
			switch(e.getButton()) {
			case PRIMARY:
				simu.forward();
				break;
			case SECONDARY:
				simu.backward();
				break;
			default:
				break;
			}
			
		});
		primaryStage.setScene(scene);
		
		makeNodesGroup();
		makeNetworkGroup();
		makeLogsGroup();
		makeStepcounter();
		makeMoveControl();
		
		simu.goToStep(1);
		simu.goToStep(0);
		primaryStage.show();
	}

	private double radius=300;
	private Position lastpositionmouse;
	private boolean moving = false;
	private void makeMoveControl() {
		double step = 10;
		double stepzoom=20;
		GridPane grid = new GridPane();
		grid.setLayoutX(10.0);
		grid.setLayoutY(400.0);
		
		Button up = new Button("Up");
		Button bottom = new Button("Bottom");
		Button left = new Button("Left");
		Button right = new Button("Right");
		
		up.setOnAction(e-> {
			center = new Position(center.getX(), center.getY()-step);			
			placeNodes();
		});
		bottom.setOnAction(e-> {
			center = new Position(center.getX(), center.getY()+step);			
			placeNodes();
		});
		left.setOnAction(e-> {
			center = new Position(center.getX()-step, center.getY());			
			placeNodes();
		});
		right.setOnAction(e-> {
			center = new Position(center.getX()+step, center.getY());			
			placeNodes();
		});
		
		Button zoom = new Button("+");
		Button dezoom = new Button("-");
		
		
		zoom.setOnAction(e-> {radius+= stepzoom; placeNodes();});
		dezoom.setOnAction(e-> {radius-= stepzoom; placeNodes();});
		
		
		scene.setOnMousePressed(e->{
			if(e.getButton() == MouseButton.MIDDLE) {
				moving=true;
				lastpositionmouse=new Position(e.getSceneX(), e.getSceneY());
			}
		});
		scene.setOnMouseReleased(e->{
			if(e.getButton() == MouseButton.MIDDLE) {
				moving=false;
			}
		});
		scene.setOnMouseDragged(e->{
			if(moving) {
				Position newposition = new Position(e.getSceneX(), e.getSceneY());
				double deltax = newposition.getX()-lastpositionmouse.getX();
				double deltay = newposition.getY() - lastpositionmouse.getY();
				
				Position newcenter = new Position(center.getX()+deltax, center.getY()+deltay);
				center=newcenter;
				lastpositionmouse=newposition;		
				placeNodes();
			}
			
		});
		
		scene.setOnScroll(e-> {
			if(e.getDeltaY()<0) {
				radius-= stepzoom;
			}else {
				radius+= stepzoom;
			}
			 placeNodes();
		});
		
		
		
		grid.add(up, 1, 0);
		grid.add(bottom, 1, 2);
		
		grid.add(left, 0, 1);
		grid.add(right, 2, 1);
		
		grid.add(zoom, 0, 3);
		grid.add(dezoom, 1, 3);
		root.getChildren().add(grid);
	}


	private void makeStepcounter() {
		HBox stepcounter = new HBox();
		root.getChildren().add(stepcounter);
				
		
		stepcounter.setLayoutX(10.0);
		stepcounter.setLayoutY(200.0);
		
		TextField valstepcounter = new TextField("0");
		valstepcounter.setPrefWidth(100);
		valstepcounter.setAlignment(Pos.CENTER_RIGHT);
		Text maxcounter = new Text("/"+simu.getListLogEvent().size());
		stepcounter.getChildren().addAll(valstepcounter,maxcounter);
		valstepcounter.setOnMouseClicked(e->{
			valstepcounter.selectAll();
		});
		valstepcounter.setOnAction(e->{
			int val = Integer.parseInt(valstepcounter.getText());
			if(val < 0) val=0;
			if(val > simu.getListLogEvent().size()) val=simu.getListLogEvent().size();
			valstepcounter.setDisable(true);
			simu.goToStep(val);	
			valstepcounter.setText(val+"");
			valstepcounter.setDisable(false);
		});
		
		simu.getNextstep().addListener((obs,o,n)->valstepcounter.setText(n+""));
		
	}


	private void makeLogsGroup() {
		
		VBox vboxlogs = new VBox(10.0);
		ScrollPane logs = new ScrollPane(vboxlogs);
		//logs.getChildren().add(vboxlogs);
		//logs.setFitToWidth(true);	
		//logs.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		Text[] logtext = new Text[5];
		for(int i=0;i< logtext.length;i++) {
			final int index=i;
			logtext[index]=new Text("");
			simu.getNextstep().addListener((obs,o,n)->{
				int pointer = n.intValue()+index-2;
				if(pointer < 0 || pointer >= simu.getListLogEvent().size()) logtext[index].setText("...");
				else logtext[index].setText(simu.getListLogEvent().get(pointer).toString());
				if(index-2 == 0 ) logtext[index].setFill(Color.RED);
				
			});
			vboxlogs.getChildren().add(logtext[index]);
		}		
		root.getChildren().add(logs);
	}

	private Group networkgroup = new Group();
	private Text messagedescription;
	private void makeNetworkGroup() {
		
		messagedescription = new Text();
				
		messagedescription.setLayoutX(50.0);
		messagedescription.setLayoutY(300.0);
		
		
		simu.getNetwork().addListener((obs,o,n)->{					
			placeMessages(n);
		});
		root.getChildren().addAll(networkgroup,messagedescription);
	}

	private Position center;

	private void makeNodesGroup() {
		Group res = new Group();
		
		center = new Position((scene.getWidth() / 2.0) , (scene.getHeight() / 2.0));
		for(Host h : simu.getHosts()) {
			HostView hview = new HostView(h);
			mappinghost.put(h.getId(), hview);
			res.getChildren().add(hview);
		}
		placeNodes();
		root.getChildren().add(res);
	}
	
	private void placeMessages(ObservableList<Message<?>> n) {
		networkgroup.getChildren().clear();
		Map<Pair<IDNode,IDNode>,Double> cpts = new HashMap<>();
		for(Message<?> m : n) {
			IDNode src = m.getSrc();
			IDNode dest = m.getDest();
			
			HostView viewsrc = mappinghost.get(src);
			HostView viewdest = mappinghost.get(dest);
			
			double cpt = cpts.getOrDefault(new Pair<>(src,dest),viewsrc.getRadius()); 
			
			
			String namemessage = m.getClass().getSimpleName();
			
			if(!messagecolor.containsKey(namemessage)) {
				messagecolor.put(namemessage, getNewColor());
			}
							
			
			Arrow arrow = new Arrow(viewsrc.getLayoutX()+cpt,
									viewsrc.getLayoutY()+cpt,
									viewdest.getLayoutX()+cpt,
									viewdest.getLayoutY()+cpt,
									messagecolor.get(namemessage));
			
			cpts.put(new Pair<>(src,dest), cpt+10);
			cpts.put(new Pair<>(dest,src), cpt+10);
							
			
			arrow.setOnMouseEntered(e->{
				messagedescription.setText(m.getNameProto()+"."+namemessage+"("+m.getContent()+")");
				messagedescription.setFill(messagecolor.get(namemessage));
			});
			arrow.setOnMouseExited(e->{
				messagedescription.setText("");
			});
			
			networkgroup.getChildren().add(arrow);
		}
	}
	
	private void placeNodes() {		
		//double radius = Math.min(center.getX(),center.getY())*factor_radius;
		double delta_angle = 2 * Math.PI / (double) reports.get(IDNode.get(0)).getExperiment().getNbNode();		
		for(Entry<IDNode,HostView> e : mappinghost.entrySet()) {
			Host h = simu.getHost(e.getKey());
			HostView hview = e.getValue();
			Position p = center.getNewPositionWith(radius, delta_angle * h.getId().getVal());
			//System.out.println(radius);
			hview.setLayoutX(p.getX());
			hview.setLayoutY(p.getY());
//			System.out.println(hview.getLayoutX()+" "+hview.getLayoutY());
		}
//		System.out.println("--------");
		if(!networkgroup.getChildren().isEmpty()) {
			placeMessages(simu.getNetwork());
		}

	}
	

	
	
}
