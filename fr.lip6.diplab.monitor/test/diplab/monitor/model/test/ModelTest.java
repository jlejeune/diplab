package diplab.monitor.model.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import diplab.adaptervalidator.broadcast.AppliBroadcast;
import diplab.adaptervalidator.broadcast.ReliableBroadcast;
import diplab.adaptervalidator.naimitrehel.NTNode;
import diplab.core.IDNode;
import diplab.core.experiment.ExperimentBuilder;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.logevent.LogCallEvent;
import diplab.core.experiment.reporting.logevent.LogCrashEvent;
import diplab.core.experiment.reporting.logevent.LogEndCallEvent;
import diplab.core.experiment.reporting.logevent.LogEndInterruptionCallEvent;
import diplab.core.experiment.reporting.logevent.LogEvent;
import diplab.core.experiment.reporting.logevent.LogMessageReceiveEvent;
import diplab.core.experiment.reporting.logevent.LogMessageSentEvent;
import diplab.core.experiment.reporting.logevent.LogRecoverEvent;
import diplab.core.experiment.reporting.logevent.LogStartCallEvent;
import diplab.core.experiment.reporting.logevent.LogStartInterruptionCallEvent;
import diplab.core.experiment.scenario.ConstanteValue;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.CrashAction;
import diplab.core.experiment.scenario.action.callscheme.Call;
import diplab.core.experiment.scenario.action.callscheme.CallSequence;
import diplab.core.experiment.scenario.action.callscheme.RepeatedCallSequence;
import diplab.core.messaging.Message;
import diplab.monitor.model.CallStack;
import diplab.monitor.model.Host;
import diplab.monitor.model.Simulation;
import diplab.system.peersim.experiment.PeerSimExperimentController;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.beans.property.ReadOnlyListProperty;
import peersim.transport.UniformRandomTransport;

public class ModelTest {

	private static final String nameproto ="NT";
	private static Map<IDNode, Report> reports;
	private static final String id_app="app";
	private static final String id_broadcast="broadcast";
	
	public static void generateConfig( ExperimentBuilder builder) {
		File conffile = new File(System.getProperty("java.io.tmpdir")+"/peersimnetwork.conf");
		if(conffile.exists()) conffile.delete();
		try(PrintStream out2 = new PrintStream(new FileOutputStream(conffile,true))){
			out2.println("protocol.transport "+UniformRandomTransport.class.getName());
			out2.println("protocol.transport.mindelay 10 ");
			out2.println("protocol.transport.maxdelay 10");
		} catch (FileNotFoundException e) {
			throw new IllegalStateException(e);
		}
		builder.addProperty(PeerSimExperimentController.PAR_PEERSIM_PATH_CONFIGFILE,conffile.getAbsolutePath());
		
	}
	
	@BeforeClass
	public static void setup() throws Exception{
		ExperimentBuilder ebuilder = new ExperimentBuilder(PeerSimExperimentController.class, 100000);
		ebuilder.addNodeProto(nameproto, NTNode.class);
		ebuilder.withSystemSize(4);
		ebuilder.addScenarioAction(new CallAction(new ConstanteValue<>(50L),new CallSequence()
				.addCallScheme(new Call(nameproto, "requestCS", new ConstanteValue<>(20L)))
				.addCallScheme(new Call(nameproto, "releaseCS", new ConstanteValue<>(30L))), IDNode.get(1)));
		
		ebuilder.addScenarioAction(new CallAction(new ConstanteValue<>(145L),new CallSequence()
				.addCallScheme(new Call(nameproto, "requestCS", new ConstanteValue<>(20L)))
				.addCallScheme(new Call(nameproto, "releaseCS", new ConstanteValue<>(30L))), IDNode.get(2)));
		
		
		ebuilder.addScenarioAction(new CallAction(new ConstanteValue<>(153L),new CallSequence()
				.addCallScheme(new Call(nameproto, "requestCS", new ConstanteValue<>(20L)))
				.addCallScheme(new Call(nameproto, "releaseCS", new ConstanteValue<>(30L))), IDNode.get(0)));
		
		
		ebuilder.addNodeProto(id_broadcast, ReliableBroadcast.class);
		ebuilder.addNodeProto(id_app,AppliBroadcast.class);
		
		ebuilder.addScenarioAction(new CallAction( 
				(r)-> 30L + r.nextInt(100), 
				new RepeatedCallSequence(1)
				.addCallScheme(new Call(id_app, "hellobroadcast", (r)->r.nextInt(30)+1L))
				)
				);

		
		
		ebuilder.addScenarioAction(new CrashAction((r)->250L, (r)-> 50L, IDNode.get(1)));
		
		
		
		generateConfig(ebuilder);
		reports =  ebuilder.build().execute();
			
		//all.analyse(PrintVisitor.get());		
	}
	
	@Test
	public void testForwardBackward() {
		Simulation simu = new Simulation(reports);
		ReadOnlyIntegerProperty nexstep = simu.getNextstep();		
		assertEquals(0,nexstep.get());
		simu.backward();
		assertEquals(0,nexstep.get());
		
		simu.forward();
		assertEquals(1,nexstep.get());
		
		simu.forward(2);
		assertEquals(3,nexstep.get());
		
		simu.goToStep(simu.getExecution().size());
		assertEquals(simu.getExecution().size(), nexstep.get());
		
		simu.forward(3);
		assertEquals(simu.getExecution().size(), nexstep.get());
				
	}
	
	@Test
	public void testNetwork() {
		Simulation simu = new Simulation(reports);
		ReadOnlyListProperty<Message<?>> network = simu.getNetwork();
		for(LogEvent event : simu.getExecution()) {
			simu.forward();
			if(event instanceof LogMessageSentEvent lme) {
				assertTrue(network.contains(lme.getMessage()));
			}
			if(event instanceof LogMessageReceiveEvent lme) {
				assertFalse(network.contains(lme.getMessage()));
			}
		}
		
	}
	
	@Test
	public void testNetworkReverse() {
		Simulation simu = new Simulation(reports);
		
		List<LogEvent> reverse =  new ArrayList<>(simu.getExecution());
		Collections.reverse(reverse);
		ReadOnlyListProperty<Message<?>> network = simu.getNetwork();
		simu.goToStep(simu.getExecution().size());
		for(LogEvent event : reverse) { 
			simu.backward();
			if(event instanceof LogMessageSentEvent lme) {
				assertFalse(network.contains(lme.getMessage()));
			}
			if(event instanceof LogMessageReceiveEvent lme) {
				assertTrue(network.contains(lme.getMessage()));
			}
		}
		
	}
	
	
	@Test
	public void testState() {
		Simulation simu = new Simulation(reports);
		for(LogEvent event : simu.getExecution()) {
			simu.forward();
			if(event instanceof LogCallEvent lce) {
				Map<String,Object> state = lce.getState();
				if(state == null) state = Collections.emptyMap();
				assertEquals(state, simu.getHost(lce.getID().getNode()).getCurState(lce.getNodeProtoName()));
			}
			
		}
		
	}
	
	@Test
	public void testStateReverse() {
		Simulation simu = new Simulation(reports);
		
		List<LogEvent> reverse =  new ArrayList<>(simu.getExecution());
		Collections.reverse(reverse);
		simu.goToStep(simu.getExecution().size());
		for(LogEvent event : reverse) { 
			if(event instanceof LogCallEvent lce) {
				Map<String,Object> state = lce.getState(); 
				if(state == null) {
					state = Collections.emptyMap();
				}
				assertEquals(state, simu.getHost(lce.getID().getNode()).getCurState(lce.getNodeProtoName()));
			}
			simu.backward();
		}
	}
	
	@Test
	public void testCallStack() {
		Simulation simu = new Simulation(reports);
		for(LogEvent event : simu.getExecution()) {
			if(! (event instanceof LogCallEvent )) {
				simu.forward();
				continue;
			}			
			Host h = simu.getHost(event.getID().getNode());
			CallStack cur = h.getCurCallStack();
			if(cur == null) {
				simu.forward();
				cur = h.getCurCallStack();
				if(event instanceof LogStartCallEvent e) {
					assertTrue(h.getCallStacks().contains(cur));
					assertEquals(1, cur.size());
					assertEquals(e,cur.top());
				}else if(event instanceof LogEndInterruptionCallEvent e) {
					assertNotNull(cur);
					assertTrue(h.getCallStacks().contains(cur));
					assertNull(cur.getInterruption());
					assertTrue(cur.top().matchWith(e));
					
				}else {
					assertFalse(true);
				}
			}else {
				LogStartCallEvent top_before = cur.top();
				int size_before = cur.size();
				
				simu.forward();
				
				if(event instanceof LogStartCallEvent e) {
					assertEquals(size_before+1, cur.size());
					assertNotEquals(top_before, e);				
					assertEquals(e,cur.top());
					assertNotNull(h.getCurCallStack());
				} else if(event instanceof LogEndCallEvent e) {
					assertEquals(size_before-1, cur.size());
					assertTrue(e.matchWith(top_before));
					if(cur.size() != 0) {
						assertNotEquals(top_before, cur.top());
						assertNotNull(h.getCurCallStack());
					}else {
						assertNull(h.getCurCallStack());
					}
				}else if(event instanceof LogStartInterruptionCallEvent e) {
					assertEquals(size_before, cur.size());
					assertEquals(e,cur.getInterruption());	
					assertTrue(h.getCallStacks().contains(cur));
					assertNull(h.getCurCallStack());
				}else {
					assertFalse(true);
				}
			}
		}
	}
	
	@Test
	public void testCrashRecover() {
		Simulation simu = new Simulation(reports);
		for(LogEvent event : simu.getExecution()) {
			simu.forward();
			Host h = simu.getHost(event.getID().getNode());
			if(event instanceof LogCrashEvent e) {
				assertFalse(h.isActive());
			}else if(event instanceof LogRecoverEvent e) {
				assertTrue(h.isActive());
			}
		}
	}
	
	@Test
	public void testCrashRecoverReverse() {
		Simulation simu = new Simulation(reports);
		
		List<LogEvent> reverse =  new ArrayList<>(simu.getExecution());
		Collections.reverse(reverse);
		simu.goToStep(simu.getExecution().size());
		for(LogEvent event : reverse) {
			simu.backward();
			Host h = simu.getHost(event.getID().getNode());
			if(event instanceof LogCrashEvent e) {
				assertTrue(h.isActive());
			}else if(event instanceof LogRecoverEvent e) {
				assertFalse(h.isActive());
			}
		}
	}
	
	
}
