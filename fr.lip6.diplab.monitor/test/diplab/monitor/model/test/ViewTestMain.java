package diplab.monitor.model.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.util.Map;

import diplab.adaptervalidator.naimitrehel.NTNode;
import diplab.core.IDNode;
import diplab.core.experiment.ExperimentBuilder;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.scenario.ConstanteValue;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.callscheme.Call;
import diplab.core.experiment.scenario.action.callscheme.CallSequence;
import diplab.monitor.Monitor;
import diplab.system.peersim.experiment.PeerSimExperimentController;
import javafx.application.Application;
import peersim.transport.UniformRandomTransport;

public class ViewTestMain {

	public static void generateConfig( ExperimentBuilder builder) {
		File conffile = new File(System.getProperty("java.io.tmpdir")+"/peersimnetwork.conf");
		if(conffile.exists()) conffile.delete();
		try(PrintStream out2 = new PrintStream(new FileOutputStream(conffile,true))){
			out2.println("protocol.transport "+UniformRandomTransport.class.getName());
			out2.println("protocol.transport.mindelay 10 ");
			out2.println("protocol.transport.maxdelay 10");
		} catch (FileNotFoundException e) {
			throw new IllegalStateException(e);
		}
		builder.addProperty(PeerSimExperimentController.PAR_PEERSIM_PATH_CONFIGFILE,conffile.getAbsolutePath());
		
	}
	private static final String nameproto ="NT";
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		ExperimentBuilder ebuilder = new ExperimentBuilder(PeerSimExperimentController.class, 100000);
		ebuilder.addNodeProto(nameproto, NTNode.class);
		ebuilder.withSystemSize(3);
		ebuilder.addScenarioAction(new CallAction(new ConstanteValue<>(50L),new CallSequence()
				.addCallScheme(new Call(nameproto, "requestCS", new ConstanteValue<>(20L)))
				.addCallScheme(new Call(nameproto, "releaseCS", new ConstanteValue<>(30L))), IDNode.get(1)));
		
		ebuilder.addScenarioAction(new CallAction(new ConstanteValue<>(145L),new CallSequence()
				.addCallScheme(new Call(nameproto, "requestCS", new ConstanteValue<>(20L)))
				.addCallScheme(new Call(nameproto, "releaseCS", new ConstanteValue<>(30L))), IDNode.get(2)));
		
		
		ebuilder.addScenarioAction(new CallAction(new ConstanteValue<>(153L),new CallSequence()
				.addCallScheme(new Call(nameproto, "requestCS", new ConstanteValue<>(20L)))
				.addCallScheme(new Call(nameproto, "releaseCS", new ConstanteValue<>(30L))), IDNode.get(0)));
		
		generateConfig(ebuilder);
		
		Map<IDNode, Report> reports =  ebuilder.build().execute();
		
		String f = System.getProperty("java.io.tmpdir")+"/reports";
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f))){
			oos.writeObject(reports);
		}
		
		Application.launch(Monitor.class, new String[] {f});
				
	}
	
}
