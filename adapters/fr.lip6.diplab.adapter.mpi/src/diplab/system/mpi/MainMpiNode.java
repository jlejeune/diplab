package diplab.system.mpi;

import static mpi.MPI.COMM_WORLD;
import static mpi.MPI.InitThread;
import static mpi.MPI.THREAD_SERIALIZED;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import diplab.core.IDNode;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.reporting.Report;
import diplab.system.mpi.experiment.MpiExperimentController;
import diplab.system.multithread.MultithreadNodeSystem;
import mpi.MPI;
import mpi.MPIException;

public class MainMpiNode {

	private static String host_controller;
	private static int port_controller;
	
	public static void main(String[] args) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException, MPIException, InterruptedException, UnknownHostException, IOException {
				
		String usage="<host controller> <port controller>";
		if(args.length < 2) {
			System.err.println(usage);
			System.exit(1);
		}
		host_controller=args[0];
		port_controller=Integer.parseInt(args[1]);
		
		InitThread(args, THREAD_SERIALIZED);
		IDNode myid=IDNode.get(COMM_WORLD.getRank());
		int size=COMM_WORLD.getSize();
		List<IDNode> allnodes=new ArrayList<>();
		for(int i=0; i<size;i++) {
			allnodes.add(IDNode.get(i));
		}
		Experiment e = getExpe();
		long starttime = getStartTime();
		
		MPINetwork network =  new MPINetwork();
				
		MultithreadNodeSystem nodesystem = new MultithreadNodeSystem(e, starttime, myid, allnodes, network);
		network.setNodeSystem(nodesystem);
		System.err.println("Starting "+myid+" on "+InetAddress.getLocalHost().getHostName());
		nodesystem.start();
		final Object lock = new Object();
		synchronized(lock) {
			lock.wait(e.getExperimentDuration());
		}
		nodesystem.stop();
		
		
		Report local = nodesystem.getLocalReport();
		Report health = nodesystem.getHealthReport();
		if(health.size()> 0) {
			local=local.merge(health, local.getName());
		}		
		setReport(nodesystem.idnode(),local);
		COMM_WORLD.barrier();
		MPI.Finalize();	
		System.exit(0);
	}

	public static Experiment getExpe() {
		try(Socket sock = new Socket(host_controller, port_controller);
				ObjectOutputStream outstream = new ObjectOutputStream(sock.getOutputStream());
				ObjectInputStream instream = new ObjectInputStream(sock.getInputStream())	){
			outstream.writeObject(MpiExperimentController.GET_EXPE);
			Experiment expe = (Experiment) instream.readObject();
			return expe;
		} catch (IOException | ClassNotFoundException e) {
			throw new IllegalStateException(e);
		}		
	}
	
	public static long getStartTime() {
		try(Socket sock = new Socket(host_controller, port_controller);
				ObjectOutputStream outstream = new ObjectOutputStream(sock.getOutputStream());
				ObjectInputStream instream = new ObjectInputStream(sock.getInputStream())	){
			outstream.writeObject(MpiExperimentController.GET_STARTTIME);
			Long starttime = (Long) instream.readObject();
			return starttime;
		} catch (IOException | ClassNotFoundException e) {
			throw new IllegalStateException(e);
		}		
	}
	
	public static void setReport(IDNode id,Report report) {
		try(Socket sock = new Socket(host_controller, port_controller);
				ObjectOutputStream outstream = new ObjectOutputStream(sock.getOutputStream());
				ObjectInputStream instream = new ObjectInputStream(sock.getInputStream())	){
			outstream.writeObject(MpiExperimentController.SET_REPORT);
			outstream.writeObject(id);
			outstream.writeObject(report);
			instream.readInt();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}		
	}
	
}
