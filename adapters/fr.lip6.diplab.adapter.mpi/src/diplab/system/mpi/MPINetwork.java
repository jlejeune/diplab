package diplab.system.mpi;

import static mpi.MPI.ANY_SOURCE;
import static mpi.MPI.COMM_WORLD;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

import diplab.core.NodeProto;
import diplab.core.event.MessageReceptionEvent;
import diplab.core.messaging.Message;
import diplab.system.multithread.MultithreadNodeSystem;
import diplab.system.multithread.Network;
import mpi.MPI;
import mpi.MPIException;
import mpi.Status;

public class MPINetwork extends Thread implements Network {

	private  MultithreadNodeSystem nodesystem;

	private static final int TAG_APPLI=0;
	private static final int TAG_END=1;
	private static final int TAG_ACK_END=2;
	private static final int NODE_MASTER=0;

	private boolean ready=false;
	private boolean ending=false;

	private boolean end_message_sent = false;
	private int nb_end_received=0;//only used by MASTER Node

	private BlockingQueue<MessageToSent> to_sent  = new LinkedBlockingQueue<>();

	private final  AtomicBoolean crashed;



	public MPINetwork() {
		this.crashed=new AtomicBoolean(false);
	}

	public void setNodeSystem(MultithreadNodeSystem system) {
		this.nodesystem=system;
		this.setName("MPINetwork-node-"+nodesystem.idnode());
	}

	@Override
	public void run() {		
		try {

			COMM_WORLD.barrier();
			synchronized(this) {
				ready=true;
				this.notifyAll();
			}
			while(!end()) {

				//envoyer eventuellement les messages
				sendMessages();
				//y a t-il des message applicatifs a recevoir
				receiveApplicativeMessages();

			}
		}catch(MPIException | InterruptedException mpie) {
			mpie.printStackTrace();
			System.exit(1);
		}
	}



	private boolean end() throws MPIException {


		synchronized (this) {

			if(!ending) return false;
			if(!end_message_sent) {
				byte[] nil=new byte[]{0};
				COMM_WORLD.send(nil, 1, MPI.BYTE,NODE_MASTER, TAG_END);
				end_message_sent=true;
			}
			Status stat = COMM_WORLD.iProbe(NODE_MASTER, TAG_ACK_END);
			if(stat != null) {
				byte[] nil=new byte[]{0};
				COMM_WORLD.recv(nil, 1, MPI.BYTE , NODE_MASTER, TAG_ACK_END);
				this.notify();
				return true;
			}
			if(nodesystem.idnode().getVal() == NODE_MASTER) {
				stat = COMM_WORLD.iProbe(ANY_SOURCE, TAG_END);
				while(stat != null) {
					byte[] nil=new byte[]{0};
					COMM_WORLD.recv(nil, 1, MPI.BYTE , stat.getSource(), TAG_END);
					nb_end_received++;
					stat = COMM_WORLD.iProbe(ANY_SOURCE, TAG_END);
				}
				if(nb_end_received == nodesystem.systemsize()) {
					for(int i=0; i < nodesystem.systemsize() ; i++) {
						byte[] nil=new byte[]{0};
						COMM_WORLD.send(nil, 1, MPI.BYTE,i, TAG_ACK_END);
					}
				}
			}

			return false;
		} 

	}

	private int sendMessages() throws MPIException, InterruptedException{
		int cpt=0;
		while(!to_sent.isEmpty()) {
			MessageToSent message = to_sent.take();
			COMM_WORLD.send(message.content, message.content.length, MPI.BYTE , message.dest , TAG_APPLI);
			cpt++;
		}
		return cpt;
	}

	private int receiveApplicativeMessages() throws MPIException {
		Status stat = COMM_WORLD.iProbe(ANY_SOURCE, TAG_APPLI);
		int cpt = 0;
		while(stat != null) {
			int tag = stat.getTag();
			int source = stat.getSource();
			int count = stat.getCount(MPI.BYTE);

			byte[] buf = new byte[count];
			COMM_WORLD.recv(buf, count, MPI.BYTE, source, tag);

			try(ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(buf));) {
				Message<?> message= (Message<?>)  ois.readObject();
				NodeProto nodeproto = nodesystem.getNodeProto(message.getNameProto());
				if(!crashed.get()) {
					Thread.sleep(0, 150);//a minimum latency, TODO change to be configurable
					nodesystem.getEventprocessor().putEvent(new MessageReceptionEvent(message, nodesystem, nodeproto));			
				}
			} catch (IOException | ClassNotFoundException  | InterruptedException e) {
				System.err.println("Error when receiving applicative message in thread "+getName());
			} 
			stat = COMM_WORLD.iProbe(ANY_SOURCE, TAG_APPLI);
			cpt++;
		}
		return cpt;
	}

	@Override
	public void start(boolean waitready) {
		this.start();
		if(waitready) {
			try {
				synchronized(this) {
					while(!ready) {
						wait();
					}
				}	
			}catch(InterruptedException ie) {
				System.err.println("Error when starting event processor on node "+nodesystem.idnode());
			}
		}

	}

	@Override
	public void stop(boolean waitend) {
		synchronized(this) {
			ending=true;
		}
		try {
			this.join();
		} catch (InterruptedException e) {
			System.err.println(" Error when joining for thread "+this.getName());
		}
	}

	@Override
	public void sendReal(Message<?> message) {
		byte[] buf;
		try(ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ObjectOutputStream oos = new ObjectOutputStream(baos);) {
			int dest = (int) message.getDest().getVal();
			oos.writeObject(message);
			buf=baos.toByteArray();
			//Thread.sleep(0, 500);
			to_sent.put(new MessageToSent(dest, buf));

		} catch ( InterruptedException | IOException e) {

		} 

	}

	@Override
	public void crash(MultithreadNodeSystem multithreadNodeSystem) {
		this.crashed.set(true);
	}

	@Override
	public void recover(MultithreadNodeSystem multithreadNodeSystem) {
		this.crashed.set(false);
	}
	private static class MessageToSent{
		private final int dest;
		private final byte[] content;

		public MessageToSent(int dest, byte[] content) {
			this.dest=dest;
			this.content=content;
		}
	}
	@Override
	public void nomoreaction(MultithreadNodeSystem multithreadNodeSystem) {
		System.err.println(multithreadNodeSystem.idnode()+" : no more action (end when no action not implemented)");
		
	}
	@Override
	public void nomoreEvents(MultithreadNodeSystem multithreadNodeSystem) {
		//TODO
	}
}