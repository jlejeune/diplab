package diplab.system.mpi.experiment;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import diplab.core.IDNode;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.ExperimentController;
import diplab.core.experiment.reporting.Report;
import diplab.system.mpi.MainMpiNode;

public class MpiExperimentController implements ExperimentController {

	public static final String PAR_MPI_PATH_HOSTFILE="experiment.mpi.hostfile";
	
	public static final String GET_EXPE="GETEXPE";

	public static final String SET_REPORT = "SETREPORT";

	public static final String GET_STARTTIME = "SETSTARTTIME";
	

	private Long starttime = null;
	private  Map<IDNode,Report> reports = new HashMap<>(); 
		
	@Override
	public Map<IDNode,Report> execute(final Experiment e) {
		
		
		try (ServerSocket ss = new ServerSocket(0);){
			int port = ss.getLocalPort();
			String host = InetAddress.getLocalHost().getHostAddress();			
			String[] cmd = new String[] {
					"mpirun",
					"--hostfile",
					(String)e.getProperties().get(PAR_MPI_PATH_HOSTFILE),
					"-np",
					e.getNbNode()+"",
					"java",
					"-cp",
					System.getProperty("java.class.path"),
					MainMpiNode.class.getCanonicalName(),
					host,
					port+""
			};
			
					
			Thread network_listener = new Thread(() -> {
				while(reports.size() < e.getNbNode()) {
					
					try(Socket sock= ss.accept();){
						try(ObjectInputStream ois = new ObjectInputStream(sock.getInputStream()); 
							ObjectOutputStream oos = new ObjectOutputStream(sock.getOutputStream()) ){
						
							Object objreceived = ois.readObject();
							if( objreceived instanceof String) {
								String request = (String) objreceived;
								switch(request) {
								case GET_EXPE:
									oos.writeObject(e);
									break;
								case SET_REPORT:
									IDNode id = (IDNode) ois.readObject();
									Report r = (Report) ois.readObject();
									reports.put(id,r);//monothreaded server, so synchro is useless
									oos.writeInt(0);
									System.err.println("Receive report from "+id);
									break;
								case GET_STARTTIME:
									if(starttime==null) {
										starttime=System.currentTimeMillis();
									}
									oos.writeObject(starttime);
									break;
								default:
									throw new IllegalStateException("Erreur de protocole de "+ sock.getInetAddress() + ":"+sock.getPort());
								}
							}
							
						} 					
					}catch (ClassNotFoundException | IOException e1) {
						throw new IllegalStateException(e1);
					} 
				}//fin boucle
			}, "Server-thread-mpi-controller");
			
			network_listener.start();
			
			
			ProcessBuilder pb = new ProcessBuilder(cmd).inheritIO();
			
			Process p = pb.start();
			
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					p.destroyForcibly();
				}
			});
			
			p.waitFor();
			System.err.println("Shutting down server socket");
			network_listener.interrupt();
			ss.close();
			network_listener.join();
			System.err.println("Shutting down server socket");
			if(p.exitValue() != 0) {
				throw new IllegalStateException("mpi process return a value > 0");
			}
			
		} catch (IOException | InterruptedException e1) {
			throw new IllegalStateException(e1);
		}
					
		return reports;
				
	}
}
