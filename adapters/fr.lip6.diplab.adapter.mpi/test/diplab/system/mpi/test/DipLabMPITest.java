package diplab.system.mpi.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import diplab.adaptervalidator.DipLabTest;
import diplab.adaptervalidator.broadcast.TestCallBroadcast;
import diplab.adaptervalidator.broadcast.TestReliableBroadcast;
import diplab.adaptervalidator.hello.TestHello;
import diplab.adaptervalidator.helloasync.TestHelloAsync;
import diplab.adaptervalidator.helloring.TestHelloRing;
import diplab.adaptervalidator.helloring.TestHelloRingCrash;
import diplab.adaptervalidator.hellosleep.TestHelloSleep;
import diplab.adaptervalidator.hellowait.TestHelloWait;
import diplab.adaptervalidator.multialgo.TestMultiAlgo;
import diplab.adaptervalidator.naimitrehel.TestNT;
import diplab.core.experiment.ExperimentBuilder;
import diplab.core.experiment.ExperimentController;
import diplab.system.mpi.experiment.MpiExperimentController;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DipLabMPITest extends DipLabTest{
	
	static final Class<? extends ExperimentController> controler_class = MpiExperimentController.class;
	
	@Test
	public void a_testHello() {
		test(controler_class, TestHello.class, 5, 12000L, 1);
	}
	
	@Test
	public void b_testHelloRing() {
		test(controler_class, TestHelloRing.class, 5, 8000L, 1);
	}
	
	@Test
	public void c_testHelloRingCrash() {
		test(controler_class, TestHelloRingCrash.class, 5, 8000L, 1);
	}
	
	@Test
	public void d_testHelloWait() {
		test(controler_class, TestHelloWait.class, 5, 8000L, 1);
	}
	
	@Test
	public void da_testHelloAsync() {
		test(controler_class, TestHelloAsync.class, 5, 15000L, 1);
	}
	
	@Test
	public void db_testHelloSleep() {
		test(controler_class, TestHelloSleep.class, 5, 15000L, 1);
	}
	
	@Test
	public void e_testMultiAlgo() {
		test(controler_class, TestMultiAlgo.class, 5, 20000L, 1);
	}
	
	@Test
	public void f_testCallBroadcast() {
		test(controler_class, TestCallBroadcast.class, 5, 5000L, 1);
	}

	@Test
	public void g_testReliableBroadcast() {
		test(controler_class, TestReliableBroadcast.class, 5, 5000L, 1);
	}
	
	@Test
	public void h_testNT() {
		test(controler_class, TestNT.class, 05, 25000L, 1);
	}
		
	@Override
	public void generateConfig(ExperimentBuilder builder) {
		
		File hostfile;
		if(System.getenv("OAR_NODEFILE") !=null) {
			hostfile=hostfileOAR(builder);
		}else {
			hostfile=hostfileLocal(builder);
		}		
		
		builder.addProperty(MpiExperimentController.PAR_MPI_PATH_HOSTFILE,hostfile.getAbsolutePath());

		builder.withProgressIndicator();
	
	}
	
	
	
	private File hostfileOAR(ExperimentBuilder builder) {
		File hostfile = new File(System.getProperty("java.io.tmpdir")+"/hostfile");
		if(hostfile.exists()) hostfile.delete();
		Map<String, Integer> slots = new HashMap<>();
		
		try(BufferedReader br = new BufferedReader(new FileReader(System.getenv("OAR_NODEFILE")))){
			String line;
			while((line = br.readLine()) !=null) {
				slots.put(line,0);
			}
			System.err.println("Using hosts : " + slots.keySet());
		} catch (IOException e1) {
			throw new IllegalStateException(e1);
		} 
		if(slots.size() < builder.getSystemsize() ) {
			System.err.println("Warning : the system size in experiment ("+builder.getSystemsize()+") is greater than the number of real hosts ("+slots.size()+")");
		}
		List<String> hosts = new ArrayList<>(slots.keySet());
		for(int i =0, j=0 ; i < builder.getSystemsize(); i++, j=(j+1)%hosts.size()) {
			String name_host=hosts.get(j);
			int cpt = slots.get(name_host);
			cpt++;
			slots.put(name_host, cpt);
		}
				
		try(PrintStream out = new PrintStream(new FileOutputStream(hostfile,true))){
			for(Entry<String,Integer> e : slots.entrySet()) {
				out.println(e.getKey()+" slots="+e.getValue());
			}
			
			
		} catch (FileNotFoundException e) {
			throw new IllegalStateException(e);
		}
		return hostfile;
	}

	private File hostfileLocal(ExperimentBuilder builder) {
		File hostfile = new File(System.getProperty("java.io.tmpdir")+"/hostfile");
		if(hostfile.exists()) hostfile.delete();

		try(PrintStream out = new PrintStream(new FileOutputStream(hostfile,true))){
			out.println("localhost slots="+builder.getSystemsize());
		} catch (FileNotFoundException e) {
			throw new IllegalStateException(e);
		}
		return hostfile;
	}
	

}
