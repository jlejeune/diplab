package diplab.system.peersim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Stream;

import diplab.core.IDNode;
import diplab.core.NodeSystem;
import diplab.core.event.CallEvent;
import diplab.core.event.Event;
import diplab.core.event.MessageReceptionEvent;
import diplab.core.event.PrimitiveCallEvent;
import diplab.core.event.WaitingConditionCheckingEvent;
import diplab.core.experiment.Deterministic;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.reporting.logevent.LogCrashEvent;
import diplab.core.experiment.reporting.logevent.LogRecoverEvent;
import diplab.core.experiment.scenario.action.Action;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.CrashAction;
import diplab.core.experiment.scenario.action.callscheme.Call;
import diplab.core.messaging.Message;
import diplab.system.peersim.experiment.scenario.CallActionExecution;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.core.Node;
import peersim.core.Protocol;
import peersim.edsim.EDProtocol;
import peersim.edsim.EDSimulator;
import peersim.transport.Transport;

@Deterministic
public class NodeSystemPeerSim extends  NodeSystem implements EDProtocol{

	public static final String PAR_TRANSPORT = "transport";
		
	private static final String recover_event="recover";
	
	
	private final int protocol_id;
	private final int transport_id;
	
	private Node me; 
	private IDNode my_id;
	
	
	private boolean is_alive=true;
	
	private static Node getNodeFromIDNode(IDNode id) {
		for(int i=0 ; i< Network.size();i++) {
			Node tmp = Network.get(i); 
			if(tmp.getID() == id.getVal()) {
				return tmp;
			}
		}
		throw new IllegalArgumentException("Id "+id+" is unknown");
	}
	
	
	private static  List<IDNode> allnodes = null;
	
	public static Experiment experiment; 
		
	public NodeSystemPeerSim(String prefix) {
		String tmp[]=prefix.split("\\.");
		protocol_id=Configuration.lookupPid(tmp[tmp.length-1]);
		transport_id=Configuration.getPid(prefix+"."+PAR_TRANSPORT);		
	}
	
	public int getProtocol_id() {
		return protocol_id;
	}


	


	@Override
	protected void build(Experiment e) {
		super.build(e);
	}

	@Override
	public Object clone() {
		NodeSystemPeerSim res = null;
		try { 
			res = (NodeSystemPeerSim) super.clone();
			res.me=CommonState.getNode();
			res.my_id=IDNode.get(res.me.getID());					
		}
		catch(CloneNotSupportedException e ) {e.printStackTrace();} // never happens
		return res;
	}
	
	@Override
	public void scheduleAction(Action a, boolean fornow) {
		if(fornow) {
			EDSimulator.add(0, a, me,  getProtocol_id());
			return;
		}
		
		long start = a.getStart().get(random());
		long currenttime=getCurrentTime();
		if(currenttime > start) return;
		EDSimulator.add(start-currenttime, a, me,  getProtocol_id());
	}
	
	@Override
	public void scheduleWaitingConditionChecking(long date) {
		long start = date;
		long currenttime=getCurrentTime();
		if(currenttime > start) return;
		EDSimulator.add(start-currenttime, WaitingConditionCheckingEvent.get(), me,  getProtocol_id());
		
	}
	
	@Override
	public int systemsize() {
		return Network.size();
	}

	

	@Override
	public List<IDNode> allnodes() {
		
		if(allnodes == null) {
			allnodes=new ArrayList<>();
			for(int i=0 ; i< Network.size();i++) {
				allnodes.add(IDNode.get(Network.get(i).getID()));
			}
		}
		
		return allnodes;
	}

	
	@Override
	public void endRootEvent(CallEvent e) {
		CallActionExecution cae = call_action_execution.remove(e);
		if(cae != null) {
			long latency = cae.getCurrentCall().getLatency().get(random());
			if(latency < 1) {
				latency=1;
			}
			EDSimulator.add(latency, cae, me, protocol_id);
		}
	}
	
	
	private Map<CallEvent,CallActionExecution> call_action_execution = new HashMap<>();
	
	
	@Override
	public void processEvent(Node node, int pid, Object event) {
		if(pid != protocol_id)
			throw new IllegalArgumentException("an event of pid "+pid+" has been delivered on protocol "+protocol_id);
		if(node != me) 
			throw new IllegalArgumentException("Event delivered on node "+me.getID()+" but it was basically for node "+node.getID());		
		
		if(event instanceof Message) {
			if(!is_alive) return;
			Message<?> message = (Message<?>)event;
			processRootEvent(new MessageReceptionEvent(message, this, getNodeProto(message.getNameProto())));
		}else if(event instanceof Action) {
			
			if(event instanceof CallAction) {
				CallAction ca = (CallAction) event;
				processEvent(node, pid, new CallActionExecution(ca));
			}else if(event instanceof CrashAction) {
				if(!is_alive) return;
				is_alive=false;
				CrashAction crashaction = (CrashAction) event;
				getLocalReport().addCarefully(new LogCrashEvent(this));
				if(crashaction.getDuration() != null) {
					long arg0=crashaction.getDuration().get(random());
					Object arg1=recover_event;
					Node arg2=node;
					int arg3=pid;
					EDSimulator.add(arg0, arg1, arg2, arg3);
				}
				
			}else {
				throw new IllegalArgumentException(" An unexpected type action has been delivered on Node "+me.getID()+" procotol "+protocol_id);
			}
		}else if(event instanceof String) {
			String event_string = (String) event;
			if(event_string.equals(recover_event)){
				is_alive=true;
				getLocalReport().addCarefully(new LogRecoverEvent(this));
				
			}else {
				throw new IllegalArgumentException("An unexpected string event has been delivered on Node "+me.getID()+" procotol "+protocol_id);
			}
		}else if(event instanceof CallActionExecution) {
			if(!is_alive) return;
			CallActionExecution cae = (CallActionExecution) event;
			Call call = cae.nextCall();
			if(call != null) {
				PrimitiveCallEvent callevent = new PrimitiveCallEvent(getNodeProto(call.getNodeProtoName()),
						call.getPrimitiveName(),Stream.of(call.getArgs()).map( s -> s.get(random())).toArray());
				
				call_action_execution.put(callevent, cae);
				processRootEvent(callevent);				
			}
			
		}else if(event instanceof WaitingConditionCheckingEvent) {
			processRootEvent((Event) event);
		}else {
			throw new IllegalArgumentException("An unexpected type object has been delivered on Node "+me.getID()+" procotol "+protocol_id);
		}	
	}


	@Override
	public IDNode idnode() {
		return my_id;
	}


	@Override
	public Random random() {
		return CommonState.r;
	}





	@Override
	public long getCurrentTime() {
		return CommonState.getTime();
	}

	
	private static Map<Node,NodeSystemPeerSim> map= null;
	
	
	
	public static Map<Node,NodeSystemPeerSim> getAll(){
		if(map==null) {
			map=new HashMap<>();
			for(int i=0; i<Network.size();i++) {
				Node tmp = Network.get(i);
				boolean found=false;
				for(int j=0; j < tmp.protocolSize();j++) {
					Protocol proto = tmp.getProtocol(j);
					if(proto instanceof NodeSystemPeerSim) {
						NodeSystemPeerSim target = (NodeSystemPeerSim) proto;
						map.put(tmp,target);
						found=true;
						break;
					}
				}
				if(!found)
					throw new RuntimeException("NodeSystemPeerSim not found on node "+tmp.getID());
				
			}
		}
		return map;
	}


	@Override
	protected void sendReal(Message<?> message) {
		if(me != getNodeFromIDNode(message.getSrc())) 
			throw new IllegalArgumentException("Node "+me.getID()+" has to send a message where source = "+message.getSrc());
		if(!is_alive) return;
		Node dest = getNodeFromIDNode(message.getDest());
		Transport t = (Transport)me.getProtocol(transport_id);
		
		t.send(me, dest, message , protocol_id);
	}

}
