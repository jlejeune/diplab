package diplab.system.peersim.transport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import peersim.config.Configuration;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.transport.Transport;

public class FIFOTransport implements Transport, EDProtocol {

	private static final String PAR_TRANSPORT = "transport";
	
	private final int transport;
	private final int protocol_id;
	
	private Map<Long, Integer> sending_cpt;
	private Map<Long, Integer> next_expected_num_seq;
	private List<FIFOMessage> pendMsg;
	
	public FIFOTransport(String prefix) {
		String tmp[] = prefix.split("\\.");
		protocol_id = Configuration.lookupPid(tmp[tmp.length - 1]);
		transport = Configuration.getPid(prefix + "." + PAR_TRANSPORT);

	}
	
	@Override
	public Object clone() {
		FIFOTransport res = null;
		try {
			res = (FIFOTransport) super.clone();
			res.sending_cpt = new HashMap<Long, Integer>();
			res.next_expected_num_seq = new HashMap<Long, Integer>();
			res.pendMsg = new ArrayList<FIFOMessage>();
		} catch (CloneNotSupportedException e) {
		} // never happens
		return res;
	}
	
	@Override
	public void send(Node src, Node dest, Object msg, int pid) {
		Transport t = (Transport) src.getProtocol(transport);
		if (!this.sending_cpt.containsKey(dest.getID())) {
			this.sending_cpt.put(dest.getID(), 0);
		}
		int numseq = sending_cpt.get(dest.getID());
		t.send(src, dest, new FIFOMessage(src.getID(), dest.getID(), msg, pid, numseq), protocol_id);
		sending_cpt.put(dest.getID(), numseq + 1);

	}

	@Override
	public long getLatency(Node src, Node dest) {
		Transport t = (Transport) src.getProtocol(transport);
		return t.getLatency(src, dest);
	}

	@Override
	public void processEvent(Node node, int pid, Object event) {
		if (protocol_id != pid) {
			throw new IllegalStateException("Receive an event for wrong protocol");
		}
		if (! (event instanceof FIFOMessage)) {
			throw new IllegalStateException("Receive unknown type event");
		}
		FIFOMessage fifomess_received = (FIFOMessage) event;
		if(node.getID() != fifomess_received.getIdDest()) {
			throw new IllegalStateException("Receive a message but wrong destination");
		}
		pendMsg.add(fifomess_received);
		long id_sender = fifomess_received.getIdSrc();
		if (!next_expected_num_seq.containsKey(id_sender)) {
			next_expected_num_seq.put(fifomess_received.getIdSrc(), 0);
		}
		boolean fini = false;
		while (!fini) {
			fini = true;
			int i = 0;
			for (FIFOMessage fifomess : pendMsg) {

				if (fifomess.getIdSrc() == id_sender) {
					int next = next_expected_num_seq.get(id_sender);
					if (fifomess.getNumseq() == next) {

						Object content = fifomess.getMessage();

						((EDProtocol) node.getProtocol(fifomess.getPidDest())).processEvent(node, fifomess.getPidDest(),
								content);

						next_expected_num_seq.put(id_sender, next + 1);
						pendMsg.remove(i);
						fini = false;
						break;
					}
				}
				i++;
			}

		}
	}
	
	
	private static class FIFOMessage  {

		private final long idsrc;
		private final long iddest;
		
		private final int numseq;
		private final Object message;
		private final int pidDest;

	
		public long getIdSrc() {
			return idsrc;
		}


		public long getIdDest() {
			return iddest;
		}


		public int getNumseq() {
			return numseq;
		}


		public Object getMessage() {
			return message;
		}


		public int getPidDest() {
			return pidDest;
		}


		public FIFOMessage(long idsrc, long iddest,  Object message, int pidDest, int numseq) {
			this.idsrc=idsrc;
			this.iddest=iddest;
			this.pidDest=pidDest;
			this.message = message;
			this.numseq = numseq;

		}
	}

	
}
