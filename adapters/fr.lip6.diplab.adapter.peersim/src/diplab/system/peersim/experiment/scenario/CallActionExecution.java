package diplab.system.peersim.experiment.scenario;

import java.util.Iterator;

import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.callscheme.Call;

public  class CallActionExecution{
	
	private final Iterator<Call> it;
	
	private Call current;
	
	public CallActionExecution(CallAction ca) {
		it= ca.getCallscheme().iterator();
	}
	
	public Call getCurrentCall() {
		return current;
	}
			
	public Call nextCall(){
		if(it.hasNext())
			current = it.next();
		else
			current=null;
		return current;
	}		
}
