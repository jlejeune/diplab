package diplab.system.peersim.experiment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import diplab.core.IDNode;
import diplab.core.experiment.Deterministic;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.ExperimentController;
import diplab.core.experiment.reporting.Report;
import diplab.system.peersim.Initialisator;
import diplab.system.peersim.NodeSystemPeerSim;
import peersim.Simulator;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.GeneralNode;
import peersim.core.Network;
import peersim.edsim.EDSimulator;
import peersim.util.ExtendedRandom;

@Deterministic
public class PeerSimExperimentController implements ExperimentController {

	public static final String PAR_PEERSIM_PATH_CONFIGFILE="experiment.peersim.configfile";
	
	
	
	public static Experiment experiment=null;
	private static boolean simulator_already_used=false;
	@Override
	public Map<IDNode,Report> execute( Experiment e) {
		experiment=e;	
				
		File main_file_config = new File(System.getProperty("java.io.tmpdir")+"/config");	
		if(main_file_config.exists()) {
			main_file_config.delete();
		}
		NodeSystemPeerSim.experiment=e;	
		try (PrintStream out = new PrintStream(new FileOutputStream(main_file_config))){
		
			out.println("simulation.experiments 1");
			out.println("random.seed "+e.getSeed());
			out.println("simulation.endtime "+e.getExperimentDuration());
			out.println("network.size "+e.getNbNode());
			
			out.println("protocol.system "+NodeSystemPeerSim.class.getName());
						
			
						
			out.println("protocol.system."+NodeSystemPeerSim.PAR_TRANSPORT+" transport");	
						
			out.println("init.initialisator "+Initialisator.class.getName());
						
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		}
		setValue(Configuration.class, "config", null);
		if(simulator_already_used)
			resetSimulator(e.getSeed());
		
		String config_file=(String)e.getProperties().get(PAR_PEERSIM_PATH_CONFIGFILE);
		String args[];
		if(config_file == null)
			args=new String[]{main_file_config.getAbsolutePath()};
		else
			args=new String[]{main_file_config.getAbsolutePath(),config_file};
	
		Simulator.main(args);
		simulator_already_used=true;
				
		Map<IDNode,Report> res = new HashMap<>();
		for(NodeSystemPeerSim node : NodeSystemPeerSim.getAll().values()) {
			res.put(node.idnode(), node.getLocalReport());
		}
			
		return res;
	}

	private static void resetSimulator(int seed) {
		setValue(Configuration.class, "config", null);
		
		setValue(EDSimulator.class, "endtime", 0);
		setValue(EDSimulator.class, "logtime", 0);
		setValue(EDSimulator.class, "controls", null);
		setValue(EDSimulator.class, "ctrlSchedules", null);
		setValue(EDSimulator.class, "heap", null);
		setValue(EDSimulator.class, "nextlog", 0);

		
		setValue(CommonState.class, "time", 0);
		setValue(CommonState.class, "endtime", -1);
		setValue(CommonState.class, "toshift", -1);
		setValue(CommonState.class, "phase", CommonState.PHASE_UNKNOWN);
		setValue(CommonState.class, "pid", 0);
		setValue(CommonState.class, "node", null);
		setValue(CommonState.class, "r", new ExtendedRandom(seed));
		
		setValue(GeneralNode.class, "counterID", -1);
			
		setValue(Network.class, "node", null);
		setValue(Network.class, "len", 0);
		setValue(Network.class, "prototype", null);
		
		
		setValue(NodeSystemPeerSim.class, "map", null);
		setValue(NodeSystemPeerSim.class,"allnodes", null);
		
	}
	
	private static void setValue(Class<?> cl, String name, Object value) {
		try {
			Field config = cl.getDeclaredField(name);
			config.setAccessible(true);
			config.set(null, value);
			config.setAccessible(false);
			
		} catch (Exception e1) {
			throw new IllegalStateException(e1);
		}
	}

		

}
