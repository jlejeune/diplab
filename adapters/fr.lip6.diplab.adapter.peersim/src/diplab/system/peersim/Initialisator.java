package diplab.system.peersim;

import java.util.Map.Entry;

import diplab.core.experiment.scenario.action.Action;
import diplab.system.peersim.experiment.PeerSimExperimentController;
import peersim.core.Control;
import peersim.core.Node;

public class Initialisator implements Control {

	public static final String PAR_NODEPROTO = "nodeproto";
	public static final String PAR_MEMBERS_NODEPROTO = "members";
	
    public Initialisator(String prefix) {
     	
    }
	
	@Override
	public boolean execute() {
		
		
		for(Entry<Node,NodeSystemPeerSim> e : NodeSystemPeerSim.getAll().entrySet()) {
			e.getValue().build(PeerSimExperimentController.experiment);
		}
		
		
		
		for(Entry<Node,NodeSystemPeerSim> e : NodeSystemPeerSim.getAll().entrySet()) {
			NodeSystemPeerSim proto = e.getValue();
			for(Action a : PeerSimExperimentController.experiment.getScenario()) {
				if(a.containsTarget(proto.idnode())){
					proto.scheduleAction(a,false);			
				}
			}
			
			proto.getNodeProtos().stream().forEach(p -> p.init());			
		}			
		return false;
	}

}
