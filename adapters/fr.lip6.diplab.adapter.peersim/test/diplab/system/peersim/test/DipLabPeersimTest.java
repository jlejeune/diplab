package diplab.system.peersim.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import diplab.adaptervalidator.DipLabTest;
import diplab.adaptervalidator.broadcast.TestCallBroadcast;
import diplab.adaptervalidator.broadcast.TestReliableBroadcast;
import diplab.adaptervalidator.hello.TestHello;
import diplab.adaptervalidator.helloasync.TestHelloAsync;
import diplab.adaptervalidator.helloring.TestHelloRing;
import diplab.adaptervalidator.helloring.TestHelloRingCrash;
import diplab.adaptervalidator.hellosleep.TestHelloSleep;
import diplab.adaptervalidator.hellowait.TestHelloWait;
import diplab.adaptervalidator.multialgo.TestMultiAlgo;
import diplab.adaptervalidator.naimitrehel.TestNT;
import diplab.core.experiment.ExperimentBuilder;
import diplab.core.experiment.ExperimentController;
import diplab.system.peersim.experiment.PeerSimExperimentController;
import peersim.transport.UniformRandomTransport;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DipLabPeersimTest extends DipLabTest{

	
	static final Class<? extends ExperimentController> controler_class = PeerSimExperimentController.class;
	
	@Test
	public void a_testHello() {
		test(controler_class, TestHello.class, 05, 15000L, 2);
		test(controler_class, TestHello.class, 10, 30000L, 2);
		test(controler_class, TestHello.class, 25, 70000L, 2);
	}
	
	@Test
	public void b_testHelloRing() {
		test(controler_class, TestHelloRing.class, 05, 15000L, 2);
		test(controler_class, TestHelloRing.class, 10, 30000L, 2);
		test(controler_class, TestHelloRing.class, 25, 70000L, 2);
	}

	@Test
	public void c_testHelloRingCrash() {
		test(controler_class, TestHelloRingCrash.class, 05, 15000L, 2);
		test(controler_class, TestHelloRingCrash.class, 10, 30000L, 2);
		test(controler_class, TestHelloRingCrash.class, 25, 70000L, 2);
	}
	
	@Test
	public void d_testHelloWait() {
		test(controler_class, TestHelloWait.class, 05, 15000L, 2);
		test(controler_class, TestHelloWait.class, 10, 30000L, 2);
		test(controler_class, TestHelloWait.class, 25, 70000L, 2);
	}
	
	@Test
	public void da_testHelloAsync() {
		test(controler_class, TestHelloAsync.class, 05, 15000L, 2);
		test(controler_class, TestHelloAsync.class, 10, 30000L, 2);
		test(controler_class, TestHelloAsync.class, 25, 70000L, 2);
	}
	
	@Test
	public void db_testHelloSleep() {
		test(controler_class, TestHelloSleep.class, 05, 15000L, 2);
		test(controler_class, TestHelloSleep.class, 10, 30000L, 2);
		test(controler_class, TestHelloSleep.class, 25, 70000L, 2);
	}
	
	@Test
	public void e_testMultiAlgo() {
		test(controler_class, TestMultiAlgo.class, 05, 15000L, 2);
		test(controler_class, TestMultiAlgo.class, 10, 30000L, 2);
		test(controler_class, TestMultiAlgo.class, 25, 70000L, 2);
	}
	
	@Test
	public void f_testCallBroadcast() {
		test(controler_class, TestCallBroadcast.class, 05, 15000L, 2);
		test(controler_class, TestCallBroadcast.class, 10, 30000L, 2);
		test(controler_class, TestCallBroadcast.class, 25, 70000L, 2);
	}
	
	@Test
	public void g_testReliableBroadcast() {
		test(controler_class, TestReliableBroadcast.class, 05, 15000L, 2);
		test(controler_class, TestReliableBroadcast.class, 10, 30000L, 2);
		test(controler_class, TestReliableBroadcast.class, 25, 70000L, 2);
	}
	
	@Test
	public void f_testNT() {
		test(controler_class, TestNT.class, 05, 15000L, 2);
		test(controler_class, TestNT.class, 10, 30000L, 2);
		test(controler_class, TestNT.class, 25, 70000L, 2);
	}
	
	@Override
	public void generateConfig( ExperimentBuilder builder) {
		File conffile = new File(System.getProperty("java.io.tmpdir")+"/peersimnetwork.conf");
		if(conffile.exists()) conffile.delete();
		try(PrintStream out2 = new PrintStream(new FileOutputStream(conffile,true))){
			out2.println("protocol.transport "+UniformRandomTransport.class.getName());
			out2.println("protocol.transport.mindelay 10 ");
			out2.println("protocol.transport.maxdelay 40");
		} catch (FileNotFoundException e) {
			throw new IllegalStateException(e);
		}
		builder.addProperty(PeerSimExperimentController.PAR_PEERSIM_PATH_CONFIGFILE,conffile.getAbsolutePath());
		
	}

}
