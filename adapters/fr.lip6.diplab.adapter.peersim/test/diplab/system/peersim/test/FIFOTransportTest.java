package diplab.system.peersim.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.Primitive;
import diplab.core.experiment.ExperimentBuilder;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.Reports;
import diplab.core.experiment.reporting.logevent.LogMessageReceiveEvent;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;
import diplab.core.experiment.reporting.visitor.selector.LogMessageEventSelector;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.callscheme.Call;
import diplab.core.messaging.IntMessage;
import diplab.core.messaging.MessageHandler;
import diplab.system.peersim.experiment.PeerSimExperimentController;
import diplab.system.peersim.transport.FIFOTransport;
import peersim.transport.UniformRandomTransport;

public class FIFOTransportTest {

	private ExperimentBuilder builder;
	
	@Before
	public void setup() {
		builder = new ExperimentBuilder(PeerSimExperimentController.class, 600000);
		File conffile = new File(System.getProperty("java.io.tmpdir")+"/peersimnetwork.conf");
		if(conffile.exists()) conffile.delete();
		try(PrintStream out2 = new PrintStream(new FileOutputStream(conffile,true))){
			out2.println("protocol.transport "+FIFOTransport.class.getName());
			out2.println("protocol.transport.transport basictransport");
			
			out2.println("protocol.basictransport  "+UniformRandomTransport.class.getName());
			out2.println("protocol.basictransport.mindelay 10 ");
			out2.println("protocol.basictransport.maxdelay 5000");
		} catch (FileNotFoundException e) {
			throw new IllegalStateException(e);
		}
		builder.addProperty(PeerSimExperimentController.PAR_PEERSIM_PATH_CONFIGFILE,conffile.getAbsolutePath());
	}
	
	@Test
	public void test() {
		int nbmessage=300;
		builder.withSystemSize(2);
		builder.addNodeProto("mock", ProtoSendRaffale.class);
		builder.addScenarioAction(
				new CallAction(
						(r)->10L,
						new Call("mock", "f", r->0L, r->nbmessage),
						IDNode.get(1)
						)
				);
		Report r = Reports.merge(builder.build().execute().values());
		LogMessageEventSelector selector = new LogMessageEventSelector(IDNode.get(0),"mock", IntMessage.class); 
		
		Report rmess = r.subreport(selector);
		
		rmess.analyse(new LogEventVisitor() {

			int expected=0;
			
			@Override
			public void visit(LogMessageReceiveEvent e) {
				assertEquals(expected,e.getMessage().getContent());
				expected++;
			}
			
		});
		
		
		
	}
	
	public static  class ProtoSendRaffale extends NodeProto{

		public ProtoSendRaffale(NodeSystem system) {
			super(system);
		}
		
		@Primitive
		public void f(Integer nbmess) {
			for(int i = 0 ; i<nbmess;i++) {
				send(IntMessage.class, IDNode.get(0), i);
			}
		}
		
		@MessageHandler(IntMessage.class)
		public void receive(IntMessage m) {
			
		}
		
	}

}
