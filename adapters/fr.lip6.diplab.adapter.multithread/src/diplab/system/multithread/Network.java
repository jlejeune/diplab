package diplab.system.multithread;

import diplab.core.messaging.Message;

public interface Network {

	public void start(boolean waitready) ;

	public void stop(boolean waitend);

	public void sendReal(Message<?> message) ;

	public void crash(MultithreadNodeSystem multithreadNodeSystem);

	public void recover(MultithreadNodeSystem multithreadNodeSystem);

	public void nomoreaction(MultithreadNodeSystem multithreadNodeSystem);

	public void nomoreEvents(MultithreadNodeSystem multithreadNodeSystem);

	

}
