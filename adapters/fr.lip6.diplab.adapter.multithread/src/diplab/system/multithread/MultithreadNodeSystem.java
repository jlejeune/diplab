package diplab.system.multithread;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.event.CallEvent;
import diplab.core.event.PrimitiveCallEvent;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.logevent.LogCrashEvent;
import diplab.core.experiment.reporting.logevent.LogRecoverEvent;
import diplab.core.experiment.scenario.action.Action;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.CrashAction;
import diplab.core.experiment.scenario.action.callscheme.Call;
import diplab.core.messaging.Message;

public class MultithreadNodeSystem extends NodeSystem {
		
	
	private final AtomicBoolean crashed = new AtomicBoolean(false);
	
	private final Timer timer ;
	
	private final EventProcessor eventprocessor;
	
	

	private final Network network;
	
	private final List<Thread> thread_actions_processors = new ArrayList<>();
	private final Experiment experiment;
	
	private boolean node_started=false;
	private boolean node_finished=false;

	private final List<IDNode> allnodes;
	private final IDNode myid;
	
	private final AtomicInteger nb_pending_action=new AtomicInteger(0);
	
	//another report to log crah and recover events which may be added concurrently with call events
	private final Report healthreport;
	
	public MultithreadNodeSystem(Experiment experiment, long starttime,
			IDNode myid,
			List<IDNode> allnodes,
			Network network) {
		
		this.starttime=starttime;
		this.network=network;
		this.experiment = experiment;
		this.myid=myid;
		this.allnodes=new ArrayList<IDNode>(allnodes);
		
		eventprocessor=new EventProcessor(this);
		timer= new Timer("Timer-action-scheduling-on-node-"+idnode());
		this.healthreport=experiment.getReportfactory().createNewReport(experiment, "health-"+idnode());
		build(experiment);
		
	}
	
	
		
	public boolean start() {
		if(isFinished()) throw new IllegalStateException("already finished");
		if(isStarted()) return false;
		
		
		network.start(true);
		
		eventprocessor.startAndEnsureReady();
		boolean hasaction=false;		
		for(Action a : experiment.getScenario()) {
			if(a.containsTarget(idnode())){
				hasaction=true;
				scheduleAction(a,false);			
			}
			
		}
		if(!hasaction) network.nomoreaction(this);
		getNodeProtos().stream().forEach(p -> p.init());	
		node_started=true;
		return true;
	}
	
	@Override
	public void scheduleAction(Action a, boolean fornow) {
		long start = a.getStart().get(random());
		long currenttime=getCurrentTime();
		
		nb_pending_action.incrementAndGet();
		
		TimerTask timertask = new TimerTask(){
			public void run() {
				synchronized(thread_actions_processors) {
					if(isFinished()) {
						System.out.println(idnode()+": Warning action (expectedStart = "+start+") not executed because the system has finished");
						return;
					}
					Thread t = new ActionProcessor(a,start);
					thread_actions_processors.add(t);
					t.start();
				}
			}
		};
		
		if(fornow || currenttime > start) {
			if(currenttime > start) System.out.println(idnode()+" Warning execute an action after its expected start time (currenttime = "+currenttime+" expectedStart = "+start+")");
			timertask.run();
			return;
		}
		synchronized(thread_actions_processors) {
			if(isFinished()){
				System.out.println(idnode()+": Warning action not scheduled because the system has finished");
				return;
			}
			timer.schedule(timertask,start-currenttime);
		}
	}
	
	@Override
	public void scheduleWaitingConditionChecking(long date) {
		long start = date;
		long currenttime=getCurrentTime();
		if(currenttime > start) return;
		synchronized(thread_actions_processors) {
			if(isFinished()){
				System.out.println(idnode()+": Warning WaitingConditionChecking not scheduled at date "+date+" because the system has finished");
				return;
			}
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					try {
						eventprocessor.putWaitingConditionCheckingEvent();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}	
				}
			},start-currenttime);		
		}
		
	}
	
	public EventProcessor getEventprocessor() {
		return eventprocessor;
	}

	
	public boolean isStarted() {
		return node_started;
	}
	
	public boolean isFinished() {
		return node_finished;
	}
	
	
	
	public boolean stop() {
		if(isFinished()) throw new IllegalStateException("MultithreadNodeSystem "+idnode()+"already finished");
		if(!isStarted()) return false;
		
		network.stop(true);
		
		eventprocessor.interrupt();
		try {
			eventprocessor.join();
		} catch (InterruptedException e1) {
			System.err.println("Interrupted when joining eventprocessor of MultithreadNodeSystem "+idnode());
		}
		
		
		synchronized(thread_actions_processors) {
			node_finished=true;
			System.err.println(idnode()+": stopping MultithreadNodeSystem");
			timer.cancel();
			for(Thread t : thread_actions_processors) {
				if(t.isAlive()) {
					t.interrupt();
				}
			}
		}
		for(Thread t : thread_actions_processors) {
			try {
				t.join();
			} catch (InterruptedException e) {
				System.err.println("Interrupted when joining actionprocessor of MultithreadNodeSystem "+idnode());
			}
		}
		return true;
		
	}
		
	@Override
	public void endRootEvent(CallEvent e) {
		synchronized(e) {
			e.notify();
		}
	}
	
	
	private class ActionProcessor extends Thread {

		private final Action a;
				
		public ActionProcessor(Action a, long starttime) {
			this.a=a;
			this.setName("Action-Processor-on-node-"+idnode()+"-at-"+starttime);
		}

		@Override
		public void run() {
			try {
				if(a instanceof CallAction) {
					CallAction ca = (CallAction) a;
					if(crashed.get()) {
						return;
					}
					for(Call call : ca.getCallscheme()) {
						NodeProto nodeproto = getNodeProto(call.getNodeProtoName());
						PrimitiveCallEvent callevent = new PrimitiveCallEvent(nodeproto,
								call.getPrimitiveName(),Stream.of(call.getArgs()).map( s -> s.get(random())).toArray());
						eventprocessor.putEvent(callevent);
						synchronized(callevent) {
							callevent.wait();
						}
						Thread.sleep(call.getLatency().get(random()));	
					}
					
				}else if(a instanceof CrashAction) {
					if(crashed.get()) return;
					CrashAction ca = (CrashAction) a;
					crashed.set(true);
					getHealthReport().addCarefully(new LogCrashEvent(MultithreadNodeSystem.this));
					eventprocessor.clearAllEvent();
					network.crash(MultithreadNodeSystem.this);
					if(ca.getDuration() != null ) {
						Thread.sleep(ca.getDuration().get(random()));
						crashed.set(false);
						getHealthReport().addCarefully(new LogRecoverEvent(MultithreadNodeSystem.this));
						network.recover(MultithreadNodeSystem.this);
					}
					
				}else {
					throw new IllegalArgumentException("Action "+a+" has an unknown type");
				}
			}catch(InterruptedException ie) {
				System.err.println("Action Processor interrupted on node "+idnode());
			}finally {
				int val = nb_pending_action.decrementAndGet();
				if(val==0) network.nomoreaction(MultithreadNodeSystem.this);
			}

		}

	}
	
	
	public Report getHealthReport() {
		return healthreport;
	}
	
	
	@Override
	public int systemsize() {
		return allnodes.size();
	}

	@Override
	public IDNode idnode() {
		return myid;
	}

	@Override
	public  void sendReal(Message<?> message) {
		network.sendReal(message);
	}
	
	@Override
	public List<IDNode> allnodes() {
		return allnodes;
	}

	
	private Random rand=null;
	private final long starttime;
	@Override
	public Random random() {
		if(rand == null) {
			rand=new Random(idnode().getVal()+experiment.getSeed());
		}
		return rand;
	}
	
	@Override
	public long getCurrentTime() {
		return System.currentTimeMillis() - starttime;
	}



	public void nomoreEvents() {
		network.nomoreEvents(this);
	}

}
