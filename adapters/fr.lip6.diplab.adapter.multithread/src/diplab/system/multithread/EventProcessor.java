package diplab.system.multithread;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import diplab.core.event.Event;
import diplab.core.event.WaitingConditionCheckingEvent;

public class EventProcessor extends Thread {

	private boolean ready=false;
	
	private final BlockingQueue<Event> events  = new LinkedBlockingQueue<>();
	
	private final MultithreadNodeSystem nodesystem;
	
	public EventProcessor(MultithreadNodeSystem nodesystem) {
		this.nodesystem=nodesystem;
		this.setName("Event-Processor-"+nodesystem.idnode());
	}
	
	@Override
	public void run() {
		synchronized(this) {
			ready=true;
			this.notifyAll();
		}
		try {
			while(!isInterrupted()) {
				nodesystem.processRootEvent(events.take());
				if(events.isEmpty()) {
					nodesystem.nomoreEvents();
				}
				
				
				
			}
		}catch(InterruptedException ie) {
			System.err.println(nodesystem.idnode()+" Event processor  interrupted (nb events remaining = "+events.size()+")");
		}
		
		
	}

	public synchronized void startAndEnsureReady() {
		start();
		try {
			while(!ready) {
				wait();
			}
		}catch(InterruptedException ie) {
			System.err.println("Error when starting event processor on node "+nodesystem.idnode());
		}
		
	}

	public void putWaitingConditionCheckingEvent() throws InterruptedException {
		events.put(WaitingConditionCheckingEvent.get());
	}

	public void putEvent(Event event) throws InterruptedException {
		events.put(event);
		
	}

	public int nbPendingEvents() {
		return events.size();
	}
	
	public void clearAllEvent() {
		events.clear();
		nodesystem.nomoreEvents();
	}	
}
