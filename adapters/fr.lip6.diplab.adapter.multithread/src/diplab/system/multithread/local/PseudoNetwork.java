package diplab.system.multithread.local;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import diplab.core.IDNode;
import diplab.core.NodeSystem;
import diplab.core.event.MessageReceptionEvent;
import diplab.core.messaging.Message;
import diplab.system.multithread.MultithreadNodeSystem;
import diplab.system.multithread.Network;
import diplab.util.RandomUtil;

public class PseudoNetwork implements Network {


	private final Map<IDNode, MultithreadNodeSystem> distributedSystem;
	private final Map<IDNode,AtomicBoolean> crashed;
	private final AtomicInteger nb_nodewitheaction;

	private long meanlatency = 20;
	private Timer channels;
	private boolean stopped=false;
	private final Object lockend;
	private final AtomicInteger nbmessagespending = new AtomicInteger(0);

	public PseudoNetwork(Object lockend) {
		this.distributedSystem = new HashMap<IDNode, MultithreadNodeSystem>();
		this.crashed = new HashMap<>();
		this.nb_nodewitheaction=new AtomicInteger();
		this.lockend=lockend;
	}

	public void putAll(Map<IDNode, MultithreadNodeSystem> nodes) {
		this.distributedSystem.putAll(nodes);
		distributedSystem.keySet().stream().forEach(id-> { 
			crashed.put(id, new AtomicBoolean(false));
		});
		nb_nodewitheaction.set(nodes.size());

	}
	
	@Override
	public void sendReal(Message<?> message) {
		if(crashed.get(message.getSrc()).get()) return;
		synchronized(channels) {
			if(stopped) {
				System.err.println("Warning : PseudoNetwork already stopped ->  message from "+message.getSrc()+" to "+message.getDest()+" containing "+message.getContent().getClass().getSimpleName()+" is not sent");
				return;
			}
			nbmessagespending.incrementAndGet();
			channels.schedule(new TimerTask() {
	
				@Override
				public void run() {
					if(!crashed.get(message.getDest()).get()) {
						NodeSystem destsystem = distributedSystem.get(message.getDest());
						try {
							distributedSystem.get(message.getDest())
											.getEventprocessor()
											.putEvent(
													new MessageReceptionEvent(message, destsystem, destsystem.getNodeProto(message.getNameProto()))
															);
						} catch (InterruptedException e) {
							System.err.println("Thread "+Thread.currentThread().getName()+" received InteruptedException when adding a MessageReceptionEvent to "+message.getDest());
						}
					}
					nbmessagespending.decrementAndGet();
					checkend();
				}
			}, RandomUtil.nextPoisson(distributedSystem.get(message.getSrc()).random(), meanlatency));
		}
	}

	@Override
	public void start(boolean waitready) {
		channels = new Timer("Channels",true);
	}

	@Override
	public void stop(boolean waitend) {
		synchronized(channels) {
			if(stopped) {
				//System.err.println("Warning : PseudoNetwork already stopped -> stop() calling ignored");
				return;
			}
			stopped=true;
			channels.cancel();
		}
		
	}


	@Override
	public void crash(MultithreadNodeSystem multithreadNodeSystem) {
		crashed.get(multithreadNodeSystem.idnode()).set(true);
	}


	@Override
	public void recover(MultithreadNodeSystem multithreadNodeSystem) {
		crashed.get(multithreadNodeSystem.idnode()).set(false);
	}

	@Override
	public void nomoreaction(MultithreadNodeSystem multithreadNodeSystem) {
		System.err.println(multithreadNodeSystem.idnode()+" : no more action");
		nb_nodewitheaction.decrementAndGet();
		checkend();
		
	}

	private void checkend() {
		if(nb_nodewitheaction.get() == 0 
				&& nbmessagespending.get() == 0
				&& distributedSystem.values().stream().map(s->s.getEventprocessor().nbPendingEvents()).reduce(0, (a,b)->a+b) == 0
			) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			synchronized(lockend) {
				lockend.notify();
			}
		}
		
	}

	@Override
	public void nomoreEvents(MultithreadNodeSystem multithreadNodeSystem) {
		checkend();		
	}

}
