package diplab.system.multithread.local;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import diplab.core.IDNode;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.ExperimentController;
import diplab.core.experiment.reporting.Report;
import diplab.system.multithread.MultithreadNodeSystem;

public class PseudoDistributedExperimentController implements ExperimentController {

	private final long starttime;
	
	
	public PseudoDistributedExperimentController() {
		starttime = System.currentTimeMillis();
	}
	
	@Override
	public Map<IDNode, Report> execute(Experiment e) {
		final Object lockend = new Object();	
		Map<IDNode, MultithreadNodeSystem> theSystem = new HashMap<IDNode, MultithreadNodeSystem>();
		List<IDNode> idnodes = IntStream.range(0, e.getNbNode()).mapToObj(i-> IDNode.get(i)).collect(Collectors.toList());
		PseudoNetwork network = new PseudoNetwork(lockend);
		for(IDNode id : idnodes) {
			theSystem.put(id, 
					new MultithreadNodeSystem(e, starttime, id, idnodes, network)
					);
		}
		network.putAll(theSystem);
		System.err.println("Starting nodes ..");
		for(MultithreadNodeSystem node : theSystem.values()) {
			node.start();
		}
		synchronized(lockend) {
			try {
				if(!e.getScenario().isEmpty()) {
					lockend.wait(e.getExperimentDuration());
				}else {
					System.err.println("Warning : empty scenario");
				}
					
			} catch (InterruptedException e1) {
				System.err.println("Unexpected interrupted Exception");
			}
		}
		System.err.println("Stopping nodes..");
		for(MultithreadNodeSystem node : theSystem.values()) {
			node.stop();
		}
		System.err.println("Retrieving reports");
		Map<IDNode, Report> res = new HashMap<IDNode, Report>();
		for(MultithreadNodeSystem node : theSystem.values()) {
			Report local = node.getLocalReport();
			Report health = node.getHealthReport();
			if(health.size()> 0) {
				System.err.println(node.idnode()+" has a health report (size = "+health.size()+")");
				local=local.merge(health, local.getName());
			}
			res.put(node.idnode(), local );
			System.err.println("Report of "+node.idnode()+" done");
		}
		
		
		return res;
	}

}
