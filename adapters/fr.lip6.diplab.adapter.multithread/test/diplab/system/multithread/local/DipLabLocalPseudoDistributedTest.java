package diplab.system.multithread.local;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import diplab.adaptervalidator.DipLabTest;
import diplab.adaptervalidator.broadcast.TestCallBroadcast;
import diplab.adaptervalidator.broadcast.TestReliableBroadcast;
import diplab.adaptervalidator.hello.TestHello;
import diplab.adaptervalidator.helloasync.TestHelloAsync;
import diplab.adaptervalidator.helloring.TestHelloRing;
import diplab.adaptervalidator.helloring.TestHelloRingCrash;
import diplab.adaptervalidator.hellosleep.TestHelloSleep;
import diplab.adaptervalidator.hellowait.TestHelloWait;
import diplab.adaptervalidator.multialgo.TestMultiAlgo;
import diplab.adaptervalidator.naimitrehel.TestNT;
import diplab.core.experiment.ExperimentBuilder;
import diplab.core.experiment.ExperimentController;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DipLabLocalPseudoDistributedTest extends DipLabTest{
	
	static final Class<? extends ExperimentController> controler_class = PseudoDistributedExperimentController.class;
	
	@Test
	public void a_testHello() {
		test(controler_class, TestHello.class, 5, 20000L, 1);
	}
	
	@Test
	public void b_testHelloRing() {
		test(controler_class, TestHelloRing.class, 5, 8000L, 1);
	}
	
	@Test
	public void c_testHelloRingCrash() {
		test(controler_class, TestHelloRingCrash.class, 5, 8000L, 1);
	}
	
	@Test
	public void d_testHelloWait() {
		test(controler_class, TestHelloWait.class, 5, 8000L, 1);
	}
	
	@Test
	public void da_testHelloAsync() {
		test(controler_class, TestHelloAsync.class, 5, 15000L, 1);
	}
	
	@Test
	public void db_testHelloSleep() {
		test(controler_class, TestHelloSleep.class, 5, 15000L, 1);
	}
	
	@Test
	public void e_testMultiAlgo() {
		test(controler_class, TestMultiAlgo.class, 5, 20000L, 1);
	}
	
	@Test
	public void f_testCallBroadcast() {
		test(controler_class, TestCallBroadcast.class, 5, 5000L, 1);
	}

	@Test
	public void g_testReliableBroadcast() {
		test(controler_class, TestReliableBroadcast.class, 5, 5000L, 1);
	}
	
	@Test
	public void f_testNT() {
		test(controler_class, TestNT.class, 05, 15000L, 1);
	}
		
	@Override
	public void generateConfig(ExperimentBuilder builder) {
		builder.withProgressIndicator();
	
	}
	
	

}
