package diplab.adapter.socket;

import diplab.core.IDNode;
import diplab.core.messaging.IntMessage;

public class EndMessage extends IntMessage {
	public EndMessage(IDNode src, IDNode dest) {
		super(src, dest, "", -1, 0);
	}

	private static final long serialVersionUID = 1L;

}
