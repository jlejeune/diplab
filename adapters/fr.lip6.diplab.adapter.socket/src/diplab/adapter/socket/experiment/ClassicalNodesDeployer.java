package diplab.adapter.socket.experiment;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import diplab.core.experiment.Experiment;

public class ClassicalNodesDeployer extends NodesDeployer {

	
	public static final String PAR_SOCKET_HOSTFILE="experiment.socket.hostfile";
	public static final String PAR_SOCKET_HOSTS="experiment.socket.hosts";
	
	private List<String> hosts= new ArrayList<String>();
	
	public ClassicalNodesDeployer(Experiment expe, InetSocketAddress controller) throws FileNotFoundException, IOException {
		super(expe, controller);
		String path_hostfile = (String)expe.getProperties().get(PAR_SOCKET_HOSTFILE);
		if(path_hostfile != null ) {
			try(BufferedReader br = new BufferedReader(new FileReader(path_hostfile))){
				String line;
			      while((line = br.readLine()) != null){
			    	  hosts.add(line);
			      }
			} 
		}else {
			String hosts_string = (String)expe.getProperties().getOrDefault(PAR_SOCKET_HOSTS,"localhost");
			hosts = Arrays.asList(hosts_string.split(":"));
		}
		
	}

	@Override
	public List<String> gethosts() {
		return hosts;
	}



}
