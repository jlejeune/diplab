package diplab.adapter.socket.experiment;

import java.io.Serializable;
import java.util.Map;

import diplab.core.IDNode;
import diplab.core.experiment.Experiment;

public class NodesInfoMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private final Map<IDNode,NodeAddressMessage> mapping;
	
	private final long starttime;
	private final Experiment experiment;
	public NodesInfoMessage(Map<IDNode, NodeAddressMessage> mapping, long starttime, Experiment experiment) {
		this.mapping = mapping;
		this.starttime = starttime;
		this.experiment = experiment;
	}
	public Map<IDNode, NodeAddressMessage> getMapping() {
		return mapping;
	}
	public long getStarttime() {
		return starttime;
	}
	public Experiment getExperiment() {
		return experiment;
	}
	
	
	
	
}
