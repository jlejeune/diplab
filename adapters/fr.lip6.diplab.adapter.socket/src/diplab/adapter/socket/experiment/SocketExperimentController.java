package diplab.adapter.socket.experiment;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import diplab.core.IDNode;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.ExperimentController;
import diplab.core.experiment.reporting.Report;

public class SocketExperimentController implements ExperimentController {

	public static final String PAR_NODE_DEPLOYER_CLASS="experiment.socket.nodedeployer";
		
	private Long starttime = null;
	private  Map<IDNode,Report> reports = new HashMap<>(); 
	
	@Override
	public Map<IDNode, Report> execute(Experiment expe) {
		
		try (ServerSocket ss = new ServerSocket(0);){
			int port = ss.getLocalPort();
			String host = InetAddress.getLocalHost().getHostAddress();
			String name_class = (String) expe.getProperties().getOrDefault(PAR_NODE_DEPLOYER_CLASS, ClassicalNodesDeployer.class.getCanonicalName() );
			Class<? extends NodesDeployer> cl_nd = Class.forName(name_class).asSubclass(NodesDeployer.class);
			NodesDeployer deployer = cl_nd.getConstructor(Experiment.class,InetSocketAddress.class)
											.newInstance(expe,InetSocketAddress.createUnresolved(host, port));

			List<Process> processes = deployer.deploy();
			final Map<IDNode, NodeAddressMessage> mapping = new HashMap<IDNode, NodeAddressMessage>();
			
			Map<IDNode,Map<IDNode,Integer>> received = new HashMap<>();
			Map<IDNode,Map<IDNode,Integer>> sent = new HashMap<>();
			Thread network_listener = new Thread(() -> {
				boolean ending=false;
				while(reports.size() < expe.getNbNode()) {
					
					try(Socket sock= ss.accept();){
						try(ObjectInputStream ois = new ObjectInputStream(sock.getInputStream()); 
							ObjectOutputStream oos = new ObjectOutputStream(sock.getOutputStream()) ){
						
							Object objreceived = ois.readObject();
							if(objreceived instanceof NodeAddressMessage) {
								NodeAddressMessage na = (NodeAddressMessage) objreceived;
								System.err.println("Controller : receive NodeAddressMessage from "+na.getHostPort());
								synchronized(mapping) {
									mapping.put(na.getId(), na);
									mapping.notify();
								}
								oos.writeInt(0);
							}else if(objreceived instanceof ReportMessage) {
								ReportMessage reportmess = (ReportMessage)objreceived;
								System.err.println("Controller : receive report from "+reportmess.getSender());
								reports.put(reportmess.getSender(), reportmess.getReport());
								oos.writeInt(0);
							}else if(objreceived instanceof EndAskingMessage) {
								EndAskingMessage easkmess=(EndAskingMessage) objreceived;
								System.err.println("Controller : receive EndAskingMessage from "+easkmess.getSource()+" sent = "+easkmess.getSent()+" received = "+easkmess.getReceived());
								received.put(easkmess.getSource(), easkmess.getReceived());
								sent.put(easkmess.getSource(), easkmess.getSent());
								oos.writeInt(0);
								oos.flush();
								if(checkEnd(expe,sent, received) && !ending) {
									ending=true;
									for(Entry<IDNode,NodeAddressMessage> e : mapping.entrySet()) {
										try(Socket s = new Socket(e.getValue().getHost(), e.getValue().getPort())){
											try(ObjectOutputStream oos2 = new ObjectOutputStream(s.getOutputStream())){
												System.err.println("Controller :sending  AckEndMessage to"+e.getKey());
												oos2.writeObject(new AckEndMessage());
											}
										}catch(SocketException exp) {
											System.err.println("Controller : SocketException when try to connect with "+e.getKey());
										}				
									}
								}
							}else {
								System.err.println("Controller : ERROR RECEIVING UNKOWN MESSAGE ("+objreceived.getClass()+"="+objreceived.toString()+") FROM "+sock.getInetAddress().getHostName());
							}
							
						}					
					}catch(SocketException e) {
						System.err.println("controller : listener interrupted");
					}catch (ClassNotFoundException | IOException e1) {
						throw new IllegalStateException("controller",e1);
					}
					
				}//fin boucle
			}, "Server-thread-controller-listener");
			network_listener.start();
			
			long start_chrono=System.currentTimeMillis();
			synchronized(mapping) {
				while(mapping.size() < processes.size()) {
					System.err.println("Controller : waiting for "+(processes.size()-mapping.size())+" NodeAddress messages");
					mapping.wait();
				}
			}
			long stop_chrono=System.currentTimeMillis();
			System.err.println("Controller : receiving all NodeAddressMessage (took "+(stop_chrono-start_chrono)+" ms)");
			starttime=System.currentTimeMillis();
			for(Entry<IDNode,NodeAddressMessage> e : mapping.entrySet()) {
				try(Socket s = new Socket(e.getValue().getHost(), e.getValue().getPort())){
					try(ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream())){
						oos.writeObject(new NodesInfoMessage(mapping, starttime, expe));
					}
				}				
			}
			
			network_listener.join();
			
			System.err.println("Shutting down server socket on controller");
			network_listener.interrupt();
			ss.close();
			network_listener.join();
			
		}catch (IOException | InterruptedException | ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e1) {
			throw new IllegalStateException("controller",e1);
		}
					
		return reports;
	}
	
	private boolean checkEnd(Experiment expe, Map<IDNode,Map<IDNode,Integer>> sent, Map<IDNode,Map<IDNode,Integer>> received) {
		
		if(sent.size() < expe.getNbNode()-1 ) return false;
		for(IDNode source : sent.keySet()) {
			if(sent.get(source).size()< expe.getNbNode()-1 ) return false;
			for(Map.Entry<IDNode,Integer> e : sent.get(source).entrySet() ) {
				IDNode dest = e.getKey();
				int nbs = e.getValue();
				if(!received.containsKey(dest)) return false;
				if(!received.get(dest).containsKey(source)) return false;
				int nbr = received.get(dest).get(source);
				if(nbs != nbr) return false;
				//if(nbs < nbr) throw new IllegalStateException(dest+" received more message from "+source+" than expected");			
			}
		}
		return true;
	}

}
