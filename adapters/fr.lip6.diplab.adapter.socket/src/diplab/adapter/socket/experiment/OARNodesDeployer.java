package diplab.adapter.socket.experiment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import diplab.core.experiment.Experiment;

public class OARNodesDeployer extends NodesDeployer{

	private final String oarnodefile;
	
	private static Logger logger = Logger.getLogger(OARNodesDeployer.class);
	
	public OARNodesDeployer(Experiment expe, InetSocketAddress controller) {
		super(expe, controller);
		oarnodefile=System.getenv("OAR_NODEFILE");
		if(oarnodefile == null)  throw new IllegalStateException("Env variable OAR_NODEFILE not defined");
		logger.info("OAR_NODEFILE = "+oarnodefile);
	}

	@Override
	public List<String> gethosts() {
		Set<String> res = new HashSet<>();
		try(BufferedReader br = new BufferedReader(new FileReader(oarnodefile))){
			String line;
			while((line = br.readLine()) !=null) {
				res.add(line);
			}
			
		} catch (IOException e1) {
			throw new IllegalStateException(e1);
		} 	
		return new ArrayList<String>(res);
	}

}
