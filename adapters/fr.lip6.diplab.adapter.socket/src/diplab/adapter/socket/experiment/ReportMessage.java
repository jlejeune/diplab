package diplab.adapter.socket.experiment;

import java.io.Serializable;

import diplab.core.IDNode;
import diplab.core.experiment.reporting.Report;

public class ReportMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	private final IDNode sender;
	private final Report report;
	public ReportMessage(IDNode sender, Report report) {
		this.sender = sender;
		this.report = report;
	}
	public IDNode getSender() {
		return sender;
	}
	public Report getReport() {
		return report;
	}
	
	
}
