package diplab.adapter.socket.experiment;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import diplab.adapter.socket.MainSocketNode;
import diplab.core.IDNode;
import diplab.core.experiment.Experiment;

public abstract class NodesDeployer {

	public static final String PAR_SSH_COMMAND="experiment.socket.sshcommand";
	public static final String PAR_NODE_MAPPING="experiment.socket.nodemapping";
	
	private final Experiment expe;
	private final InetSocketAddress controller;
	
			
	//private static Logger logger = Logger.getLogger(NodesDeployer.class);
	
	
	public NodesDeployer(Experiment expe, InetSocketAddress controller) {
		this.expe = expe;
		this.controller = controller;
	}

	private Map<IDNode,String> defaultMapping = null;
	private Map<IDNode,String> getDefaultMapping(){
		if(defaultMapping == null) {
			defaultMapping  = new HashMap<>();
			List<String> hosts = gethosts();
			for(int i =0 ; i < expe.getNbNode();i++) {
				defaultMapping.put(IDNode.get(i), hosts.get(i%hosts.size()));
			}
		}
		return defaultMapping;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Process> deploy() throws IOException {
				
		List<String> hosts = gethosts();
		if(hosts.isEmpty()) throw new IllegalStateException("Host list is empty");
		if(hosts.size() < expe.getNbNode()) {
			System.err.println("Warning : the system size in experiment ("+expe.getNbNode()+") is greater than the number of real hosts ("+hosts.size()+")");
		}
	
		System.err.println("hosts : "+hosts);
		
		List<ProcessBuilder> processbuilders = new ArrayList<>();
		
		String sshcommand = (String)expe.getProperties().getOrDefault(PAR_SSH_COMMAND, "ssh");
		
		Map<IDNode,String> nodemapping = (Map<IDNode,String>) expe.getProperties().getOrDefault(PAR_NODE_MAPPING, getDefaultMapping());
		
		for(int i =0 ; i < expe.getNbNode();i++) {
			String h = nodemapping.get(IDNode.get(i)); //hosts.get(i%hosts.size());
			String[] cmd = new String[] {
				sshcommand,
				h,
				"java -cp \""+System.getProperty("java.class.path")+"\""
				 +" "+MainSocketNode.class.getCanonicalName()
				 +" "+controller.getHostName()
				 +" "+controller.getPort()
				 +" "+i
			};
			processbuilders.add(i, new ProcessBuilder(cmd).inheritIO());
		}
				
		List<Process> res = new ArrayList<Process>();
		
		for(int i =0 ; i < expe.getNbNode();i++) {
			ProcessBuilder pb = processbuilders.get(i);
			try {
				Process p = pb.start();
				res.add(i,p);				
			} catch (Exception e) {
				for(Process p : res) {
					p.destroyForcibly();
				}
				throw e;
			}
		}
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				for(int i=0 ; i < res.size() ;i++) {
					Process p = res.get(i);
					p.destroy();
					String h = hosts.get(i%hosts.size());
					ProcessBuilder pb = new ProcessBuilder(sshcommand,h, "kill -SIGINT $(cat "+System.getProperty("java.io.tmpdir")+"/server"+i+".pid"+") 2> /dev/null").inheritIO();
					try {
						Process ptmp = pb.start();
						ptmp.waitFor();
					} catch (IOException | InterruptedException e) {
						e.printStackTrace();
					}
					
				}
				
				
				
				
			}
		});
		
		
		return res;
			
	}
	
	public abstract List<String> gethosts();
	
}
