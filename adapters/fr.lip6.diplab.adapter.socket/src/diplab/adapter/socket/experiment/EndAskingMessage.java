package diplab.adapter.socket.experiment;

import java.io.Serializable;
import java.util.Map;

import diplab.core.IDNode;

public class EndAskingMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final IDNode source;
	private final Map<IDNode,Integer> sent;
	private final Map<IDNode,Integer> received;
	
	public EndAskingMessage(IDNode source, Map<IDNode,Integer> sent, Map<IDNode,Integer> received) {
		this.source=source;
		this.sent=sent;
		this.received=received;
	}

	public IDNode getSource() {
		return source;
	}

	public Map<IDNode, Integer> getSent() {
		return sent;
	}

	public Map<IDNode, Integer> getReceived() {
		return received;
	}
	
}
