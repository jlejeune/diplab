package diplab.adapter.socket.experiment;

import java.io.Serializable;

import diplab.core.IDNode;

public final class NodeAddressMessage implements Serializable {
	private static final long serialVersionUID = 1L;

	private final String host;
	private final int port;
	private final IDNode id;
	
	
	
	
	public NodeAddressMessage(String host, int port, IDNode id) {
		super();
		this.host = host;
		this.port = port;
		this.id = id;
	}
	
	public String getHostPort() {
		return host+":"+port;
	}
	
	public String getHost() {
		return host;
	}
	public int getPort() {
		return port;
	}
	public IDNode getId() {
		return id;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		result = prime * result + port;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NodeAddressMessage other = (NodeAddressMessage) obj;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (port != other.port)
			return false;
		return true;
	}
	
	
}
