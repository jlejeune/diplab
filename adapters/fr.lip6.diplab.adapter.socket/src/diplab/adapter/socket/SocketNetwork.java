package diplab.adapter.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import diplab.adapter.socket.experiment.AckEndMessage;
import diplab.adapter.socket.experiment.EndAskingMessage;
import diplab.adapter.socket.experiment.NodesInfoMessage;
import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.event.MessageReceptionEvent;
import diplab.core.messaging.Message;
import diplab.system.multithread.MultithreadNodeSystem;
import diplab.system.multithread.Network;

public class SocketNetwork implements Network {

	//private static Logger logger = Logger.getLogger(SocketNetwork.class);

	public static final String PAR_MIN_LATENCY="experiment.socket.minlatency";
	public static final long DEFAULT_MIN_LATENCY=1L;//in Miliseconds
	public static final String PAR_ENDING_ACTIVATED="experiment.socket.endingactivated";

	private NodesInfoMessage info=null;
	private final ServerSocket ss;
	private final Thread network_listener;
	private MultithreadNodeSystem nodesystem;
	private final  AtomicBoolean crashed;
	private final Timer messages_delivering;

	private final int port;
	private String host;

	//IdNode -> thread which sends message to it
	private final Map<IDNode,MessageSender> senders = new ConcurrentHashMap<>();

	//IdNode -> thread which receive messages from it
	private final Map<IDNode,MessageReceiver> receivers = new ConcurrentHashMap<>();


	private final IDNode myid;
	private final String host_controller;
	private final int port_controller;
	private long minlatency=DEFAULT_MIN_LATENCY;
	private boolean endingactivated=false;

	private class MessageSender extends Thread{

		private final IDNode remote;
		private final BlockingQueue<Message<?>> sendingqueue  = new LinkedBlockingQueue<>();
		private final AtomicInteger count=new AtomicInteger(0);
		
		public MessageSender(IDNode remote) {
			this.remote=remote;
			this.setName("sender-"+myid+"-to-"+remote);

		}

		@Override
		public void run() {
			try {
				String host = info.getMapping().get(remote).getHost();
				int port = info.getMapping().get(remote).getPort();
				try(Socket sock = new Socket(host, port);
						ObjectOutputStream oos = new ObjectOutputStream(sock.getOutputStream())){
					oos.writeObject(myid);
					oos.flush();
					while(!Thread.currentThread().isInterrupted()) {
						Message<?> message = sendingqueue.take();
						oos.writeObject(message);
						count.incrementAndGet();
						oos.flush();
						if(message instanceof EndMessage) {
							this.interrupt();
						}
					}
				}
			}catch(InterruptedException  e) {
				System.err.println(myid+" : "+getName()+" interrupted");
			}catch(IOException e) {
				IllegalStateException ise=new IllegalStateException(e);
				//System.err.println("", ise);
				throw ise;
			}

		}

	}

	
	
	
	private class MessageReceiver extends Thread{

		private final Socket comm;
		private final IDNode remote;
		private final ObjectInputStream ois;
		private final AtomicInteger count=new AtomicInteger(0);
			
		public MessageReceiver(Socket comm, ObjectInputStream ois, IDNode remote) {
			this.comm=comm;
			this.remote=remote;
			this.ois=ois;
			setName("receiver-"+myid+"-from-"+remote);
		}

		public void run() {

			try(ois){
				while(!Thread.currentThread().isInterrupted()) {
					try {
						Object objreceived =  ois.readObject();
						count.incrementAndGet();
						//checkend();
						if(! (objreceived instanceof Message)) {
							System.err.println(myid+" (receiver from "+remote+") : ERROR RECEIVING UNKOWN MESSAGE TYPE ("+objreceived.getClass()+"="+objreceived.toString());
							continue;
						}
						Message<?> message = (Message<?>)objreceived;
						if(message instanceof EndMessage) {
							this.interrupt();
						}else {
							if(!crashed.get()) {
								messages_delivering.schedule(new TimerTask() {
									
									@Override
									public void run() {
										try {
											deliverMessage(message);
										}catch (InterruptedException e) {
											System.err.println(myid+" : interrupted while delivering "+message);
										}
										
									}
								}, minlatency);	
							}
							
						}
						
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					}
				}
				comm.close();
			}  catch (IOException e) {
				throw new IllegalStateException(e);
			}
		}

	}

	public void setEndingActivated(boolean value) {
		endingactivated=value;
	}

	public void setMinimalLatency(long minlatency) {
		this.minlatency=minlatency;
	}

	private void deliverMessage(Message<?> message) throws InterruptedException {
		if(crashed.get()) return;
		MultithreadNodeSystem system =  getNodeSystem();
		NodeProto nodeproto = system.getNodeProto(message.getNameProto());
		system.getEventprocessor().putEvent(new MessageReceptionEvent(message, system, nodeproto));
	}


	public SocketNetwork(IDNode myid, String host_controller, int port_controller, Object lockend) throws IOException {
		this.myid=myid;
		this.host_controller=host_controller;
		this.port_controller=port_controller;
		messages_delivering =  new Timer("Timer-message-delivering-"+myid);
		ss = new ServerSocket(0);//port_controller+((int)myid.getVal()+1)
		this.crashed=new AtomicBoolean(false);
		port = ss.getLocalPort();
		host = InetAddress.getLocalHost().getHostAddress();
		network_listener = new Thread(() -> {

			try {
				while(!Thread.currentThread().isInterrupted()) {
					try {
						Socket sock= ss.accept();
						ObjectInputStream ois = new ObjectInputStream(sock.getInputStream());
						Object objreceived = ois.readObject();
						if(objreceived instanceof NodesInfoMessage) {
							setNodesInfoMessage((NodesInfoMessage)objreceived);
							try{
								sock.close();
							}catch(IOException e) {
								System.err.println(myid+" error when closing socket");
							}
							continue;
						}else if(objreceived instanceof IDNode) {
							IDNode remote = (IDNode) objreceived;
							if(receivers.containsKey(remote)) {
								throw new IllegalStateException(myid+" : ERROR RECEIVING TWO CONNECTIONS FROM "+remote);
							}
							MessageReceiver receiver = new MessageReceiver(sock, ois, remote);
							receivers.put(remote, receiver);
							receiver.start();
						}else if(objreceived instanceof AckEndMessage) {
							synchronized (lockend) {
								lockend.notify();
							}							
						}else {
							System.err.println(myid+" : ERROR RECEIVING UNKOWN MESSAGE ("+objreceived.getClass()+"="+objreceived.toString()+") FROM "+sock.getInetAddress().getHostName());
						}
					}catch(SocketException e) {
						System.err.println(myid+" : connexion listener interrupted");
						return;
					} catch (ClassNotFoundException e1) {
						e1.printStackTrace();
					}
				}//fin boucle
			}catch(IOException e) {
				throw new IllegalStateException(e);
			}
		}, "Thread-connexion-listener-"+myid);
		network_listener.start();
	}

	public int getPort() {
		return port;
	}
	public String getHost() {
		return host;
	}

	private synchronized MultithreadNodeSystem getNodeSystem() throws InterruptedException {
		while(nodesystem == null) {
			wait();
		}
		return nodesystem;
	}

	public synchronized void setNodeSystem(MultithreadNodeSystem nodesystem) {
		this.nodesystem=nodesystem;
		notify();		
	}


	@Override
	public void start(boolean waitready) {

		synchronized(senders) {
			for(long i=0;i<nodesystem.systemsize();i++) {
				if(i== myid.getVal()) continue;
				IDNode remote = IDNode.get(i);
				MessageSender ms = new MessageSender(remote);
				senders.put(remote,ms);
				ms.start();
			}
		}

	}

	@Override
	public void sendReal(Message<?> message) {

		try {
			if(message.getDest().equals(myid)) {
				if(crashed.get()) return;
				messages_delivering.schedule(new TimerTask() {
					
					@Override
					public void run() {
						try {
							deliverMessage(message);
						}catch (InterruptedException e) {
							System.err.println(myid+" : interrupted while delivering "+message);
						}
						
					}
				}, 1L);	//delay at least one milisecond to deliver a message locally

			}else {
				IDNode dest = message.getDest();
				synchronized (senders) {
					while(senders.get(dest)==null) {
						senders.wait();
					}
					senders.get(dest).sendingqueue.put(message);
				}				
			}	
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
	}

	@Override
	public void stop(boolean waitend) {
		try {
			messages_delivering.cancel();
			network_listener.interrupt();
			ss.close();
			for(MessageSender s : senders.values()) {
				try {
					s.sendingqueue.put(new EndMessage(myid, s.remote));
					s.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
			for(MessageReceiver r : receivers.values()) {
				try {
					r.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}			

		} catch (IOException e) {
			throw new IllegalStateException(myid+"",e);
		}

	}



	@Override
	public void crash(MultithreadNodeSystem multithreadNodeSystem) {
		this.crashed.set(true);

	}

	@Override
	public void recover(MultithreadNodeSystem multithreadNodeSystem) {
		this.crashed.set(false);
	}



	private synchronized void setNodesInfoMessage(NodesInfoMessage info) {
		this.info=info;
		notify();
	}

	public synchronized NodesInfoMessage getNodesInfoMessage() throws InterruptedException {
		while(info==null) {
			wait();
		}
		return info;
	}

	
	private void checkend() {
		if(!endingactivated) return;
		if(nomoreaction 
				&& 
				nodesystem.getEventprocessor().nbPendingEvents() == 0) {
			try(Socket s = new Socket(host_controller,port_controller)){
				try(ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream())){
					try(ObjectInputStream ois = new ObjectInputStream(s.getInputStream())){
						Map<IDNode,Integer> sent = new HashMap<>();
						for(MessageSender sender: senders.values()) {
							sent.put(sender.remote, sender.count.intValue());
						}
						Map<IDNode,Integer> received = new HashMap<>();
						for(MessageReceiver receiver : receivers.values()) {
							received.put(receiver.remote, receiver.count.intValue());
						}
						System.err.println(myid+" send EndAskingMessage sent = "+sent+" received="+received);
						oos.writeObject(new EndAskingMessage(myid, sent, received));
						ois.readInt();
					}
				}		
				
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	@Override
	public void nomoreaction(MultithreadNodeSystem multithreadNodeSystem) {
		System.err.println(multithreadNodeSystem.idnode()+" : no more action");
		nomoreaction=true;
		checkend();
	}

	
	private boolean nomoreaction=false;
	@Override
	public void nomoreEvents(MultithreadNodeSystem multithreadNodeSystem) {
		checkend();
	}

}
