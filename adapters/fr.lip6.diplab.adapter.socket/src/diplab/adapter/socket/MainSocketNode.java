package diplab.adapter.socket;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import diplab.adapter.socket.experiment.NodeAddressMessage;
import diplab.adapter.socket.experiment.NodesInfoMessage;
import diplab.adapter.socket.experiment.ReportMessage;
import diplab.core.IDNode;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.reporting.Report;
import diplab.system.multithread.MultithreadNodeSystem;

public class MainSocketNode {
	private static String host_controller;
	private static int port_controller;
	private static IDNode myid;

	//private static Logger logger = Logger.getLogger(MainSocketNode.class);
	
	public static void main(String[] args) throws FileNotFoundException, UnknownHostException {
		String usage="<host controller> <port controller> <idnode>";
		if(args.length < 3) {
			System.err.println(usage);
			System.exit(1);
		}
		host_controller=args[0];
		port_controller=Integer.parseInt(args[1]);
		myid=IDNode.get(Long.parseLong(args[2]));
		final Object lock = new Object();
		System.err.println("Starting node "+myid+" on "+InetAddress.getLocalHost().getHostName());
		File f = new File(System.getProperty("java.io.tmpdir")+"/server"+myid.getVal()+".pid");
		if(f.exists()) f.delete();
		try(PrintStream ps = new PrintStream(f)){
			ps.print(ProcessHandle.current().pid()+"");
		}
		
		try {
			System.err.println("Launching "+myid+" controller = "+host_controller+":"+port_controller);
			SocketNetwork network = new SocketNetwork(myid, host_controller,port_controller,lock);
			try(Socket s = new Socket(host_controller, port_controller)){
				try(ObjectOutputStream oos = new ObjectOutputStream(s.getOutputStream())){
					System.err.println(myid+" : sending NodeAddresMessage to "+s.getInetAddress().getHostName()+" "+s.getPort());
					long start_chrono=System.currentTimeMillis();
					oos.writeObject(new NodeAddressMessage(network.getHost(), network.getPort(), myid));
					oos.flush();
					try(ObjectInputStream ois = new ObjectInputStream(s.getInputStream())){
						System.err.println(myid+" : waiting ack NodeAddresMessage from "+s.getInetAddress().getHostName()+" "+s.getPort());
						ois.readInt();
					}
					long stop_chrono=System.currentTimeMillis();
					System.err.println(myid+" : reiceive ack from "+s.getInetAddress().getHostName()+" "+s.getPort()+" (took" +(stop_chrono-start_chrono)+" ms)");
				}
			} 
			NodesInfoMessage info = network.getNodesInfoMessage();
			System.err.println(myid+"  NodesInfoMessage OK");
			int size=info.getMapping().size();
			List<IDNode> allnodes=new ArrayList<>();
			for(int i=0; i<size;i++) {
				allnodes.add(IDNode.get(i));
			}
			Experiment e =info.getExperiment(); 
			long starttime =info.getStarttime(); 

			Long min_latency = (Long) e.getProperties().getOrDefault(SocketNetwork.PAR_MIN_LATENCY, SocketNetwork.DEFAULT_MIN_LATENCY);
			network.setMinimalLatency(min_latency);
			boolean endingactivated = (Boolean) e.getProperties().getOrDefault(SocketNetwork.PAR_ENDING_ACTIVATED, false);
			network.setEndingActivated(endingactivated);
			MultithreadNodeSystem nodesystem = new MultithreadNodeSystem(e, starttime, myid, allnodes, network);
			network.setNodeSystem(nodesystem);
			nodesystem.start();
			System.err.println(""+myid+" started on "+network.getHost());

			long begin= System.currentTimeMillis();
			synchronized(lock) {
				lock.wait(e.getExperimentDuration());
				long end = System.currentTimeMillis();
				if(end-begin > e.getExperimentDuration() ) {
					System.err.println(""+myid+" TIMEOUT !!!!! ");
				}
			}
			System.err.println(""+myid+" stopping (MainSocketNode.java)");
			nodesystem.stop();


			Report local = nodesystem.getLocalReport();
			Report health = nodesystem.getHealthReport();
			if(health.size()> 0) {
				local=local.merge(health, local.getName());
			}		


			//envoyer le report au master
			System.err.println(""+myid+" sending report to controller");
			setReport(nodesystem.idnode(),local);
		}catch(InterruptedException | IOException  e) {
			throw new IllegalStateException(myid+"",e);
		}
		System.exit(0);

	}


	public static void setReport(IDNode id,Report report) {
		try(Socket sock = new Socket(host_controller, port_controller);
				ObjectOutputStream outstream = new ObjectOutputStream(sock.getOutputStream());
				ObjectInputStream instream = new ObjectInputStream(sock.getInputStream())	){
			
			
			outstream.writeObject(new ReportMessage(myid, report));
			instream.readInt();
		} catch (IOException e) {
			throw new IllegalStateException(myid+"",e);
		}		
	}

}
