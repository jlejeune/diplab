package diplab.adaptervalidator.multialgo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import diplab.adaptervalidator.TestExec;
import diplab.adaptervalidator.hello.TestHello;
import diplab.adaptervalidator.helloring.TestHelloRing;
import diplab.adaptervalidator.hellowait.TestHelloWait;
import diplab.core.NodeProto;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.scenario.Scenario;
import diplab.core.experiment.scenario.action.Action;

public class TestMultiAlgo extends TestExec {

	private final List<TestExec> testexecs = new ArrayList<>();

	public TestMultiAlgo(int sizeSystem) {
		super(sizeSystem);
		testexecs.add(new TestHello(sizeSystem));
		testexecs.add(new TestHelloRing(sizeSystem));
		testexecs.add(new TestHelloWait(sizeSystem));
	}

	@Override
	public Map<String, Class<? extends NodeProto>> getAlgonodeclasses() {
		
		Map<String,Class<? extends NodeProto>> res = new HashMap<>();
		testexecs.forEach(t -> res.putAll(t.getAlgonodeclasses()));
		return res;
	}

	@Override
	public Scenario getScenario() {
		Scenario res = new Scenario();
		for(TestExec t : testexecs) {
			for(Action a : t.getScenario()) {
				res.addAction(a);
			}
		}
		
		return res;
	}

	@Override
	public void check(Report r) {
		
		testexecs.stream().forEach(t->t.check(r));
		
	}

}
