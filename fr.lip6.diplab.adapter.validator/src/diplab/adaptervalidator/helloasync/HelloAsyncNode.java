package diplab.adaptervalidator.helloasync;


import diplab.core.Loggable;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.Primitive;

public  class HelloAsyncNode extends NodeProto {

	@Loggable
	int nbcallhello=0;
	
	@Loggable
	int nbcallhelloasync=0;
	
	public HelloAsyncNode(NodeSystem system) {
		super(system);
	}
		
	@Primitive
	public void hello() {
		nbcallhello++;
		callAsync(getNameProto(), "helloasync");
		wait(()-> nbcallhello == nbcallhelloasync);
	}
	
	@Primitive
	public void helloasync() {
		nbcallhelloasync++;
	}
}
