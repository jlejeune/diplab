package diplab.adaptervalidator.broadcast;

import java.io.Serializable;

import diplab.core.IDNode;
import diplab.core.messaging.Message;

public class ReliableBroadcastMessageContent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final IDNode sender;
	private final int numSeq;
	private final Message<?> message;
	public ReliableBroadcastMessageContent(IDNode sender, int numSeq, Message<?> message) {
		super();
		this.sender=sender;
		this.numSeq = numSeq;
		this.message = message;
	}
	public IDNode getSender() {
		return sender;
	}
	public int getNumSeq() {
		return numSeq;
	}
	public Message<?> getMessage() {
		return message;
	}
	
	
	
}
