package diplab.adaptervalidator.broadcast;

import diplab.core.IDNode;
import diplab.core.Loggable;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.Primitive;
import diplab.core.messaging.Message;
import diplab.core.messaging.MessageHandler;
import diplab.core.messaging.StringMessage;

public class AppliBroadcast extends NodeProto {

		
	public AppliBroadcast(NodeSystem system) {
		super(system);
	}
	
	@Loggable
	private int nb_broadcast;
	
	private int cpt=0;
	
	@Primitive
	public Message<String> hellobroadcast() {
		System.out.println(getCurrentTime()+" : "+idnode()+": broadcast message");
		Message<String> message = buildMessage(StringMessage.class, IDNode.ANY, "hello"+(cpt++));
		nb_broadcast = (int) call("broadcast","broadcast", message);
		return message;
	}
	
	@MessageHandler(StringMessage.class)
	public void receiveMessage(StringMessage m) {
		System.out.println(getCurrentTime()+" : "+idnode()+": Receive "+m.getContent()+" from "+m.getSrc());
	}
	

}
