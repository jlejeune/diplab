package diplab.adaptervalidator.broadcast;

import diplab.core.IDNode;
import diplab.core.messaging.Message;

public class ReliableBroadcastMessage extends Message<ReliableBroadcastMessageContent> {

	
	
	

	public ReliableBroadcastMessage(IDNode src, IDNode dest, String nameproto, long id,
			ReliableBroadcastMessageContent content) {
		super(src, dest, nameproto, id, content);
	}

	private static final long serialVersionUID = 7908833166837741030L;

	
	
	
	@Override
	public int hashCode() {
		return getContent().getSender().hashCode()+getContent().getNumSeq();
	}

	public boolean isEquivalent(ReliableBroadcastMessage other) {
		return getContent().getSender().equals(other.getContent().getSender()) && getContent().getNumSeq() == other.getContent().getNumSeq();
	}

	

}
