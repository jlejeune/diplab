package diplab.adaptervalidator.broadcast;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import diplab.adaptervalidator.TestExec;
import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.logevent.LogEndCallEvent;
import diplab.core.experiment.reporting.logevent.LogStartCallEvent;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;
import diplab.core.experiment.reporting.visitor.selector.LogNodeProtoEventSelector;
import diplab.core.experiment.scenario.Scenario;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.CrashAction;
import diplab.core.experiment.scenario.action.callscheme.Call;
import diplab.core.experiment.scenario.action.callscheme.RepeatedCallSequence;
import diplab.core.messaging.Message;

public class TestReliableBroadcast extends TestExec {

	private static final String id_app="app";
	private static final String id_broadcast="broadcast";


	public TestReliableBroadcast(int sizeSystem) {
		super(sizeSystem);

	}

	@Override
	public Map<String, Class<? extends NodeProto>> getAlgonodeclasses() {
		Map<String, Class<? extends NodeProto>> res = new HashMap<>();
		res.put(id_broadcast, ReliableBroadcast.class);
		res.put(id_app,AppliBroadcast.class);
		return res;
	}

	private final List<IDNode> failling = Arrays.asList(IDNode.get(1), IDNode.get(4));

	@Override
	public Scenario getScenario() {
		Scenario res = new Scenario();

		res.addAction(new CallAction( 
				(r)-> 30L + r.nextInt(100), 
				new RepeatedCallSequence(10)
				.addCallScheme(new Call(id_app, "hellobroadcast", (r)->r.nextInt(30)+1L))
				)
				);

		res.addAction(new CrashAction((r)->50L, (r)-> 50L, IDNode.get(1)));
		res.addAction(new CrashAction((r)->80L, (r)-> 100L , IDNode.get(4)));
		return res;
	}

	@Override
	public void check(Report r) {
		final Map<Message<String>, List<IDNode>> map =new HashMap<>();
		Report subreport = r.subreport(new LogNodeProtoEventSelector(id_app, LogEndCallEvent.class, LogStartCallEvent.class));

		LogEventVisitor visitor = new LogEventVisitor() {
			@SuppressWarnings("unchecked")
			@Override
			public void visit(LogEndCallEvent e) {
				if(! e.getNameprimitive().equals("hellobroadcast")) return;
				map.put((Message<String>)e.getResult(), new ArrayList<>());				
			}

			@Override
			public void visit(LogStartCallEvent e) {
				if(!e.getNameprimitive().equals("receiveMessage")) return;
				map.get(e.getArgs()[0]).add(e.getID().getNode());
			}


		};

		subreport .analyse(visitor);


		for(Entry<Message<String>, List<IDNode>> e : map.entrySet()) {
			//				Validity : if a correct process broadcasts a message m,
			//				then all correct processes deliver m
			Message<String> message= e.getKey();
			List<IDNode> receivers = e.getValue();
			if(! failling.contains(message.getSrc())) {
				for(int i=0;i< getSizeSystem(); i++) {
					IDNode cur = IDNode.get(i);
					if(! failling.contains(cur)) {
						assertTrue(receivers.contains(cur));
					}
				}
			}
			//				Agreement : if a correct process delivers a message m, then 
			//				all correct processes deliver m
			if(receivers.contains(IDNode.get(0))) {
				for(int i=0;i< getSizeSystem(); i++) {
					IDNode cur = IDNode.get(i);
					if(! failling.contains(cur)) {
						assertTrue(receivers.contains(cur));
					}
				}
			}

			int nb_correct = getSizeSystem()-failling.size();
			//				Integrity: A message m is delivered at most once in any correct process 
			//				and only if it has been broadcasted by a process
			List<IDNode> tmp = new ArrayList<>();
			for(IDNode id : receivers) {
				if(! failling.contains(id)) {
					assertFalse(tmp.contains(id));
					tmp.add(id);
				}
			}
			assertEquals(nb_correct,tmp.size());

		}

	}

}
