package diplab.adaptervalidator.broadcast;

import java.util.ArrayList;
import java.util.List;

import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.Primitive;
import diplab.core.messaging.Message;
import diplab.core.messaging.MessageHandler;

public class BestEffortBroadcast extends NodeProto {

	private final List<IDNode> allnodes =new ArrayList<>();
	
	
	public BestEffortBroadcast(NodeSystem system) {
		super(system);
		for (int i = 0; i < this.systemsize(); i++) {
			allnodes.add(IDNode.get(i));
		}
	}
	
	public static class BestEffortBroadcastMessage extends Message<Message<?>>{

		public BestEffortBroadcastMessage(IDNode src, IDNode dest, String nameproto, long id, Message<?> content) {
			super(src, dest, nameproto, id, content);
		}
		/**
		 * 
		 */
		private static final long serialVersionUID = -4049626467604898723L;
		
	}
	
	
	@Primitive
	public int broadcast(Message<?> m) {
		
		sendBroadcast(BestEffortBroadcastMessage.class, allnodes, m);
		return allnodes.size();
		
	}
	
	@MessageHandler(BestEffortBroadcastMessage.class)
	public void receiveMessage(BestEffortBroadcastMessage m) {
		deliverMessage(m.getContent().getNameProto(), m.getContent());
	}

}
