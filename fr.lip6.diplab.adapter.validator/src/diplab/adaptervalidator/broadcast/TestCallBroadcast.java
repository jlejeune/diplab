package diplab.adaptervalidator.broadcast;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import diplab.adaptervalidator.TestExec;
import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.visitor.selector.LogMessageEventSelector;
import diplab.core.experiment.scenario.Scenario;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.callscheme.Call;
import diplab.core.messaging.StringMessage;

public class TestCallBroadcast extends TestExec {

	private static final String id_app="app";
	private static final String id_broadcast="broadcast";


	public TestCallBroadcast(int sizeSystem) {
		super(sizeSystem);
	}

	@Override
	public Map<String, Class<? extends NodeProto>> getAlgonodeclasses() {
		Map<String, Class<? extends NodeProto>> res = new HashMap<>();
		res.put(id_broadcast, BestEffortBroadcast.class);
		res.put(id_app,AppliBroadcast.class);
		return res;
	}

	@Override
	public Scenario getScenario() {
		Scenario res = new Scenario();
		res.addAction(
				new CallAction((r)-> 60L, 
						new Call(id_app, "hellobroadcast", (r)->1L),
						IDNode.get(0)
						)	
				);
		return res;
	}

	@Override
	public void check(Report r) {

		LogMessageEventSelector e = new LogMessageEventSelector(id_app, StringMessage.class);

		r.analyse(e);

		assertEquals(getSizeSystem(), e.get().size());	

	}

}
