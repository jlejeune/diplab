package diplab.adaptervalidator.broadcast;

import java.util.HashSet;
import java.util.Set;

import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.Primitive;
import diplab.core.messaging.Message;
import diplab.core.messaging.MessageHandler;

public class ReliableBroadcast extends NodeProto {

	public ReliableBroadcast(NodeSystem system) {
		super(system);
	}

	private int cur_num_seq = 0;
	
	private Set<ReliableBroadcastMessage> received;

	@Override
	public void init() {
		received=new HashSet<>();
	}

	
	@Primitive
	public int broadcast(Message<?> m) {
		for (int i = 0; i < this.systemsize(); i++) {
			
			send(ReliableBroadcastMessage.class, IDNode.get(i), 
					new ReliableBroadcastMessageContent(idnode(),cur_num_seq, m) );
		}
		cur_num_seq++;
		return cur_num_seq;
	}

	@MessageHandler(ReliableBroadcastMessage.class)
	public void receiveReliableMessage(ReliableBroadcastMessage message) {
		if(! containsEquvalent(message)) {
			received.add(message);
			if(!message.getSrc().equals(this.idnode())) {
				for (int i = 0; i < this.systemsize(); i++) {
					send(ReliableBroadcastMessage.class, IDNode.get(i),
							message.getContent());
				}
			}
			deliverMessage(message.getContent().getMessage().getNameProto(), message.getContent().getMessage());
		}
	}

	private boolean containsEquvalent(ReliableBroadcastMessage message) {
		for(ReliableBroadcastMessage m : received) {
			if(m.isEquivalent(message)) return true;
		}
		return false;
	}
	
}
