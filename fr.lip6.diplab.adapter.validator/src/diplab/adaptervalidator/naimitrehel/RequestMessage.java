package diplab.adaptervalidator.naimitrehel;

import diplab.core.IDNode;
import diplab.core.messaging.Message;

public class RequestMessage extends Message<IDNode> {

	public RequestMessage(IDNode src, IDNode dest, String nameproto, long id, IDNode content) {
		super(src, dest, nameproto, id, content);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3214756719397073063L;


}
