package diplab.adaptervalidator.naimitrehel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import diplab.adaptervalidator.TestExec;
import diplab.adaptervalidator.naimitrehel.NTNode.State;
import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.logevent.LogCallEvent;
import diplab.core.experiment.reporting.logevent.LogEndCallEvent;
import diplab.core.experiment.reporting.logevent.LogStartCallEvent;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;
import diplab.core.experiment.reporting.visitor.selector.LogCallEventSelector;
import diplab.core.experiment.reporting.visitor.selector.LogEventSelector;
import diplab.core.experiment.scenario.ConstanteValue;
import diplab.core.experiment.scenario.Scenario;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.callscheme.Call;
import diplab.core.experiment.scenario.action.callscheme.RepeatedCallSequence;

public class TestNT extends TestExec {

	public TestNT(int sizeSystem) {
		super(sizeSystem);
	}

	private final String nameproto ="NT";

	private final long begin=200;

	private final long time_in_cs=220;

	private final long time_tranquil=200;

	private final int nb_cs=10;

	@Override
	public Map<String, Class<? extends NodeProto>> getAlgonodeclasses() {
		Map<String,Class<? extends NodeProto>> res = new HashMap<>();
		res.put(nameproto, NTNode.class);
		return res;
	}

	@Override
	public Scenario getScenario() {
		Scenario res = new Scenario();
		res.addAction(
				new CallAction(new ConstanteValue<Long>(begin), 
						new RepeatedCallSequence(nb_cs)
						.addCallScheme(new Call(nameproto, "requestCS", new ConstanteValue<>(time_in_cs)))
						.addCallScheme(new Call(nameproto, "releaseCS", new ConstanteValue<>(time_tranquil)))
						)
				);


		return res;
	}

	@Override
	public void check(Report r) {

		//there is at most one node in inCS state all the time
		LogEventVisitor safety = new LogEventVisitor() {

			Map<IDNode,NTNode.State> states=new HashMap<>();

			@Override
			public void visit(LogCallEvent e) {
				IDNode id = e.getID().getNode();
				NTNode.State state = (State) e.getStateOf("state");
				states.put(id, state);

				long cpt = states.values().stream().filter(s-> s == State.INCS).count();
				assertTrue(cpt <= 1);
			}

		};

		//there are nbcs release and nbcs request per node
		LogEventSelector startcallrequestevents = new LogCallEventSelector(nameproto, "requestCS", LogStartCallEvent.class);
		LogEventSelector endcallrequestevents = new LogCallEventSelector(nameproto, "requestCS", LogEndCallEvent.class);
		LogEventSelector startcallreleaseevents = new LogCallEventSelector(nameproto, "releaseCS", LogStartCallEvent.class);
		LogEventSelector endcallreleaseevents = new LogCallEventSelector(nameproto, "releaseCS", LogEndCallEvent.class);


		r.analyse(safety,startcallrequestevents, endcallrequestevents, startcallreleaseevents, endcallreleaseevents);
		assertEquals(nb_cs*getSizeSystem(), startcallrequestevents.size());
		assertEquals(nb_cs*getSizeSystem(), endcallrequestevents.size());
		assertEquals(nb_cs*getSizeSystem(), startcallreleaseevents.size());
		assertEquals(nb_cs*getSizeSystem(), endcallreleaseevents.size());



		LogEventSelector request = new LogCallEventSelector(nameproto, "requestCS");
		LogEventSelector release = new LogCallEventSelector(nameproto, "releaseCS");

		Report r2= r.subreport(request,release);

		//for a given node a time between EndrequestCS and beginreleaseCS >= time_in_CS
		//for a given node a time between EndreleaseCS and beginrequestCS >= time_tranquil
		LogEventVisitor time_control = new LogEventVisitor() {

			Map<IDNode, LogCallEvent> last = new HashMap<>();

			@Override
			public void visit(LogStartCallEvent e) {
				IDNode id = e.getID().getNode();
				LogCallEvent last_call_event = last.get(id);
				if(e.getNameprimitive().equals("requestCS")) {
					if(last.get(id) == null) {
						last.put(id, e);
						return;
					}
					assertNotNull(last_call_event);
					assertTrue(last_call_event instanceof LogEndCallEvent);
					assertEquals("releaseCS",last_call_event.getNameprimitive());
					assertTrue(last_call_event.getID().getDate() + time_tranquil <= e.getID().getDate() );
				}else if(e.getNameprimitive().equals("releaseCS")) {
					assertNotNull(last.get(id));
					assertTrue(last_call_event instanceof LogEndCallEvent);
					assertEquals("requestCS",last_call_event.getNameprimitive());
					assertTrue(last_call_event.getID().getDate() + time_in_cs <= e.getID().getDate() );					
				}else {
					assertTrue(false);
				}
				last.put(id, e);
			}

			@Override
			public void visit(LogEndCallEvent e) {
				IDNode id = e.getID().getNode();
				LogCallEvent last_call_event = last.get(id);
				assertNotNull(last.get(id));
				assertTrue(last_call_event instanceof LogStartCallEvent);
				
				if(e.getNameprimitive().equals("requestCS")) {
					assertEquals("requestCS",last_call_event.getNameprimitive());
					
				}else if(e.getNameprimitive().equals("releaseCS")) {
					assertEquals("releaseCS",last_call_event.getNameprimitive());
					
				}else {
					assertTrue(false);
				}
				last.put(id, e);
			}
		};

		r2.analyse(time_control);


	}

}
