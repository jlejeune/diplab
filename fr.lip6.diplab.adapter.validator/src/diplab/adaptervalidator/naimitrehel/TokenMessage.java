package diplab.adaptervalidator.naimitrehel;

import diplab.core.IDNode;
import diplab.core.messaging.StringMessage;

public class TokenMessage extends StringMessage {

	public TokenMessage(IDNode src, IDNode dest, String nameproto, long id, String content) {
		super(src, dest, nameproto, id, content);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
