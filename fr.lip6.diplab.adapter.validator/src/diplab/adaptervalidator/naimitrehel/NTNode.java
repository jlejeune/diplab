package diplab.adaptervalidator.naimitrehel;

import diplab.core.IDNode;
import diplab.core.Loggable;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.Primitive;
import diplab.core.messaging.MessageHandler;

public class NTNode extends NodeProto{

	public NTNode(NodeSystem system) {
		super(system);
	}
	
	public static enum State{TRANQUIL,REQUESTING,INCS}
	private static final IDNode elected_node=IDNode.get(0);
	
	@Loggable
	private State state;
	@Loggable
	private IDNode next;
	@Loggable
	private IDNode last;
	
	@Override
	public void init() {
		state=State.TRANQUIL;
		next=null;
		if(idnode().equals(elected_node)) {
			last=null;
		}else {
			last=elected_node;
		}
		
	}
	
	
	@Primitive
	public void requestCS() {
		state=State.REQUESTING;
		if(last != null) {
			send(RequestMessage.class,last, idnode());
			last=null;
			wait(()->state == State.INCS);
		}
	}

	@Primitive
	public void releaseCS() {
		state=State.TRANQUIL;
		if(next != null) {
			send(TokenMessage.class, next, "");
			next=null;
		}
	}
	
	@MessageHandler(RequestMessage.class)
	public void receiveRequest(RequestMessage m) {
		IDNode requester = m.getContent();
		if(last==null) {
			if(state != State.TRANQUIL) {
				next= requester;
			}else {
				send(TokenMessage.class, requester, "");
			}
		}else {
			send(RequestMessage.class, last, requester);
		}
		last=requester;
	}
	
	@MessageHandler(TokenMessage.class)
	public void receiveToken(TokenMessage m) {
		state=State.INCS;
		System.err.println(idnode()+" entering in CS");
	}
}
