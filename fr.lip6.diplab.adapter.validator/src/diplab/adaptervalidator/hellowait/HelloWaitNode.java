package diplab.adaptervalidator.hellowait;

import diplab.core.IDNode;
import diplab.core.Loggable;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.Primitive;
import diplab.core.messaging.IntMessage;
import diplab.core.messaging.MessageHandler;
import diplab.core.messaging.StringMessage;

public  class HelloWaitNode extends NodeProto {

	@Loggable
	private int value;
	
	private IntMessage received=null;
	
	public HelloWaitNode(NodeSystem system) {
		super(system);
	}

	@Override
	public void init() {
		value=random().nextInt(50);	
	}
	
	@Primitive
	public int getVal(IDNode id) {
		received=null;
		send(StringMessage.class, id, "");
		wait(()->received!=null);
		return received.getContent();		
	}
	
	@MessageHandler(IntMessage.class)
	public void receiveResVal(IntMessage m) {
		received=m;
	}
	
	@MessageHandler(StringMessage.class)
	public void receiveGetVal(StringMessage m) {	
		send(IntMessage.class, m.getSrc(), value);
	}
	
}