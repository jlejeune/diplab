package diplab.adaptervalidator.hellowait;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import diplab.adaptervalidator.TestExec;
import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.logevent.LogEndInterruptionCallEvent;
import diplab.core.experiment.reporting.logevent.LogStartInterruptionCallEvent;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;
import diplab.core.experiment.scenario.Scenario;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.callscheme.Call;

public class TestHelloWait extends TestExec {


	private final String nameproto ="HelloWait";
	public TestHelloWait(int sizeSystem) {
		super(sizeSystem);
	}

	@Override
	public Map<String,Class<? extends NodeProto>> getAlgonodeclasses() {
		Map<String,Class<? extends NodeProto>> res = new HashMap<>();
		res.put(nameproto, HelloWaitNode.class);

		return res;
	}


	@Override
	public Scenario getScenario() {
		Scenario res = new Scenario();
		for(int i = 0; i < getSizeSystem() ; i++) {
			int x=i;
			res.addAction(
					new CallAction(
							(r)-> 500L * (x+1),
							new Call(nameproto,"getVal", (r)->1L, (r)-> IDNode.get(x)),
							IDNode.get(0)
							)
					);
		}		
		return res;
	}



	@Override
	public void check(Report r) {

		LogEventVisitor v1 = new LogEventVisitor() {

			//IDnode-> numcall->numinter->LogStartInterruption
			Map<IDNode,Map<Long, Map<Integer,LogStartInterruptionCallEvent>>> map = new HashMap<>();

			@Override
			public void visit(LogStartInterruptionCallEvent e) {

				if(!map.containsKey(e.getID().getNode())) {
					map.put(e.getID().getNode(), new HashMap<>());
				}
				Map<Long, Map<Integer,LogStartInterruptionCallEvent>> map_node =  map.get(e.getID().getNode());

				if(!map_node.containsKey(e.getNumcall())) {
					map_node.put(e.getNumcall(), new HashMap<>());
				}
				Map<Integer,LogStartInterruptionCallEvent> map_node_numcall = map_node.get(e.getNumcall());
				map_node_numcall.put(e.getNuminter(), e);								
			}

			@Override
			public void visit(LogEndInterruptionCallEvent e_end) {	

				assertTrue(map.containsKey(e_end.getID().getNode()));
				assertTrue(map.get(e_end.getID().getNode()).containsKey(e_end.getNumcall()));
				assertTrue(map.get(e_end.getID().getNode()).get(e_end.getNumcall()).containsKey(e_end.getNuminter()));

				LogStartInterruptionCallEvent e_start = map.get(e_end.getID().getNode()).get(e_end.getNumcall()).get(e_end.getNuminter());

				assertNotNull(e_start);
				assertEquals(e_end.getID().getNode(), e_start.getID().getNode());
				assertTrue(e_end.getID().getDate() > e_start.getID().getDate());
				assertEquals(e_end.getNameprimitive(), e_start.getNameprimitive());
				assertEquals(e_end.getNumcall(), e_start.getNumcall());
				assertEquals(e_end.getNuminter(), e_start.getNuminter());
			}			
		};

		r.analyse(v1);

		//			assertEquals(0, v2.getNbLogCrashEvent());
		//			assertEquals(0, v2.getNbLogRecoverEvent());
		//			
		//			assertEquals(getSizeSystem()*2*2, v2.getNbLogMessageEvent());
		//			assertEquals(getSizeSystem()*2, v2.getNbLogMessageSentEvent());
		//			assertEquals(getSizeSystem()*2, v2.getNbLogMessageReceiveEvent());
		//			
		//			assertEquals(getSizeSystem()*2, v2.getNbLogInterruptionCallEvent());
		//			assertEquals(getSizeSystem(), v2.getNbLogStartInterruptionCallEvent());
		//			assertEquals(getSizeSystem(), v2.getNbLogEndInterruptionCallEvent());




	}
}
