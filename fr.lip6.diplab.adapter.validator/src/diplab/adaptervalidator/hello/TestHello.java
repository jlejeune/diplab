package diplab.adaptervalidator.hello;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import diplab.adaptervalidator.TestExec;
import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.logevent.LogCallEvent;
import diplab.core.experiment.reporting.logevent.LogCrashEvent;
import diplab.core.experiment.reporting.logevent.LogEndCallEvent;
import diplab.core.experiment.reporting.logevent.LogEvent;
import diplab.core.experiment.reporting.logevent.LogInterruptionCallEvent;
import diplab.core.experiment.reporting.logevent.LogMessageEvent;
import diplab.core.experiment.reporting.logevent.LogRecoverEvent;
import diplab.core.experiment.reporting.logevent.LogStartCallEvent;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;
import diplab.core.experiment.reporting.visitor.selector.LogCallEventSelector;
import diplab.core.experiment.reporting.visitor.selector.LogEventSelector;
import diplab.core.experiment.reporting.visitor.selector.LogNodeProtoEventSelector;
import diplab.core.experiment.scenario.Scenario;
import diplab.core.experiment.scenario.action.Action;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.callscheme.Call;
import diplab.core.experiment.scenario.action.callscheme.RepeatedCallSequence;


/**
 * 
 * A protocol test class where each node calls {@link TestHello#nbcall} times the following sequence :
 * <ul>
 * 	<li> call the hello primitive of {@link HelloNode} protocol  </li>
 *  <li> wait a given time {@link TestHello#time_after_hello}</li>
 *  <li> call the hello2 primitive of {@link HelloNode} protocol  </li>
 *  <li> wait a given time {@link TestHello#time_after_hello2}  </li>
 * </ul>
 * This sequence is executed at {@link TestHello#date_begin_sequence}}
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public class TestHello extends TestExec {


	public TestHello(int sizeSystem) {
		super(sizeSystem);
	}

	int nbcall=10;
	private String nameproto = "Hello";

	@Override
	public Map<String,Class<? extends NodeProto>> getAlgonodeclasses() {
		Map<String,Class<? extends NodeProto>> res = new HashMap<>();
		res.put(nameproto, HelloNode.class);

		return res;
	}

	static long time_after_hello=800L;
	static long time_after_hello2=300L; 
	static long date_begin_sequence=50L;

	@Override
	public Scenario getScenario() {
		Scenario res = new Scenario();
		RepeatedCallSequence rcs = new RepeatedCallSequence(nbcall);
		rcs.addCallScheme(new Call(nameproto,"hello", (r)-> time_after_hello),
				new Call(nameproto,"hello2", (r)-> time_after_hello2));		
		Action a = new CallAction((r)-> date_begin_sequence, rcs);
		res.addAction(a);

		return res;
	}



	@Override
	public void check(Report r) {

		// a visitor ensuring that all Log event are sorted chronologically
		LogEventVisitor v1 = new LogEventVisitor() {
			long lastdate=0;
			@Override
			public void visit(LogEvent e) {
				assertTrue(e.getID().getDate()>=lastdate);
				lastdate=e.getID().getDate();
			}

		};


		//a visitor ensuring that the state nbcallhello is consistent with the expected 
		//number of time where the hello primitive is called. 
		LogEventVisitor v3 = new LogEventVisitor() {

			Map<IDNode,Integer> cpt_expected = new HashMap<>();
			@Override
			public void visit(LogStartCallEvent e) {
				if(! e.getNodeProtoName().equals(nameproto)) return;
				if(!e.getNameprimitive().equals("hello")) return;
				int value_expected = cpt_expected.getOrDefault(e.getID().getNode(), 0);

				int value = (int) e.getStateOf("nbcallhello");
				assertEquals(value_expected, value);

				cpt_expected.put(e.getID().getNode(), value_expected+1);

			}
			@Override
			public void visit(LogEndCallEvent e) {
				if(! e.getNodeProtoName().equals(nameproto)) return;
				if(!e.getNameprimitive().equals("hello")) return;
				int value_expected = cpt_expected.get(e.getID().getNode());
				int value = (int) e.getStateOf("nbcallhello");
				assertEquals(value_expected, value);
			}

		};


		//test series ensuring the expected number of each type of event
		LogEventSelector crashevents = new LogEventSelector(LogCrashEvent.class);
		LogEventSelector recoverevents = new LogEventSelector(LogRecoverEvent.class);
		LogEventSelector messageevents = new LogNodeProtoEventSelector(nameproto,LogMessageEvent.class);
		LogEventSelector interruptionevents = new LogNodeProtoEventSelector(nameproto,LogInterruptionCallEvent.class);
		LogEventSelector startcallhelloevents = new LogCallEventSelector(nameproto, "hello", LogStartCallEvent.class);
		LogEventSelector endcallhelloevents = new LogCallEventSelector(nameproto, "hello", LogEndCallEvent.class);
		LogEventSelector startcallhello2events = new LogCallEventSelector(nameproto, "hello2", LogStartCallEvent.class);
		LogEventSelector endcallhello2events = new LogCallEventSelector(nameproto, "hello2", LogEndCallEvent.class);

		r.analyse(v1,v3,crashevents,recoverevents, messageevents,interruptionevents,startcallhelloevents, endcallhelloevents,
				startcallhello2events, endcallhello2events);

		assertEquals(0, crashevents.size());
		assertEquals(0, recoverevents.size());

		assertEquals(0, messageevents.size());

		assertEquals(0, interruptionevents.size());

		assertEquals(nbcall*getSizeSystem(), startcallhelloevents.size());
		assertEquals(nbcall*getSizeSystem(), endcallhelloevents.size());
		assertEquals(nbcall*getSizeSystem(), startcallhello2events.size());
		assertEquals(nbcall*getSizeSystem(), endcallhello2events.size());

		for(int i=0; i< getSizeSystem();i++) {

			Report subr1 = r.subreport(new  LogCallEventSelector(IDNode.get(i), nameproto, "hello"));
			Report subr2 = r.subreport(new  LogCallEventSelector(IDNode.get(i), nameproto, "hello2"));
			Report subr3 = subr1.merge(subr2, "merge_"+subr1.getName()+"_"+subr2.getName());
			subr3.analyse(new LogEventVisitor() {

				LogCallEvent previous = null;

				@Override
				public void visit(LogStartCallEvent e) {
					if(previous == null) {
						previous=e;
						return;
					}
					if(e.getNameprimitive().equals("hello")) {
						assertTrue(e.getID().getDate() >= previous.getID().getDate()+time_after_hello2);
						assertEquals("hello2",previous.getNameprimitive());
					}else if(e.getNameprimitive().equals("hello2")) {
						assertTrue(e.getID().getDate() >= previous.getID().getDate()+time_after_hello);
						assertEquals("hello",previous.getNameprimitive());
					}else {
						assertFalse(true);
					}
					assertEquals(e.getNumcall(),previous.getNumcall()+1);
					assertTrue(previous instanceof LogEndCallEvent);
					previous=e;
				}

				@Override
				public void visit(LogEndCallEvent e) {
					assertNotNull(previous);
					if(e.getNameprimitive().equals("hello")) {
						assertEquals("hello",previous.getNameprimitive());
					}else if(e.getNameprimitive().equals("hello2")) {
						assertEquals("hello2",previous.getNameprimitive());
					}else {
						assertFalse(true);
					}
					assertTrue(e.getID().getDate() >= previous.getID().getDate());
					assertEquals(e.getNumcall(),previous.getNumcall());
					assertTrue(previous instanceof LogStartCallEvent);
					previous=e;
				}
			});				
		}


	}
}
