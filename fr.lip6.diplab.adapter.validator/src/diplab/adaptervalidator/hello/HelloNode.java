package diplab.adaptervalidator.hello;


import diplab.core.Loggable;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.Primitive;

public  class HelloNode extends NodeProto {

	@Loggable
	int nbcallhello=0;
	
	@Loggable
	int nbcallhello2=0;
	
	public HelloNode(NodeSystem system) {
		super(system);
	}
		
	@Primitive
	public void hello() {
		nbcallhello++;
	}
	
	@Primitive
	public void hello2() {
		nbcallhello2+=2;
	}
}
