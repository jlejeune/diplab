package diplab.adaptervalidator.hellosleep;


import diplab.core.Loggable;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.Primitive;

public  class HelloSleepNode extends NodeProto {

	public static final long time_sleep= 100;
	
	@Loggable
	int nbcallhello=0;
	
	
	public HelloSleepNode(NodeSystem system) {
		super(system);
	}
		
	@Primitive
	public void hello() {
		nbcallhello++;
		sleep(time_sleep);
	}
	
}
