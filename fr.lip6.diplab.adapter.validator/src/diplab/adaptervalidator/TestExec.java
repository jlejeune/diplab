package diplab.adaptervalidator;

import java.util.Map;

import diplab.core.NodeProto;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.scenario.Scenario;

public abstract class TestExec{
	
	
	private final int sizeSystem;
	
		
	public TestExec(int sizeSystem) {
		super();
		this.sizeSystem = sizeSystem;
	}
	public abstract Map<String,Class<? extends NodeProto>> getAlgonodeclasses() ;
	public abstract Scenario getScenario() ;
	public abstract void check(Report r) ;
	
	public int getSizeSystem() {
		return sizeSystem;
	}
	
}
