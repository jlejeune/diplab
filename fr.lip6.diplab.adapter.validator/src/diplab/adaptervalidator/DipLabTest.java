package diplab.adaptervalidator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.junit.BeforeClass;

import diplab.core.IDNode;
import diplab.core.experiment.Deterministic;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.ExperimentBuilder;
import diplab.core.experiment.ExperimentController;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.Reports;


public class DipLabTest {

	@BeforeClass
	public static void beforeclass() {
		Logger logRoot = Logger.getRootLogger();
		logRoot.setLevel(Level.INFO);
	    ConsoleAppender ca = new ConsoleAppender();
	    ca.setName("console");
	    ca.setImmediateFlush(true);
	    ca.setTarget("System.err");
	    ca.setLayout(new SimpleLayout());
	    ca.activateOptions();
	    logRoot.addAppender(ca);
	}
	
	public void test( Class<? extends ExperimentController> controller_class, Class<? extends TestExec> testexec_class, int size, long timeout, int nbtimes)  {
		TestExec testexec;
		try {
			testexec = testexec_class.getConstructor(int.class).newInstance(size);
		} catch (Exception e1) {
			throw new IllegalStateException(e1);
		}
		Report pred_report=null;
		
		ExperimentBuilder builder = new ExperimentBuilder(controller_class, timeout)
				.withNodeProtos(testexec.getAlgonodeclasses())
				.withScenario(testexec.getScenario())
				.withSystemSize(size);
		generateConfig(builder);
		for(int i=0; i< nbtimes;i++) {
			System.err.println("\n**********************************");
			System.err.println(controller_class.getSimpleName()+" "+testexec_class.getSimpleName()+" size = "+size+" (attempt "+(i+1)+"/"+nbtimes+")");
			System.err.println("**********************************\n");
						
			Experiment expe = builder.build();
			Map<IDNode,Report> reports = expe.execute();	
			try {	Thread.sleep(500);	} catch (InterruptedException e) {	}
			Report r = Reports.merge(reports.values());
	
			assertEquals(reports.values().stream().map(x->x.size()).reduce((a,b) -> a+b).get().longValue(), r.size());
			if(pred_report != null) {
				assertFalse(r==pred_report);
				if(expe.getExperimentControllerClass().isAnnotationPresent(Deterministic.class) ) {
					assertTrue(r.isEquivalent(pred_report));
				}
				
			}
			pred_report=r;
			testexec.check(r);
		}
	}
	
	public void generateConfig(ExperimentBuilder builder) {}
	
	

}
