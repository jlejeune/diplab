package diplab.adaptervalidator.helloring;

import diplab.core.IDNode;
import diplab.core.Loggable;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.Primitive;
import diplab.core.messaging.MessageHandler;
import diplab.core.messaging.StringMessage;

public  class HelloRingNode extends NodeProto {

	@Loggable
	private IDNode neigbor;
	
	
	public HelloRingNode(NodeSystem system) {
		super(system);
	}

	@Override
	public void init() {
		neigbor=IDNode.get( (idnode().getVal() + 1)% allnodes().size());
				
	}
	
	@Primitive
	public void sayhello() {
		send(StringMessage.class, neigbor, idnode()+"");
	}
	
	@MessageHandler(StringMessage.class)
	public void receiveHello(StringMessage m) {
		if(!m.getContent().equals(idnode()+"")) {
			send(StringMessage.class, neigbor, m.getContent());
		}
			
		
		
		
	}
	
}
