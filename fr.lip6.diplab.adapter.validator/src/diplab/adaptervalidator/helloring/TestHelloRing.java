package diplab.adaptervalidator.helloring;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import diplab.adaptervalidator.TestExec;
import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.logevent.LogCrashEvent;
import diplab.core.experiment.reporting.logevent.LogEndCallEvent;
import diplab.core.experiment.reporting.logevent.LogInterruptionCallEvent;
import diplab.core.experiment.reporting.logevent.LogMessageReceiveEvent;
import diplab.core.experiment.reporting.logevent.LogMessageSentEvent;
import diplab.core.experiment.reporting.logevent.LogRecoverEvent;
import diplab.core.experiment.reporting.logevent.LogStartCallEvent;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;
import diplab.core.experiment.reporting.visitor.selector.LogCallEventSelector;
import diplab.core.experiment.reporting.visitor.selector.LogEventSelector;
import diplab.core.experiment.reporting.visitor.selector.LogNodeProtoEventSelector;
import diplab.core.experiment.scenario.Scenario;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.callscheme.Call;

public class TestHelloRing extends TestExec {




	public TestHelloRing(int sizeSystem) {
		super(sizeSystem);
	}

	private final String nameproto ="HelloRing";

	@Override
	public Map<String,Class<? extends NodeProto>> getAlgonodeclasses() {
		Map<String,Class<? extends NodeProto>> res = new HashMap<>();
		res.put(nameproto, HelloRingNode.class);

		return res;
	}



	@Override
	public Scenario getScenario() {
		Scenario res = new Scenario();
		for(int i = 0; i < getSizeSystem() ; i++) {
			int x=i;
			res.addAction(new CallAction(
					(r)-> 500L * (x+1), 
					new Call(nameproto,"sayhello",(r)->1L),
					IDNode.get(x)
					));
		}		
		return res;
	}


	@Override
	public void check(Report r) {

		LogEventVisitor v1 = new LogEventVisitor() {


			//node->nameproto->id_mess-> log
			Map<IDNode, Map< String ,Map<Long,LogMessageSentEvent>>> map = new HashMap<>();

			@Override
			public void visit(LogMessageSentEvent e) {
				map.putIfAbsent(e.getSender(), new HashMap<>());

				Map< String ,Map<Long,LogMessageSentEvent>> map2 = map.get(e.getSender());
				map2.putIfAbsent(e.getNodeProtoName(), new HashMap<>());

				Map<Long,LogMessageSentEvent> map3 = map2.get(e.getNodeProtoName());

				map3.put(e.getNumSent(), e);
			}

			@Override
			public void visit(LogMessageReceiveEvent e) {				
				assertTrue(map.containsKey(e.getSender()));
				assertTrue(e.getMessage().getDest().equals(e.getID().getNode()));
				IDNode sender = e.getSender();
				String protoname = e.getNodeProtoName();
				long id_message=e.getNumSent();

				LogMessageSentEvent sent_event = map.get(sender).get(protoname).get(id_message);
				assertNotNull(sent_event);
				assertEquals(sender, sent_event.getSender());
				assertEquals(protoname, sent_event.getNodeProtoName());
				long date_reception = e.getID().getDate();
				assertEquals(id_message, sent_event.getNumSent());
				long date_emission = sent_event.getID().getDate();

				assertTrue("inconsistency in message -> receiving date (="+date_reception+") <= sending date (="+date_emission+")",date_reception > date_emission);
				assertEquals(e.getMessage().getSrc(), sent_event.getSender());
				assertEquals(e.getMessage().getSrc(), sent_event.getMessage().getSrc());
				assertEquals(e.getMessage().getDest(), sent_event.getMessage().getDest());
				assertEquals(e.getMessage().getNameProto(), sent_event.getMessage().getNameProto());
				assertEquals(e.getMessage().getId(), sent_event.getMessage().getId());

			}			
		};

		LogEventSelector crashevents = new LogEventSelector(LogCrashEvent.class);
		LogEventSelector recoverevents = new LogEventSelector(LogRecoverEvent.class);
		LogEventSelector sentmessageevents = new LogNodeProtoEventSelector(nameproto,LogMessageSentEvent.class);
		LogEventSelector receivemessageevents = new LogNodeProtoEventSelector(nameproto,LogMessageReceiveEvent.class);
		LogEventSelector interruptionevents = new LogNodeProtoEventSelector(nameproto,LogInterruptionCallEvent.class);

		LogEventSelector startcallhelloevents = new LogCallEventSelector(nameproto, "sayhello", LogStartCallEvent.class);
		LogEventSelector endcallhelloevents = new LogCallEventSelector(nameproto, "sayhello", LogEndCallEvent.class);

		LogEventSelector startcallreceiveHelloevents = new LogCallEventSelector(nameproto, "receiveHello", LogStartCallEvent.class);
		LogEventSelector endcallreceiveHelloevents = new LogCallEventSelector(nameproto, "receiveHello", LogEndCallEvent.class);

		r.analyse(v1,
				crashevents,
				recoverevents,
				sentmessageevents,
				receivemessageevents,
				interruptionevents,
				startcallhelloevents,
				endcallhelloevents,
				startcallreceiveHelloevents,
				endcallreceiveHelloevents
				);

		assertEquals(0, crashevents.size());
		assertEquals(0, recoverevents.size());

		assertEquals(getSizeSystem()*getSizeSystem(),sentmessageevents.size());
		assertEquals(getSizeSystem()*getSizeSystem(), receivemessageevents.size());

		assertEquals(0, interruptionevents.size());


		assertEquals(getSizeSystem(), startcallhelloevents.size());
		assertEquals(getSizeSystem(), endcallhelloevents.size());

		assertEquals(getSizeSystem()*getSizeSystem(), startcallreceiveHelloevents.size());
		assertEquals(getSizeSystem()*getSizeSystem(), endcallreceiveHelloevents.size());


	}

}
