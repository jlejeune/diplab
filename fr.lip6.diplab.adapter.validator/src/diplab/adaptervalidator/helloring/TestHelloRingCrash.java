package diplab.adaptervalidator.helloring;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import diplab.adaptervalidator.TestExec;
import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.logevent.LogCrashEvent;
import diplab.core.experiment.reporting.logevent.LogEndCallEvent;
import diplab.core.experiment.reporting.logevent.LogInterruptionCallEvent;
import diplab.core.experiment.reporting.logevent.LogMessageReceiveEvent;
import diplab.core.experiment.reporting.logevent.LogMessageSentEvent;
import diplab.core.experiment.reporting.logevent.LogRecoverEvent;
import diplab.core.experiment.reporting.logevent.LogStartCallEvent;
import diplab.core.experiment.reporting.visitor.selector.LogCallEventSelector;
import diplab.core.experiment.reporting.visitor.selector.LogEventSelector;
import diplab.core.experiment.reporting.visitor.selector.LogNodeProtoEventSelector;
import diplab.core.experiment.scenario.Scenario;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.CrashAction;
import diplab.core.experiment.scenario.action.callscheme.Call;

public class TestHelloRingCrash extends TestExec {


	public TestHelloRingCrash(int sizeSystem) {
		super(sizeSystem);
	}

	private final String nameproto ="HelloRing";
	@Override
	public Map<String,Class<? extends NodeProto>> getAlgonodeclasses() {
		Map<String,Class<? extends NodeProto>> res = new HashMap<>();
		res.put(nameproto, HelloRingNode.class);

		return res;
	}

	@Override
	public Scenario getScenario() {
		Scenario res = new Scenario();
		res.addAction(new CallAction((r)-> 500L, new Call(nameproto,"sayhello",(r)->1L), IDNode.get(0) ));
		res.addAction(new CrashAction(
				(r)-> 100L,
				(r)-> 5000L,
				IDNode.get(getSizeSystem()/2)
				));		
		res.addAction(new CallAction((r)-> 5500L, new Call(nameproto,"sayhello",(r)->1L),IDNode.get(0) ) );
		return res;
	}

	@Override
	public void check(Report r) {


		LogEventSelector crashevents = new LogEventSelector(LogCrashEvent.class);
		LogEventSelector recoverevents = new LogEventSelector(LogRecoverEvent.class);
		LogEventSelector sentmessageevents = new LogNodeProtoEventSelector(nameproto,LogMessageSentEvent.class);
		LogEventSelector receivemessageevents = new LogNodeProtoEventSelector(nameproto,LogMessageReceiveEvent.class);
		LogEventSelector interruptionevents = new LogNodeProtoEventSelector(nameproto,LogInterruptionCallEvent.class);

		LogEventSelector startcallhelloevents = new LogCallEventSelector(nameproto, "sayhello", LogStartCallEvent.class);
		LogEventSelector endcallhelloevents = new LogCallEventSelector(nameproto, "sayhello", LogEndCallEvent.class);

		LogEventSelector startcallreceiveHelloevents = new LogCallEventSelector(nameproto, "receiveHello", LogStartCallEvent.class);
		LogEventSelector endcallreceiveHelloevents = new LogCallEventSelector(nameproto, "receiveHello", LogEndCallEvent.class);

		r.analyse(crashevents,
				recoverevents,
				sentmessageevents,
				receivemessageevents,
				interruptionevents,
				startcallhelloevents,
				endcallhelloevents,
				startcallreceiveHelloevents,
				endcallreceiveHelloevents
				);

		assertEquals(1, crashevents.size());
		assertEquals(1, recoverevents.size());

		assertEquals(getSizeSystem()/2 + getSizeSystem(),sentmessageevents.size());
		assertEquals(getSizeSystem()/2 + getSizeSystem() -1, receivemessageevents.size());

		assertEquals(0, interruptionevents.size());


		assertEquals(2, startcallhelloevents.size());
		assertEquals(2, endcallhelloevents.size());

		assertEquals(getSizeSystem()/2 + getSizeSystem() -1, startcallreceiveHelloevents.size());
		assertEquals(getSizeSystem()/2 + getSizeSystem() -1, endcallreceiveHelloevents.size());

	}
}
