# What is it ?

DiPLab (**D**istributed **P**rotocols **Lab**oratory) is a Java library providing a simple API to develop and to test experimentally distributed protocols (or distributed algorithms) following an event-based model while being independant of the API of the underlying distributed execution platform.

[Quick start](doc/quick_start.md)

# Why does this tool exist ?

Experimental analysis of distributed protocols is a very strong aspect of the [DELYS team](https://team.inria.fr/delys/)'s research activities. Exprimentation implies necessarily software development step which uses tools or libraries to handle a distributed system model and thus deploy the protocols to be evaluated. Usually we model  a distributed system as a set of computing nodes linked by a network, which do not share any memory and communicates only thanks to messages passing. It exists a lot of tools to implement and to test distributed protocols. However it is possible to classify this tools into two families : **Middleware for real distributed infrastructure** and **Simulators**.

**Middleware for real distributed infrastructure** are tools that consider a set of real remote sites being able to host several processes (= the nodes of the model) which communicate thanks to a real network with sockets. The **M**essage **P**assing **I**interface (MPI) is the most famous standard for message-oriented middleware and offers a large panel of communication primitives. This family brings a better representativeness of a real distributed system  which allows to value experiments. However, the development is not trivial  because  : 
 - it is impossible to have a global view
 - the network brings indeterminism in the message delivery order (making the debug phases harder)
 - the settings are numerous and hard to fix
 - it is usually impossible to run the experiment in a totally controlled infrastructure due to sharing with other users or unpredictable unavailability of system components

**Simulators** are centralized tools. Thanks to a configuration file, the user is able to describe a network (topology, links, latency ...) with its nodes that compose it (terminal, router ...) and then can simulate the execution of distributed applications inside a single process. Simulators simplify debug phases because they are determinist and offer a global view of the simulated system. However, they are limited if we want to consider large-scale system and/or reproduce the real world as faithfully as possible (e.g. by simulating lower layers of the network and the operating systems) leading to a very long simulation time. 

All these tools are complementary but it remains hard to switch from one to the other without changes of the specific code of experiments because each tool offers its own API which could be in different programming languages. Consequently, DipLab helps the protocol experimenter  to make these tools interchangeable  without modifying the experiment code.  

# Features
- Define distributed protocols with an event-based Java API
- Define your own experiment scenarios such as primitive call, node crashes or node recovery
- Choose the events to log during the experiment 
- Execute your experiment over a distributed platform regardless of its API whic can be a discrete event simulator or a real middleware
- Analyse the produced logs by the experiment execution in order to compute metrics or to verify invariants of the protocol
- Define your own diplab adapter if you need to use a specific distributed platform execution

# Provided components
- **DiPLab core** : the main component containing the engine of diplab (i.e. experiment definition, protocol API, reporting)
- **DiPLab adapter validator** : a component to validate adapters linking diplab core and a target platform
- **DiPLab peersim** : a platform adapter for the discrete event-based simulator  [peersim](http://peersim.sourceforge.net).
- **DiPLab multithread** : a platform adapter allowing to run experiment locally in a pseudo-distributed manner 
- **DiPLab MPI** (not stable) : a platform adapter for the MPI standard (actually tested over [Open MPI](https://www.open-mpi.org/) implementation) 
- **DiPLab Socket** : a platform adapter based on TCP java sockets
- **DipLab Monitor** : a tool to replay graphically an step by step an experiment execution from its trace logs (based on Java FX library) 

# Building DipLab

prerequisites to build DiPLab component :
- Java version >= 11 
- [Apache Maven](https://maven.apache.org/)
- for the DiPLab MPI component, you need [Open MPI](https://www.open-mpi.org/) >= 4.1.1 configured and built for java applications (see [here](https://www.open-mpi.org/faq/?category=java))   

Take the whished component directory (parent component to compile all components) as current directory  and launch
`mvn install` (add the option `-DskipTests` to disable tests execution)

This will produce jar files and install them into your local maven repository.

# Testing Code
Use `mvn surefire:test` to test your installation by running a suite of unit tests. 

