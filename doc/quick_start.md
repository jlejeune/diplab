# Quick start


We show here with a simple example : 
- how to define a distributed protocol
- how to configure and launch an experiment
- how to analyse the produced logs


## The Naimi-Trehel's algorithm implementation


The [Naimi-Trehel's algorithm](https://www.sciencedirect.com/science/article/pii/S0743731596900416) is a simple distributed mutual exclusion algorithm where  N nodes are organized  in a dynamic logical tree and based on a token passing. A mutual exclusion protocol offers two primitives as API : 
- `RequestCS` to enter in the critical section, this primitive is blocking until the calling node obtains the token
- `ReleaseCS` to leave the critical section, assuming that the calling node is in critical section and then has the token 


The protocol considers two types of messages : 
- `Request` : containing the id of the requester and sent when a node wishes to enter in the critical section
- `Token` : containing no particular information but just indicating  that the receiver has the right to  enter in critical section.
So we need to define two java classes (one for each message) and two annotated methods  as a message handler.


## Step 1 : defining types of message
We define the class `RequestMessage` which is a message containing the id of a node (the requester).


```java
public class RequestMessage extends Message<IDNode> {


    public RequestMessage(IDNode src, IDNode dest, String nameproto, long id, IDNode content) {
        super(src, dest, nameproto, id, content);
    }


}
```
Thenn we define the `TokenMessage` which is a message with no content. In this case, we consider that the content is an empty `String`. 


```java
public class TokenMessage extends StringMessage {


    public TokenMessage(IDNode src, IDNode dest, String nameproto, long id, String content) {
        super(src, dest, nameproto, id, content);
    }
}
```


## Step 2 : defining the protocol


To define a protocol layer, you have to define a class extending `NodeProto`. The constraint is to define a constructor taking as parameters the reference to the corresponding NodeSystem (a class representing the current node or machine).


```java 
public class NTNode extends NodeProto{
    public NTNode(NodeSystem system) {
        super(system);
    }
    //to be continued
}
```


Then, we define the local variables maitaining by each node in each instance of this protocol. 


```java
    public static enum State{TRANQUIL,REQUESTING,INCS}
    @Loggable //useful for analysis step
    private State state;
    private IDNode next;
    private IDNode last;
```


Then we define the `init` method which is called once the whole node is built but before any event fired by the experiment. 


```java
    private static final IDNode elected_node=IDNode.get(0);
    @Override
    public void init() {
        state=State.TRANQUIL;
        next=null;
        if(idnode().equals(elected_node)) {
            last=null;
        }else {
            last=elected_node;
        }
        
    }
```
We can now define the two primitives `RequestCS` and `ReleaseCS`


```java
    @Primitive
    public void requestCS() {
        state=State.REQUESTING;
        if(last != null) {
            send(RequestMessage.class,last, idnode());
            last=null;
            wait(()->state == State.INCS);
        }
    }


    @Primitive
    public void releaseCS() {
        state=State.TRANQUIL;
        if(next != null) {
            send(TokenMessage.class, next, "");
            next=null;
        }
    }
```
Finally the two message handlers


```java
    @MessageHandler(RequestMessage.class)
    public void receiveRequest(RequestMessage m) {
        IDNode requester = m.getContent();
        if(last==null) {
            if(state != State.TRANQUIL) {
                next= requester;
            }
        }else {
            send(RequestMessage.class, last, requester);
        }
        last=requester;
    }
    
    @MessageHandler(TokenMessage.class)
    public void receiveToken(TokenMessage m) {
        state=State.INCS;
    }
```


The DiPLab engine ensure that each method of a given protocol is executed  exclusively. Consequently, we do not need to manage any concurrent or lock mechanism to avoid inconsistency over local variables. Moreover, if an execution is blocked by a call of the  `wait(BooleanSupplier)` system primitive (because the predicate in the parameter is false), the DiPLab engine will unblock automatically the corresponding thread as soon as the predicate will become true. Thus, there is no need to call any system primitive like `notify`. Indeed, after each event execution, the engine evaluates all predicate of all blocked threads.



## Step 3 : configuring an experiment


To build an experiment, you need to use an `ExperimentBuilder` which allows you to define the configuration. 
An experimentBuilder requires at least two parameters :
- a class of an `ExperimentController`, i.e. a controller defining the system adapter to execute the experiment
- the duration of the experiment (the considered unit is the millisecond)

In this example, we use the `PseudoDistributedExperimentController` class controller and we set the duration to 15000 ms. 
This controller allows to run experiment locally (in the same host) in a pseudo-distributed manner. We choose it here because it does not need to use a third-party library to work and does not require additional configuration parameters. 

```java
ExperimentBuilder builder = new ExperimentBuilder(PseudoDistributedExperimentController.class, 15000L);
```
Now we can configure our experiment : 
 - with 5 nodes
 - by deploying the Naimi-Trehel protocol on all nodes. In a experiment, each instance of protocol is identified by a name. So it is possible to deploy several instances of the same protocol if each instance has its own name. 

```java
builder.withSystemSize(5)
builder.addNodeProto("NT-locking", NTNode.class)
```
Note that the default system size is 5, so the first instruction is optional in this example. 


## Step 4 : defining the scenario

Before building the final experiment object, we need to define a scenario, i.e. a scheduling of events such as primitive calls or node crashes over the experiment's  timeline. If no scenario is defined, then no event will bootstrap the experiment execution leading thus to an execution where the system does nothing. 

In our example, we want that each node makes sequentially 10 critical section. The time of critical section is set to 220 ms and the time between two request is set to 200ms. Here, we use constant value but in a real experiment it is suitable to use mean value to introduce a minimum of variance. 
Since the critical section time is greater than the idle time, a waiting queue will be created between the pending requests.    

We fisrt define the scheme of the primitive calls for each node which a repeated sequence of `requestCS` and `releaseCS` primitives.
```java
CallSequence scheme = new RepeatedCallSequence(10);
scheme.addCallScheme(new Call("NT-locking", "requestCS", new ConstanteValue<>(220)));
scheme.addCallScheme(new Call("NT-locking", "releaseCS", new ConstanteValue<>(200)));
```
Then we need to define on which nodes and when this execution scheme should be triggered. In our case, this scheme is applicable on every node (the  protocol should obviously deployed on the target nodes) and start at t=200.

```java
Action a = new CallAction(new ConstanteValue<Long>(200), scheme);
```
Finally, we can instanciate the scenario (which is collection of actions), add the action to it and link the whole with the experiment builder.
```java
Scenario s = new Scenario();
s.addAction(a);
builder.withScenario(s);
```

## Step 5 : Building the experiment and running it
Once the experiment is ready, we first call the `build` method of the builder to instanciate an object `Experiment` (non-mutable object) and then call the `execute` method.

```java
Experiment expe = builder.build();
Map<IDNode,Report> reports = expe.execute();
```
The last instruction deploys the system with the correct number of nodes, builds each protocol layer on each node according to the configuration of the experiment and then schedule the actions of the scenario. The `execute()` method returns when the end of the experiment is reached.  It returns a map associating  for each node of the system, their respective set of produced logs (= a report).   

## Step 6 : analysing the reports

At the end of an experiment, you can make a post-mortem analysis (e.g. metrics computing, invariants checking ...). All events are logged by default (see [here](event_logging.md) for more information about logging). The report is an ordered set of `LogEvent` according to the chronolical order. 

In our case, we want to check two properties :
- safety : there is at most one node in the critical section 
- liveness : every request is eventually satisfied

Since we do not need to group the reports by node, we first merge all the produced reports into only one, on which we will apply the analysis.

```java
Report report = Reports.merge(reports.values());
```
To check the safety property, we define a `LogEventVisitor` which cares about any `LogCallEvent` to check that the number of node in `INCS`  state is always less or equals to 1.

```java
LogEventVisitor safety = new LogEventVisitor() {

			Map<IDNode,NTNode.State> states=new HashMap<>();

			@Override
			public void visit(LogCallEvent e) {
				IDNode id = e.getID().getNode();
				NTNode.State state = (State) e.getStateOf("state");
				states.put(id, state);

				long cpt = states.values().stream().filter(s-> s == State.INCS).count();
				assert cpt <= 1;
			}

		};
```
To check the liveness property, we have to ensure that the number of beginning and ending of `requestCS` primitive calls are equal to 50 (10 per node). We can use two filters `LogEventSelector` (one for beginning events, one for ending) and then compares their size with the expected value. 

```java
LogEventSelector startcallrequestevents = new LogCallEventSelector(nameproto, "requestCS", LogStartCallEvent.class);
LogEventSelector endcallrequestevents = new LogCallEventSelector(nameproto, "requestCS", LogEndCallEvent.class);
```

Finally, we launch the analysis over the report and we check that the `assert` clauses throw no exception.
```java
report.analyse(safety,startcallrequestevents, endcallrequestevents)
assert 50 == startcallrequestevents.size();
assert 50 == endcallrequestevents.size();
```



