package diplab.util;

import java.util.Random;

public final class RandomUtil {

	
	private RandomUtil() {}
	
	
	/**
	 * Extracts the next integer, according to a Poisson distribution.
	 * 
	 * @param r the random object
	 * @param mean The mean of the Poisson distribution.
	 * @return An integer Poisson extraction.
	 */
	public static  long nextPoisson(Random r, double mean) {
		
		double emean = Math.exp(-1 * mean);
		double product = 1;
		int count = 0;
		int result = 0;
		while (product >= emean) {
			product *= r.nextDouble();
			result = count;
			count++; // keep result one behind
		}
		return result;
	}
	
	/**
	* Implements nextLong(long) the same way nexInt(int) is implemented in
	* java.util.Random.
	* @param r the random object
	* @param max the bound on the random number to be returned. Must be positive.
	* @return a pseudorandom, uniformly distributed long value between 0
	* (inclusive) and n (exclusive).
	*/
	public long nextLong(Random r,long max) {

		if (max<=0)
			throw new IllegalArgumentException("n must be positive");
		
		if ((max & -max) == max)  // i.e., n is a power of 2
		{	
			return r.nextLong()&(max-1);
		}
		
		long bits, val;
		do
		{
			bits = (r.nextLong()>>>1);
			val = bits % max;
		}
		while(bits - val + (max-1) < 0);
		
		return val;
	}
	
//	public long nextUniformBetween(long min, long max) {
//		if(min> max) throw new IllegalArgumentException("min (="+min+") > max(="+max+")");
//		this.ne
//	}
}
