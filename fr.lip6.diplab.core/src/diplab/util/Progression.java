package diplab.util;

import java.text.DecimalFormat;



public class Progression {

	//private static Logger logger = Logger.getRootLogger();
	
	private final Number max;
	
	private final String name;
	private final Number stepprogress;
	private final ProgressionListener[] listeners;
	private final long start = System.currentTimeMillis();
	
	
	private long previous_timestamp;
	private double nextthreshold=0.0;
	private Number curProgress=0;
	
	public Progression(String name, Number max, Number stepprogress) {
		this(name,max,stepprogress,defaultProgressionListener);
	}
	
	
	public Progression(String name, Number max, Number stepprogress, ProgressionListener ... listeners) {
		this.name=name;
		this.max=max;
		this.stepprogress=stepprogress;
		nextthreshold-=stepprogress.doubleValue();
		this.listeners=listeners;
		this.previous_timestamp = start;
	}
	
	public void progress(Number p) {
		curProgress=p;
		double percent = getProgressPercent();
		if(percent - (nextthreshold+stepprogress.doubleValue()) >= 0.0  || percent - 100.0>= 0) {
			nextthreshold=percent;
			for(ProgressionListener l : listeners) {
				l.newProgressStep(this);
			}
			previous_timestamp=System.currentTimeMillis();
		}
		
	}
	
	public Number getMax() {
		return max;
	}
	
	public String getName() {
		return name;
	}
	
	public double getProgressPercent() {
		 double ratio = getCurProgress().doubleValue() / getMax().doubleValue();
		 return ratio*100.0;
	}
	
	public Number getCurProgress() {
		return curProgress;
	}
	
	public long getTotalEllapsedTime() {
		return System.currentTimeMillis()-start;
	}
	
	public long getEllapsedTimeSinceLastNotification() {
		return System.currentTimeMillis()-previous_timestamp;
	}
	

	@FunctionalInterface
	public static interface ProgressionListener{
		void newProgressStep(Progression p);
	}
	
	public static final ProgressionListener defaultProgressionListener = (p) -> {
		System.err.println("Progress "+p.getName()
							+" = "+new DecimalFormat("#0.00").format(p.getProgressPercent())+ " % "
							+" ("+p.getCurProgress()+" /"+p.getMax()+")"
							+" time ellapsed = "+p.getTotalEllapsedTime()+" ms (+"+p.getEllapsedTimeSinceLastNotification()+" ms)");
	};
	
}
