package diplab.core.experiment;

import java.util.Map;

import diplab.core.IDNode;
import diplab.core.experiment.reporting.Report;

public interface ExperimentController {
	public Map<IDNode,Report> execute(Experiment e);
}
