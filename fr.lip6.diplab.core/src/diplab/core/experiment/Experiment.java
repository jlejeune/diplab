package diplab.core.experiment;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.ReportFactory;
import diplab.core.experiment.reporting.ReportWatcher;
import diplab.core.experiment.reporting.visitor.selector.LogEventSelector;
import diplab.core.experiment.scenario.Scenario;

public class Experiment implements Serializable {

	private static final long serialVersionUID = 8845721758817273820L;
	private final Scenario scenario;
	private final int seed;
	private final Class<? extends ExperimentController> experimentControllerClass;
	private final Map<String,Class<? extends NodeProto>> nodeProtoClasses;
	private final int nbNode;
	private final long experimentDuration;
	private final long timestamp;
	private final Map<String,Object> properties;
	private final File reportsdirectory;
	private final ReportFactory reportfactory;
	private final Collection<LogEventSelector> logEventSelectors;
	private final Collection<ReportWatcher> reportWatchers;
	
	private final Map<String,Set<IDNode>> deploymentmap;
	
	Experiment(long timestamp, Scenario scenario, int seed, Class<? extends ExperimentController> experimentControllerClass,
			Map<String, Class<? extends NodeProto>> nodeProtoClasses, int nb_node, long experimentDuration,
			Map<String,Object> properties,
			Map<String,Set<IDNode>> deploymentmap,
			File reportsdirectory,
			ReportFactory reportfactory,
			Collection<LogEventSelector> logEventSelectors,
			Collection<ReportWatcher> reportWatchers) {
		this.timestamp=timestamp;
		this.scenario = scenario;
		this.seed = seed;
		this.experimentControllerClass = experimentControllerClass;
		this.nodeProtoClasses = nodeProtoClasses;
		this.nbNode = nb_node;
		this.experimentDuration = experimentDuration;
		this.properties=properties;
		this.deploymentmap=deploymentmap;
		this.reportsdirectory=reportsdirectory;
		this.reportfactory=reportfactory;
		this.logEventSelectors=logEventSelectors;
		this.reportWatchers=reportWatchers;
	}

	public Class<? extends ExperimentController> getExperimentControllerClass(){
		return experimentControllerClass;
	}
	
	public Map<String,Class<? extends NodeProto>> getNodeProtoClasses(){
		return nodeProtoClasses;
	}
	
	public long getTimestamp() {
		return timestamp;
	}

	public int getNbNode() {
		return nbNode;
	}
	
	
	public long getExperimentDuration() {
		return experimentDuration;
	}

	public Scenario getScenario() {
		return scenario;
	}
	
	public Map<IDNode,Report> execute() {
		try {
			return getExperimentControllerClass().getConstructor().newInstance().execute(this);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		return null;
	}
	public int getSeed() {
		return seed;
	}

	public Map<String,Object> getProperties() {
		return properties;
	}

	public Map<String,Set<IDNode>> getDeploymentmap() {
		return deploymentmap;
	}

	@Override
	public int hashCode() {
		return Objects.hash(timestamp);
	}

	public File getReportsdirectory() {
		return reportsdirectory;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Experiment)) {
			return false;
		}
		Experiment other = (Experiment) obj;
		return timestamp == other.timestamp;
	}

	public ReportFactory getReportfactory() {
		return reportfactory;
	}

	public Collection<LogEventSelector> getLogEventSelectors() {
		return logEventSelectors;
	}

	public Collection<ReportWatcher> getReportWatchers() {
		return reportWatchers;
	}
	
}
