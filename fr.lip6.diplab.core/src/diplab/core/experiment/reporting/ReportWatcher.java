package diplab.core.experiment.reporting;

import java.io.Serializable;

import diplab.core.experiment.reporting.logevent.LogEvent;

@FunctionalInterface
public interface ReportWatcher extends  Serializable {

	void newLogEventAdded(Report report, LogEvent newlogevent);
}
