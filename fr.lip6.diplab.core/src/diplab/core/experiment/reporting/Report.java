package diplab.core.experiment.reporting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import diplab.core.experiment.Experiment;
import diplab.core.experiment.reporting.logevent.LogEvent;
import diplab.core.experiment.reporting.serializer.RawFileReportSerializer;
import diplab.core.experiment.reporting.serializer.ReportSerializer;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;
import diplab.core.experiment.reporting.visitor.selector.LogEventSelector;

/**
 * An object which stores all log events of an experiment.
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public interface Report extends Iterable<LogEvent> {

	/**
	 * Add a new log event in the report but only if it is authorized by 
	 * the logevent selectors of the experiment.  
	 * @param e the new log event to add in the report
	 */
	default void addCarefully(LogEvent e) {
		Collection<LogEventSelector> selectors= getExperiment().getLogEventSelectors();
		boolean ok=selectors.isEmpty();
		for(LogEventSelector selector : selectors) {
			selector.resetSelected();
			e.accept(selector);
			if(!selector.get().isEmpty()) {
				ok=true;
				selector.resetSelected();
				break;
			}
		}
		if(ok) {
			add(e);
			for(ReportWatcher w : getExperiment().getReportWatchers()) {
				w.newLogEventAdded(this, e);
			}
		}
	}
	
	
	/**
	 * Add a new log event in the report. Should be thread safe.
	 * @param e the new log event to add in the report
	 */
	void add(LogEvent e);
	
	/**
	 * Merges the current report with another one. 
	 * @param other the other report we want to merge with
	 * @return a new report resulting from the merging of instance and other
	 */
	default Report merge(Report other, String namemerge) {
			
		Report res = getExperiment().getReportfactory().createNewReport(getExperiment(), namemerge);
		Iterator<LogEvent> itthis=iterator();
		Iterator<LogEvent> itother=other.iterator();
			
		LogEvent evthis=null;
		LogEvent evother=null;
		while(itthis.hasNext() || itother.hasNext() || evthis!=null || evother !=null) {
			if(itthis.hasNext() && evthis ==null) {
				evthis=itthis.next();
			}
			if(itother.hasNext() && evother == null) {
				evother = itother.next();
			}
			if(evthis == null) {
				res.add(evother);
				evother=null;
			}else if(evother == null) {
				res.add(evthis);
				evthis=null;
			}else {
				int tmp = evthis.compareTo(evother);
				if(tmp<0) {
					res.add(evthis);
					evthis=null;
				}else if(tmp > 0) {
					res.add(evother);
					evother=null;
				}else {
					res.add(evthis);
					res.add(evother);
					evthis=null;
					evother=null;
				}
			}

		}
		return res;
		
		
	}
	
	/**
	 * Transform the report into a list of LogEvent sorted in the chronological order 
	 * @return the list the log events of the report
	 */
	default List<LogEvent> toList(){
		List<LogEvent> res = new ArrayList<>();
		analyse(new LogEventVisitor() {
			public void visit(LogEvent e) {
				res.add(e);
			}
		});
		return res;
	}
	
	
	
	/**
	 * Produces a sub report containing only log events according to 
	 * a given {@link LogEventSelector}
	 * @param selector the descriptor of the selection
	 * @return a new report with a subset of log event respecting the parameter
	 */
	default Report subreport(LogEventSelector selector) {
		return subreports(selector).get(0);
	}
	

	/**
	 * Produces a list of report for a given list of {@link LogEventSelector}
	 * The ith selector in the resulting list corresponds with the ith given selector
	 * @param selectors a list of log event selector
	 * @return the list of subreports
	 */
	default List<Report> subreports(List<LogEventSelector> selectors) {
		return subreports(selectors.toArray(new LogEventSelector[0]));
	}
		
	/**
	 * Produces a list of report for a given list of {@link LogEventSelector}
	 * The ith selector in the resulting list corresponds with the ith given selector
	 * @param selectors a list of log event selector
	 * @return the list of subreports
	 */
	default List<Report> subreports(LogEventSelector... selectors) {
		
		analyse(selectors);
		
		List<Report> res=  Arrays.stream(selectors).map( (selector)->{
			Report report = getExperiment().getReportfactory().createNewReport(getExperiment(), getName()+"sub"+System.currentTimeMillis());
			selector.get().forEach(logevent->report.add(logevent));
			return report;
		}).collect(Collectors.toList());
		return Collections.unmodifiableList(res);
		
	}
	
	/**
	 * Produces a report with an union of elements matching any {@link LogEventSelector}
	 * @param selectors a list of log event selector
	 * @return a report containing all element which match with at least one {@link LogEventSelector}
	 */
	default Report subreport(LogEventSelector... selectors) {
		return Reports.merge(subreports(selectors));
	}
	
	/**
	 * Sort the Log Events according to  increasing 
	 * timestamp and then browses all element sequentially by applying 
	 * each visitor in the paramater.
	 * @param visitors visitors to apply for each log event
	 */
	default void analyse(LogEventVisitor... visitors) {
		analyse(Arrays.asList(visitors));
	}

	
	/**
	 * Sort the Log Events according to  increasing 
	 * timestamp and then browses all element sequentially by applying 
	 * each visitor in the paramater.
	 * @param visitors visitors to apply for each log event
	 */
	default void analyse(Collection<LogEventVisitor> visitors) {
		for(LogEvent e : this) {
			for(LogEventVisitor visitor : visitors) {
				e.accept(visitor);
			}
		}
	}
	
	/**
	 * Check if two reports are the same logs 
	 * @param other the other report
	 * @return true if the two reports are exactly the same logevent, false otherwise
	 */
	default boolean isEquivalent(Report other) {
		Iterator<LogEvent> itthis = this.iterator();
		Iterator<LogEvent> itother = other.iterator();
		while(itthis.hasNext() && itother.hasNext()) {
			if(!itthis.next().equals(itother.next())) return false;
		}
		return !itthis.hasNext() && ! itother.hasNext();
	}
	

	/**
	 * Exports the report as a file to a given directory
	 * @param directorypath the directory where the report file will  be created
	 */
	default void exportTo(String directorypath) {
		getReportSerializer().export(directorypath,this);
	}
	
	
	/**
	 * Exports the report as a file to the defined report directory of the experiment
	 */
	default void export() {
		exportTo(getExperiment().getReportsdirectory().getAbsolutePath());
	}
	
	default ReportSerializer getReportSerializer() {
		return RawFileReportSerializer.instance();
	}
	
	default long getMaxDate() {
		class MaxDate{
			long max=0;
		}
		final MaxDate max= new MaxDate();
		analyse(new LogEventVisitor() {

			@Override
			public void visit(LogEvent e) {
				long date = e.getID().getDate();
				if(date > max.max) max.max= date;
			}
		});
		return max.max;
	}
	
	/**
	 * Returns the associated experiment
	 * @return the associated experiment
	 */
	Experiment getExperiment();
	
	/**
	 * Returns the name of the report, should be unique for a given experiment 
	 * @return the the name of the report
	 */
	String getName();
	
	/**
	 * Returns the number of logevent in the report
	 * @return the number of logevent in the report
	 */
	long size();
}
