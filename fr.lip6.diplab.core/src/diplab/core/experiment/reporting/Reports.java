package diplab.core.experiment.reporting;

import java.util.Arrays;
import java.util.Collection;

public final class Reports {

	private Reports() {}
	
	
	public static Report merge(Report... reports) {
		return merge(Arrays.asList(reports));
	}
	
	public static Report merge(Collection<Report> reports) {
		return reports.parallelStream().reduce((r1,r2)->r1.merge(r2, "merge_"+r1.getName())).get();
	}
	
	public static void export(Report... reports) {
		export(Arrays.asList(reports));
	}
	
	public static void export(Collection<Report> reports) {
		reports.parallelStream().forEach(r->r.export());
	}
	
	public static void exportTo(String directory,Report... reports) {
		exportTo(directory, Arrays.asList(reports));
	}
	public static void exportTo(String directory,Collection<Report> reports) {
		reports.parallelStream().forEach(r->r.exportTo(directory));
	}
}
