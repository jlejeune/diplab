package diplab.core.experiment.reporting;

import diplab.core.experiment.reporting.logevent.LogEvent;
import diplab.util.Progression;

public class ExperimentProgressIndicator implements ReportWatcher {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private transient Progression progression = null;
	
	
	@Override
	public void newLogEventAdded(Report report, LogEvent newlogevent) {
				
		if(progression == null) {
			progression=new Progression("Expe"+report.getExperiment().getTimestamp() ,report.getExperiment().getExperimentDuration(), 1);
		}
		progression.progress(newlogevent.getID().getDate());


	}

}
