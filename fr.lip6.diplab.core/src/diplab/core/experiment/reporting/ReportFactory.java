package diplab.core.experiment.reporting;

import java.io.Serializable;

import diplab.core.experiment.Experiment;

@FunctionalInterface
public interface ReportFactory extends Serializable {

	Report createNewReport(Experiment e,String name);
}
