package diplab.core.experiment.reporting.visitor.selector;

import diplab.core.IDNode;
import diplab.core.experiment.reporting.logevent.LogMessageEvent;
import diplab.core.experiment.reporting.logevent.LogNodeProtoEvent;
import diplab.core.messaging.Message;

public class LogMessageEventSelector extends LogNodeProtoEventSelector {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Class<? extends Message<?>> type_message;
	
	
	public LogMessageEventSelector(String nameproto, Class<? extends Message<?>> type_message){
		this(null,0L,Long.MAX_VALUE, nameproto, type_message);
	}
	@SafeVarargs
	public LogMessageEventSelector(String nameproto,
			Class<? extends Message<?>> type_message,
			Class<? extends LogMessageEvent>... types_logevent){
		this(null,0L,Long.MAX_VALUE, nameproto, type_message, types_logevent);
	}
	
	
	public LogMessageEventSelector(IDNode node, String nameproto, Class<? extends Message<?>> type_message){
		this(node,0L,Long.MAX_VALUE, nameproto, type_message);
	}
	@SafeVarargs
	public LogMessageEventSelector(IDNode node,
			String nameproto,
			Class<? extends Message<?>> type_message,
			Class<? extends LogMessageEvent>... types_logevent){
		this(node,0L,Long.MAX_VALUE, nameproto, type_message, types_logevent);
	}
	
	
	public LogMessageEventSelector(IDNode node, Long fromdate, String nameproto, Class<? extends Message<?>> type_message){
		this(node,fromdate,Long.MAX_VALUE, nameproto, type_message);
	}
	@SafeVarargs
	public LogMessageEventSelector(IDNode node,
			Long fromdate,
			String nameproto,
			Class<? extends Message<?>> type_message,
			Class<? extends LogMessageEvent>... types_logevent){
		this(node,fromdate,Long.MAX_VALUE, nameproto, type_message, types_logevent);
	}
	
	
	public LogMessageEventSelector(IDNode node, Long fromdate, Long untildate, String nameproto, Class<? extends Message<?>> type_message){
		super(node,fromdate,untildate, nameproto);
		this.type_message=type_message;
	}
	@SafeVarargs
	public LogMessageEventSelector(IDNode node,
			Long fromdate,
			Long untildate,
			String nameproto,
			Class<? extends Message<?>> type_message,
			Class<? extends LogMessageEvent>... types_logevent){
		super(node,fromdate,untildate, nameproto, types_logevent);
		this.type_message=type_message;
	}
	
	@Override
	public final void visit(LogNodeProtoEvent e) {}

	@Override
	public void visit(LogMessageEvent e) {
		
		if(!e.getMessage().getClass().equals(type_message)) return;
		
		super.visit((LogNodeProtoEvent)e);
	}

	
	
	
	
	
}
