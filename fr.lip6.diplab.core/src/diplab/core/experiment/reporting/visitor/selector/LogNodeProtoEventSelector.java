package diplab.core.experiment.reporting.visitor.selector;

import diplab.core.IDNode;
import diplab.core.experiment.reporting.logevent.LogEvent;
import diplab.core.experiment.reporting.logevent.LogNodeProtoEvent;

public class LogNodeProtoEventSelector extends LogEventSelector {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String nameproto;
	
	
	public LogNodeProtoEventSelector(String nameproto){
		this(null,0L, Long.MAX_VALUE,nameproto);
	}
	@SafeVarargs
	public LogNodeProtoEventSelector(String nameproto, Class<? extends LogNodeProtoEvent>... types_logevent){
		this(null,0L, Long.MAX_VALUE,nameproto, types_logevent);
	}
	
	
	public LogNodeProtoEventSelector(IDNode node,  String nameproto){
		this(node,0L, Long.MAX_VALUE,nameproto);
	}
	@SafeVarargs
	public LogNodeProtoEventSelector(IDNode node,  String nameproto, Class<? extends LogNodeProtoEvent>... types_logevent){
		this(node,0L, Long.MAX_VALUE,nameproto, types_logevent);
	}
	
	
	
	public LogNodeProtoEventSelector(IDNode node, Long fromdate,  String nameproto){
		this(node,fromdate, Long.MAX_VALUE,nameproto);
	}
	@SafeVarargs
	public LogNodeProtoEventSelector(IDNode node, Long fromdate,  String nameproto, Class<? extends LogNodeProtoEvent>... types_logevent){
		this(node,fromdate, Long.MAX_VALUE,nameproto, types_logevent);
	}
	
	
	public LogNodeProtoEventSelector(IDNode node, Long fromdate, Long untildate, String nameproto){
		super(node,fromdate,untildate);
		this.nameproto=nameproto;
	}
	@SafeVarargs
	public LogNodeProtoEventSelector(IDNode node, Long fromdate, Long untildate, String nameproto, Class<? extends LogNodeProtoEvent>... types_logevent){
		super(node,fromdate,untildate, types_logevent);
		this.nameproto=nameproto;
	}
	
	
	@Override
	public final void visit(LogEvent e) {	}//come back to the default behaviour

	@Override
	public void visit(LogNodeProtoEvent e) {
		if(nameproto!=null) {
			if(!nameproto.equals(e.getNodeProtoName())) return;
		}		
		super.visit((LogEvent)e);
	}

	
	
	
	
}
