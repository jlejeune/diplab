package diplab.core.experiment.reporting.visitor.selector;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import diplab.core.IDNode;
import diplab.core.experiment.reporting.logevent.LogEvent;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;

public class LogEventSelector implements LogEventVisitor, Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final IDNode node;
	private final Long fromdate;
	private final Long untildate;
	private final Collection<Class<? extends LogEvent>> types_logevent = new HashSet<>();
	
	private /*transient*/ final List<LogEvent> selected=new ArrayList<>();
	
	
	public LogEventSelector(){
		this(null, 0L, Long.MAX_VALUE);
	}
	@SafeVarargs
	public LogEventSelector(Class<? extends LogEvent>... types_logevent){
		this(null, 0L, Long.MAX_VALUE,types_logevent);
	}
	
	
	public LogEventSelector(IDNode node){
		this(node, 0L, Long.MAX_VALUE);
	}
	@SafeVarargs
	public LogEventSelector(IDNode node,Class<? extends LogEvent>... types_logevent){
		this(node, 0L, Long.MAX_VALUE,types_logevent);
	}
	
	
	public LogEventSelector(IDNode node, Long fromdate){
		this(node, fromdate, Long.MAX_VALUE);
	}
	@SafeVarargs
	public LogEventSelector(IDNode node, Long fromdate, Class<? extends LogEvent>... types_logevent){
		this(node, fromdate, Long.MAX_VALUE,types_logevent);
	}
	
	
	public LogEventSelector(IDNode node, Long fromdate, Long untildate){
		this.node=node;
		this.fromdate=fromdate;
		this.untildate=untildate;
	}
	
	@SafeVarargs
	public LogEventSelector(IDNode node, Long fromdate, Long untildate, Class<? extends LogEvent>... types_logevent){
		this(node,fromdate,untildate);
		this.types_logevent.addAll(Arrays.asList(types_logevent));
	}
	
	@Override
	public void visit(LogEvent e) {
		if(node != null) {
			if(!node.equals(e.getID().getNode())) return;
		}
		if(e.getID().getDate() < fromdate ) return;
		if(untildate < e.getID().getDate() ) return;
		
		if(!types_logevent.isEmpty()) {
			boolean flag=false;
			for(Class<? extends LogEvent> cl : types_logevent) {
				if(cl.isAssignableFrom(e.getClass())) {
					flag=true;
					break;
				}
			}
			if(!flag) return;
		}
		selected.add(e);
	}
	
	public List<LogEvent> get(){
		return Collections.unmodifiableList(selected);
	}
	
	public int size(){
		return selected.size();
	}
	
	public void resetSelected() {
		selected.clear();
	}
	
}
