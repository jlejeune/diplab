package diplab.core.experiment.reporting.visitor.selector;

import diplab.core.IDNode;
import diplab.core.experiment.reporting.logevent.LogCallEvent;
import diplab.core.experiment.reporting.logevent.LogNodeProtoEvent;

public class LogCallEventSelector extends LogNodeProtoEventSelector {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String nameprimitive;
	
	
	public LogCallEventSelector(String nameproto, String nameprimitive){
		this(null,0L,Long.MAX_VALUE, nameproto,nameprimitive);

	}
	
	
	@SafeVarargs
	public LogCallEventSelector(
			String nameproto,
			String nameprimitive,
			Class<? extends LogCallEvent>... types_logevent){
		this(null,0L,Long.MAX_VALUE, nameproto,nameprimitive, types_logevent);

	}
	
	
	public LogCallEventSelector(IDNode node,   String nameproto, String nameprimitive){
		this(node,0L,Long.MAX_VALUE, nameproto,nameprimitive);

	}
	
	
	@SafeVarargs
	public LogCallEventSelector(IDNode node,
			String nameproto,
			String nameprimitive,
			Class<? extends LogCallEvent>... types_logevent){
		this(node,0L,Long.MAX_VALUE, nameproto,nameprimitive, types_logevent);

	}
	
	
	public LogCallEventSelector(IDNode node, Long fromdate,  String nameproto, String nameprimitive){
		this(node,fromdate,Long.MAX_VALUE, nameproto,nameprimitive);

	}
	@SafeVarargs
	public LogCallEventSelector(IDNode node,
			Long fromdate,
			String nameproto,
			String nameprimitive,
			Class<? extends LogCallEvent>... types_logevent){
		this(node,fromdate,Long.MAX_VALUE, nameproto,nameprimitive, types_logevent);

	}
	
	
	
	public LogCallEventSelector(IDNode node, Long fromdate, Long untildate, String nameproto, String nameprimitive){
		super(node,fromdate,untildate, nameproto);
		this.nameprimitive=nameprimitive;
	}
	@SafeVarargs
	public LogCallEventSelector(IDNode node,
			Long fromdate,
			Long untildate,
			String nameproto,
			String nameprimitive,
			Class<? extends LogCallEvent>... types_logevent){
		super(node,fromdate,untildate, nameproto, types_logevent);
		this.nameprimitive=nameprimitive;
	}
	
	@Override
	public final void visit(LogNodeProtoEvent e) {}
	@Override
	public void visit(LogCallEvent e) {
		
		if(nameprimitive!=null) {
			if(!nameprimitive.equals(e.getNameprimitive()))return;
		}
		
		super.visit((LogNodeProtoEvent)e);
	}
	
	
	
}
