package diplab.core.experiment.reporting.visitor;

import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.logevent.LogCallEvent;
import diplab.core.experiment.reporting.logevent.LogCrashEvent;
import diplab.core.experiment.reporting.logevent.LogEndCallEvent;
import diplab.core.experiment.reporting.logevent.LogEndInterruptionCallEvent;
import diplab.core.experiment.reporting.logevent.LogEvent;
import diplab.core.experiment.reporting.logevent.LogInterruptionCallEvent;
import diplab.core.experiment.reporting.logevent.LogMessageEvent;
import diplab.core.experiment.reporting.logevent.LogMessageReceiveEvent;
import diplab.core.experiment.reporting.logevent.LogMessageSentEvent;
import diplab.core.experiment.reporting.logevent.LogNodeProtoEvent;
import diplab.core.experiment.reporting.logevent.LogRecoverEvent;
import diplab.core.experiment.reporting.logevent.LogStartCallEvent;
import diplab.core.experiment.reporting.logevent.LogStartInterruptionCallEvent;
import diplab.core.experiment.reporting.logevent.LogUserEvent;
/**
 * An interface to define a visitor of {@link LogEvent} objects. 
 * The main usage of this type  is the method 
 * {@link Report#analyse(LogEventVisitor...)} to visit the whole LogEvent
 * of a report.
 * 
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public interface LogEventVisitor {

	default void visit(LogEvent e) {}
	default void visit(LogNodeProtoEvent e) {}
		
	default void visit(LogCrashEvent e) {}
	default void visit(LogRecoverEvent e) {}
			
	default void visit(LogCallEvent e) {}
	default void visit(LogStartCallEvent e) {}
	default void visit(LogEndCallEvent e) {}
	default void visit(LogUserEvent e) {}
	
	default void visit(LogInterruptionCallEvent e) {}
	default void visit(LogStartInterruptionCallEvent e) {}
	default void visit(LogEndInterruptionCallEvent e) {}
	
	
	default void visit(LogMessageEvent e) {}
	default void visit(LogMessageSentEvent e) {}
	default void visit(LogMessageReceiveEvent e) {}
	
	
}
