package diplab.core.experiment.reporting.visitor;

import diplab.core.experiment.reporting.logevent.LogEvent;

public final class PrintVisitor implements LogEventVisitor {

	private static PrintVisitor instance = null;
	
	private PrintVisitor() {
	}
	
	public static PrintVisitor get() {
		if(instance == null) {
			instance = new PrintVisitor();
		}
		return instance;
	}

	@Override
	public void visit(LogEvent e) {
		System.out.println(e);
	}
}
