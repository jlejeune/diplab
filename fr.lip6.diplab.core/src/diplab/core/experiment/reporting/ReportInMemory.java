package diplab.core.experiment.reporting;


import java.io.Serializable;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

import diplab.core.experiment.Deterministic;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.reporting.logevent.LogEvent;



public class ReportInMemory extends AbstractReport implements Report, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final NavigableSet<LogEvent> events = new TreeSet<>();
	
	private final boolean potential_concurrent_add;
	
	public ReportInMemory(Experiment experiment, String name) {
		super(experiment,name);
		if(experiment.getExperimentControllerClass() != null) {
			potential_concurrent_add = !experiment.getExperimentControllerClass().isAnnotationPresent(Deterministic.class);
		}else {//dummy controller in the other case (test case)
			potential_concurrent_add=false;
		}
		
	}
	
	@Override
	public void add(LogEvent e) {
		if(potential_concurrent_add) {
			add0(e);
		}else {//if deterministic as a discrete event simulator, the lock is useless
			events.add(e);
		}
		
	}
	
	private void add0(LogEvent e) {
		synchronized (this) {
			  // it may happen an inconsistency between size() and real size of tree
			  //due to concurrent adding of MEssageReceptionEvent and other callEvents
			events.add(e);
		}
	}
	
	@Override
	public String toString() {
		return events.toString();
	}

	@Override
	public Iterator<LogEvent> iterator() {
		return events.iterator();
	}

	@Override
	public synchronized long size() {
		if(potential_concurrent_add) {
			synchronized (this) {
				return events.size();
			}
		}else {
			return events.size();
		}
		
		
	}
	
	
}
