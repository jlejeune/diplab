package diplab.core.experiment.reporting.serializer;

import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.ReportFactory;

public interface ReportSerializer {

	void export(String directorypath, Report r);
	Report load(String filereportpath,  ReportFactory factory);
}
