package diplab.core.experiment.reporting.serializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import diplab.core.experiment.Experiment;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.ReportFactory;
import diplab.core.experiment.reporting.logevent.LogEvent;
import diplab.util.Progression;

public class RawFileReportSerializer implements ReportSerializer {

	private static RawFileReportSerializer instance=null;
	
	private RawFileReportSerializer() {	}
	
	
	public static ReportSerializer instance() {
		if(instance == null) {
			instance=new RawFileReportSerializer();
		}
		return instance;
	}
	
	@Override
	public void export(String directorypath, Report r) {
		File directory=new File(directorypath);
		if(!directory.exists()) {
			throw new IllegalArgumentException("Directory "+directorypath+" does not exists");
		}
		if(!directory.isDirectory()) {
			throw new IllegalArgumentException("Export path "+directorypath+" exists but it is not a directory");
		}
		long size=r.size();
		File f = new File(directory, r.getName());
		System.err.println("Exporting report "+r.getName()+" to file "+f.getAbsolutePath());
		Progression progression = new Progression("Exporting ", size,5);
		
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f))){
			oos.writeObject(r.getExperiment());
			oos.writeObject(Long.valueOf(r.size()));
			long i=1;
			for(LogEvent l : r) {
				oos.writeObject(l);
				progression.progress(i++);
			}
		} catch (Exception e) {
			throw new IllegalStateException(e);
		} 

	}

	@Override
	public Report load(String filereportpath,  ReportFactory factory) {
		File filereport=new File(filereportpath);
		if(!filereport.exists()) {
			throw new IllegalArgumentException("File report "+filereportpath+" does not exists");
		}
		if(!filereport.isFile()) {
			throw new IllegalArgumentException("File report "+filereportpath+" exists but it is not regular file");
		}
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filereport))) {
			System.err.println("Loading report from file "+filereport.getAbsolutePath());	
			Experiment expe = (Experiment) ois.readObject();
			Report res = factory.createNewReport(expe, filereport.getName());
			long size = ((Long) ois.readObject()).longValue();
			Progression progression = new Progression("Loading ", size,5);
			for(long i=0; i< size; i++) {
				LogEvent log = (LogEvent) ois.readObject();
				res.add(log);
				progression.progress(i);
			}			
			return res;
		} catch (Exception e) {
			throw new IllegalStateException(e);
		} 
				
	}

}
