package diplab.core.experiment.reporting.chunking;

import diplab.core.experiment.reporting.logevent.LogEvent;

public interface Chunk extends Iterable<LogEvent>{
	public void add(LogEvent e);
	public boolean isFull();
	public int size();
	
}
