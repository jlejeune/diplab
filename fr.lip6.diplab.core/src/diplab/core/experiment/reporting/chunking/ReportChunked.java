package diplab.core.experiment.reporting.chunking;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;

import diplab.core.experiment.Experiment;
import diplab.core.experiment.reporting.AbstractReport;
import diplab.core.experiment.reporting.logevent.LogEvent;

public class ReportChunked extends AbstractReport {
	
	private static final long serialVersionUID = 1L;

	private final int sizechunk;
	
	private final  List<Chunk> chunks;
	
	private final ChunkFactory chunkfactory;
		
	public ReportChunked(Experiment experiment, String name, ChunkFactory chunkfactory, int sizechunk) {
		super(experiment, name);
		this.sizechunk=sizechunk;
		this.chunkfactory=chunkfactory;
		chunks=new ArrayList<>();
		chunks.add(chunkfactory.newChunk(this));
	}


	@Override
	public void add(LogEvent e) {
		Chunk chunk = chunks.get(chunks.size()-1);
		if(chunk.isFull()) {
			chunk=chunkfactory.newChunk(this);
			chunks.add(chunk);
		}
		chunk.add(e);		
	}


	@Override
	public long size() {
		return chunks.stream().map(c->Long.valueOf(c.size())).reduce((a,b)->a+b).orElse(0L);
	}

	@Override
	public Iterator<LogEvent> iterator() {
		return new ReportInMemoryChunkedIterator();
	}

	
	public int getSizechunk() {
		return sizechunk;
	}


	private final class ReportInMemoryChunkedIterator implements Iterator<LogEvent> {

		private NavigableMap<LogEvent, Iterator<LogEvent>> state;
		
		public ReportInMemoryChunkedIterator() {
			state=new TreeMap<>();
			for(Chunk chunk : chunks) {
				Iterator<LogEvent> it_log = chunk.iterator();
				if(it_log.hasNext()) {
					state.put(it_log.next(), it_log);
				}
				
			}
		}
		
		@Override
		public boolean hasNext() {
			return !state.isEmpty();
		}

		@Override
		public LogEvent next() {
			LogEvent res = state.firstKey();
			Iterator<LogEvent> it = state.remove(res);
			if(it.hasNext()) {
				state.put(it.next(), it);
			}
			return res;
		}

	}
	
}
