package diplab.core.experiment.reporting.chunking;

@FunctionalInterface
public interface ChunkFactory {

	Chunk newChunk(ReportChunked report);
}
