package diplab.core.experiment.reporting.chunking;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;

import diplab.core.experiment.Experiment;
import diplab.core.experiment.reporting.logevent.LogEvent;

public class ChunkToFile extends ChunkInMemory{
	private static int cpt=0;
	private final Path file;
	private boolean flushed=false;
	
	public ChunkToFile(ReportChunked report) {
		super(report);
		try {
			Experiment e = report.getExperiment();
			Path parent = e.getReportsdirectory().toPath();
			file=Files.createTempFile(parent,report.getName()+"_", ".chunk"+(cpt++));
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					System.err.println("Deleting Chunk "+file);
					Files.delete(file);
				} catch (IOException e) {
				}
			}
			
		},"Cleanup"));
	}
	
	
	
	public void add(LogEvent e) {
		super.add(e);
		if(isFull()) {
			flushed=true;
			try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file.toFile()))) {
				for(LogEvent log : events) {
					oos.writeObject(log);
				}
			} catch (IOException e1) {
				throw new IllegalStateException(e1);
			}
			this.events.clear();
		}
		
	}
	
	public boolean isFull() {
		return super.isFull() || flushed;
	}
	
	public int size() {
		if(flushed) return report.getSizechunk();
		return super.size();
	}
	
	@Override
	public Iterator<LogEvent> iterator() {
		if(!flushed) {
			return super.iterator();
		}
		return new ChunkFileIterator();
	}
	
	public class ChunkFileIterator implements Iterator<LogEvent>{
		
		private LogEvent cur;
		private  ObjectInputStream ois;
		
		public ChunkFileIterator() {
			try {
				ois = new ObjectInputStream(new FileInputStream(file.toFile()));		
				cur =read();
			}catch(Exception e) {
				throw new IllegalStateException(e);
			}
		}
		
		private LogEvent read() throws ClassNotFoundException, IOException {

			
			LogEvent res;
			try {
				res = (LogEvent) ois.readObject();
			}catch(EOFException e) {
				res = null;
				ois.close();
			}

			return res;
		}
		
		@Override
		public boolean hasNext() {
			return cur != null;
		}

		@Override
		public LogEvent next() {
			LogEvent res = cur;
			try {
				cur=read();
			} catch (ClassNotFoundException | IOException e) {
				throw new IllegalStateException(e);
			}
			return res;
		}
		
		protected void finalize() throws Throwable {
			//super.finalize();
			ois.close();
		}
		
	}
	
	
}