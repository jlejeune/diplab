package diplab.core.experiment.reporting.chunking;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

import diplab.core.experiment.reporting.logevent.LogEvent;

public class ChunkInMemory implements Chunk {

	protected final NavigableSet<LogEvent> events = new TreeSet<>();
	protected final ReportChunked report;
	
	
	
	public 	ChunkInMemory(ReportChunked report) {
		this.report=report;
	}
	
	public void add(LogEvent e) {
		if(isFull()) {
			throw new IllegalStateException("Try to add a logevent into a full chunk");
		}
		events.add(e);
	}
	
	public boolean isFull() {
		return events.size() >= report.getSizechunk();
	}
	
	public int size() {
		return events.size();
	}

	@Override
	public Iterator<LogEvent> iterator() {
		return events.iterator();
	}
					
}
