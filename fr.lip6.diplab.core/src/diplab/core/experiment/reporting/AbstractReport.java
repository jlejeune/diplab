package diplab.core.experiment.reporting;

import java.io.Serializable;
import java.util.Objects;

import diplab.core.experiment.Experiment;

public abstract class AbstractReport implements Serializable, Report {

	private static final long serialVersionUID = 2438321773542291002L;
	private final Experiment experiment;
	private final String name;
	
	public AbstractReport(Experiment experiment, String name) {
		this.experiment=experiment;
		this.name=name;
	}

	@Override
	public Experiment getExperiment() {
		return experiment;
	}
	public String getName() {
		return name;
	}

	@Override
	public final boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Report)) {
			return false;
		}
		Report other = (Report) obj;
		if(! Objects.equals(getExperiment(), other.getExperiment())) return false;
		return this.isEquivalent(other);
	}

	
		
	
}
