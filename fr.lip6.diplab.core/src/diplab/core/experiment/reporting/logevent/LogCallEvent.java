package diplab.core.experiment.reporting.logevent;

import java.util.Arrays;
import java.util.List;

import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;

public abstract class LogCallEvent extends LogNodeProtoEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6931327698481975257L;

	
	private final String nameprimitive;
	private final List<Object> args;
	private final long numcall;
	
	
	public LogCallEvent(NodeSystem node, NodeProto nodeproto, String nameprimitive, Object[] args, long numcall) {
		super(node,nodeproto);
		this.nameprimitive = nameprimitive;
		this.args = Arrays.asList(args);
		this.numcall = numcall;
	}
	public String getNameprimitive() {
		return nameprimitive;
	}
	public Object[] getArgs() {
		return args.toArray();
	}
	public long getNumcall() {
		return numcall;
	}
	
	public void accept(LogEventVisitor visitor) {
		super.accept(visitor);
		visitor.visit(this);
	}
	
	public boolean matchWith(LogCallEvent other) {
		return getID().getNode().equals(other.getID().getNode())
				 && getNodeProtoName().equals(other.getNodeProtoName())
				 && getNameprimitive().equals(other.getNameprimitive())
				 && getNumcall() == other.getNumcall();
		
	}
	
}
