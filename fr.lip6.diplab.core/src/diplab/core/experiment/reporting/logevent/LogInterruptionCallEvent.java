package diplab.core.experiment.reporting.logevent;

import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;

public abstract class LogInterruptionCallEvent extends LogCallEvent {

	private static final long serialVersionUID = 5888103613651862585L;
	private final int numinter;
	
	public LogInterruptionCallEvent(NodeSystem node, NodeProto nodeproto, String nameprimitive, Object[] args, long numcall, int num_inter) {
		super(node,nodeproto, nameprimitive, args, numcall);
		this.numinter=num_inter;
	}

	public int getNuminter() {
		return numinter;
	}

	
	public void accept(LogEventVisitor visitor) {
		super.accept(visitor);
		visitor.visit(this);
	}
	
	@Override
	public boolean matchWith(LogCallEvent other) {
		boolean res = true;
		if(other instanceof LogInterruptionCallEvent) {
			LogInterruptionCallEvent othercast = (LogEndInterruptionCallEvent) other;
			res = othercast.numinter == numinter;
		}
		return res && super.matchWith(other);
	}
}
