package diplab.core.experiment.reporting.logevent;

import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;

public class LogUserEvent extends LogNodeProtoEvent {

	private final String message;
	
	public LogUserEvent(NodeSystem node, NodeProto nodeproto, String message) {
		super(node, nodeproto);
		this.message=message;
	}

	private static final long serialVersionUID = 1L;

	public String getMessage() {
		return message;
	}
	
	public void accept(LogEventVisitor visitor) {
		super.accept(visitor);
		visitor.visit(this);
	}
	
}
