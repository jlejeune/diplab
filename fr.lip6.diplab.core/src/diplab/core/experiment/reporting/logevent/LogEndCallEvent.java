package diplab.core.experiment.reporting.logevent;

import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;

public class LogEndCallEvent extends LogCallEvent {

	private static final long serialVersionUID = 2270233480660472771L;

	private final Object result;
		
	
	public LogEndCallEvent(NodeSystem node, NodeProto nodeproto, String nameprimitive, Object[] args, long numcall, Object result) {
		super(node,nodeproto, nameprimitive, args, numcall);
		this.result=result;
	}

	public Object getResult() {
		return result;
	}
	
	
	@Override
	public void accept(LogEventVisitor visitor) {
		super.accept(visitor);
		visitor.visit(this);
	}
}
