package diplab.core.experiment.reporting.logevent;

import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;

public class LogStartInterruptionCallEvent extends LogInterruptionCallEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1526369678317255462L;

	public LogStartInterruptionCallEvent(NodeSystem node, NodeProto nodeproto, String nameprimitive, Object[] args, long numcall,
			int num_inter) {
		super(node,nodeproto, nameprimitive, args, numcall, num_inter);
	}
	
	@Override
	public void accept(LogEventVisitor visitor) {
		super.accept(visitor);
		visitor.visit(this);
	}

}
