package diplab.core.experiment.reporting.logevent;

import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;

public class LogCrashEvent extends LogEvent {

	private static final long serialVersionUID = -639232820366089328L;
	
	
	public LogCrashEvent(NodeSystem node) {
		super(node);
	}


	@Override
	public void accept(LogEventVisitor visitor) {
		super.accept(visitor);
		visitor.visit(this);
	}
	

}
