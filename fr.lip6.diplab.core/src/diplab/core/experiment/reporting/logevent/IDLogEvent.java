package diplab.core.experiment.reporting.logevent;

import java.io.Serializable;

import diplab.core.IDNode;

public final class IDLogEvent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3502442137166345227L;
	private final IDNode node;
	private final long date;
	private final long numevent;
	
	public IDLogEvent(IDNode node, long date, long numevent) {
		this.node=node;
		this.date=date;
		this.numevent=numevent;
	}

	public IDNode getNode() {
		return node;
	}

	public long getDate() {
		return date;
	}

	public long getNumevent() {
		return numevent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (date ^ (date >>> 32));
		result = prime * result + ((node == null) ? 0 : node.hashCode());
		result = prime * result + (int) (numevent ^ (numevent >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IDLogEvent other = (IDLogEvent) obj;
		if (date != other.date)
			return false;
		if (node == null) {
			if (other.node != null)
				return false;
		} else if (!node.equals(other.node))
			return false;
		if (numevent != other.numevent)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[date=" + date + " node=" + node + ", numevent=" + numevent + "]";
	}
	
	
}
