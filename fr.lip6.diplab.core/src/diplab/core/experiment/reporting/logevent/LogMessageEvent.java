package diplab.core.experiment.reporting.logevent;

import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;
import diplab.core.messaging.Message;

public abstract class LogMessageEvent extends LogNodeProtoEvent {

	
	private static final long serialVersionUID = -6705119813961010803L;
	
	
	private final IDNode sender;
	private final long num_sent;
	private final Message<?> message;
	
	public LogMessageEvent(NodeSystem node, NodeProto nodeproto, IDNode sender, long num_sent, Message<?> message) {
		super(node,nodeproto);
		this.sender=sender;
		this.num_sent=num_sent;
		this.message=message;
	}

	public IDNode getSender() {
		return sender;
	}

	public long getNumSent() {
		return num_sent;
	}

	public Message<?> getMessage() {
		return message;
	}

	public void accept(LogEventVisitor visitor) {
		super.accept(visitor);
		visitor.visit(this);
	}
	

}
