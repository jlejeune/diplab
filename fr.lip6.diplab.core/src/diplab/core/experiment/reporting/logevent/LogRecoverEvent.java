package diplab.core.experiment.reporting.logevent;

import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;

public class LogRecoverEvent extends LogEvent {

	private static final long serialVersionUID = 2875837361768899525L;

	public LogRecoverEvent(NodeSystem node) {
		super(node);
	}
	
	@Override
	public void accept(LogEventVisitor visitor) {
		super.accept(visitor);
		visitor.visit(this);
	}
}
