package diplab.core.experiment.reporting.logevent;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;

public abstract class LogEvent implements Serializable, Comparable<LogEvent> {


	private static final long serialVersionUID = -7584671965052870127L;
		
	private final IDLogEvent id;
	
	public LogEvent(NodeSystem node) {
		super();
		this.id=new IDLogEvent(node.idnode(), node.getCurrentTime(), node.newEventNum());
	}
	
	
	public IDLogEvent getID() {
		return id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogEvent other = (LogEvent) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public int compareTo(LogEvent o) {
		if(id.getDate() != o.id.getDate()) {
			return Long.compare(id.getDate(), o.id.getDate());
		}
		if(id.getNode().getVal() != o.id.getNode().getVal())
			return Long.compare(id.getNode().getVal(),o.id.getNode().getVal());
		return Long.compare(id.getNumevent(),o.id.getNumevent());
	}
	
	public void accept(LogEventVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public final String toString() {
		StringBuilder sb = new StringBuilder();
		Class<?> cl = this.getClass();
		sb.append(id.toString()+" : "+cl.getSimpleName()+"(");
		
		
		List<String> tokens = new ArrayList<>();
		while(!cl.equals(LogEvent.class)) {
			List<Field> fields=Arrays.asList(cl.getDeclaredFields());
			Collections.reverse(fields);
			for(Field f : fields ) {
			    if((f.getModifiers() & Modifier.STATIC) != 0) continue;
			    f.setAccessible(true);
			    try {
					tokens.add(f.getName()+"="+f.get(this));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					throw new IllegalStateException(e);
				}
			}
			cl=cl.getSuperclass();
		}
		Collections.reverse(tokens);
		for(String s : tokens) {
			sb.append(" "+s+",");
		}		
		sb.append(")");
		  
		return sb.toString();
	}
	
}
