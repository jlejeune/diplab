package diplab.core.experiment.reporting.logevent;

import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;

public class LogEndInterruptionCallEvent extends LogInterruptionCallEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6384815089844007205L;

	private final boolean timeout;
	private final boolean checked;
	
	public LogEndInterruptionCallEvent(NodeSystem node, NodeProto nodeproto, String nameprimitive, Object[] args, long numcall,
			int num_inter, boolean timeout, boolean checked) {
		super(node, nodeproto, nameprimitive, args, numcall, num_inter);
		this.timeout=timeout;
		this.checked=checked;
	}
	
	public boolean isTimeout() {
		return timeout;
	}

	public boolean isChecked() {
		return checked;
	}

	@Override
	public void accept(LogEventVisitor visitor) {
		super.accept(visitor);
		visitor.visit(this);
	}
}
