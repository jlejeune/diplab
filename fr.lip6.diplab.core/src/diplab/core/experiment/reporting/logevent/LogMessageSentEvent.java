package diplab.core.experiment.reporting.logevent;

import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;
import diplab.core.messaging.Message;

public class LogMessageSentEvent extends LogMessageEvent {
	private static final long serialVersionUID = 817344673696555225L;
	
	public LogMessageSentEvent(NodeSystem node, NodeProto nodeproto, IDNode sender, long num_sent, Message<?> message) {
		super(node, nodeproto, sender, num_sent, message);
	}

	

	@Override
	public void accept(LogEventVisitor visitor) {
		super.accept(visitor);
		visitor.visit(this);
	}
	
}
