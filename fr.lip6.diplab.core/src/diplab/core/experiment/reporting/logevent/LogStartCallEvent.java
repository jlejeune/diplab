package diplab.core.experiment.reporting.logevent;

import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;

public class LogStartCallEvent extends LogCallEvent {

	private static final long serialVersionUID = -5739028285207186765L;

	public LogStartCallEvent(NodeSystem node, NodeProto nodeproto, String nameprimitive, Object[] args, long numcall) {
		super(node,nodeproto, nameprimitive, args, numcall);
	}

	@Override
	public void accept(LogEventVisitor visitor) {
		super.accept(visitor);
		visitor.visit(this);
	}
	
}
