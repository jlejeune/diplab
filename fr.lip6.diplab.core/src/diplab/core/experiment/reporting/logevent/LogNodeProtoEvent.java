package diplab.core.experiment.reporting.logevent;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import diplab.core.Loggable;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.visitor.LogEventVisitor;

public abstract class LogNodeProtoEvent extends LogEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1142954288117778957L;

	
	private final String nodeprotoname;
	private  Map<String,Object> nodeProtoState = null;
	
	public LogNodeProtoEvent(NodeSystem node, NodeProto nodeproto) {
		super(node);
		this.nodeprotoname=nodeproto.getNameProto();
		try {
			Class<? extends NodeProto> cl_nodeproto = nodeproto.getClass();
			while(!cl_nodeproto.equals(NodeProto.class)) {
				for(Field f : cl_nodeproto.getDeclaredFields()) {
					if(f.isAnnotationPresent(Loggable.class)) {
						if(nodeProtoState == null) nodeProtoState = new HashMap<>();
						f.setAccessible(true);
						Object attribute = f.get(nodeproto);
						if(((attribute instanceof Map) || (attribute instanceof Collection)) && attribute instanceof Cloneable ) {	
							Method clone =  attribute.getClass().getMethod("clone");
							clone.setAccessible(true);
							attribute = clone.invoke(attribute);
						}
						nodeProtoState.put(f.getName(), attribute);
					}
				}
				cl_nodeproto=cl_nodeproto.getSuperclass().asSubclass(NodeProto.class);
			}
			
		}catch(Exception e) {
			throw new IllegalStateException(e);
		}
	}
	
	public Object getStateOf(String name) {
		if(nodeProtoState == null) return null;
		return nodeProtoState.get(name);
	}
	
	public Map<String,Object> getState(){
		if(nodeProtoState == null) return null;
		return new HashMap<>(nodeProtoState);
	}
	
	public String getNodeProtoName() {
		return nodeprotoname;
	}
			
	public void accept(LogEventVisitor visitor) {
		super.accept(visitor);
		visitor.visit(this);
	}

}
