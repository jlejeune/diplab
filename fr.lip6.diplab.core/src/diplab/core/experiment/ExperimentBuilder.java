package diplab.core.experiment;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.experiment.reporting.ExperimentProgressIndicator;
import diplab.core.experiment.reporting.ReportFactory;
import diplab.core.experiment.reporting.ReportInMemory;
import diplab.core.experiment.reporting.ReportWatcher;
import diplab.core.experiment.reporting.visitor.selector.LogEventSelector;
import diplab.core.experiment.scenario.Scenario;
import diplab.core.experiment.scenario.action.Action;

public class ExperimentBuilder {

	private  Scenario scenario ;
	private  int seed ;
	private  Map<String,Class<? extends NodeProto>> nodeProtoClasses ;
	private  int systemsize ;
	private  Map<String,Object> properties;
	
	private Map<String,Set<IDNode>> deploymentmap = new HashMap<>();
	
	private  long experimentDuration;
	private  Class<? extends ExperimentController> experimentControllerClass;
	private File reportdirectory=null;
	private ReportFactory reportfactory=(e,name)->new ReportInMemory(e,name);
	private Collection<LogEventSelector> logEventSelectors = new HashSet<>();
	
	private Collection<ReportWatcher> reportWatchers = new HashSet<>();
	
	public ExperimentBuilder(Class<? extends ExperimentController> experimentControllerClass, long experimentDuration) {
		this.experimentControllerClass = experimentControllerClass;
		this.experimentDuration = experimentDuration;
		clear();
	}


	public void clear() {
		seed = 20;
		scenario=new Scenario();
		nodeProtoClasses = new HashMap<>();
		systemsize = 5;
		properties=new HashMap<>();
		reportWatchers.clear();
		progress=false;
	}


	public ExperimentBuilder deployOnly(String nodeProtoname, Set<IDNode> nodesToDeploy) {
		if(!nodeProtoClasses.containsKey(nodeProtoname)) {
			throw new IllegalArgumentException("NodeProto "+nodeProtoname+" is not declared");
		}
		for(IDNode id:nodesToDeploy) {
			if(id.getVal()<0 || id.getVal()>= systemsize) {
				throw new IllegalArgumentException("Node with id="+id+" is invalid");
			}
		}
		deploymentmap.put(nodeProtoname, new HashSet<>(nodesToDeploy));
		return this;
	}
	
	public ExperimentBuilder withScenario(Scenario s) {
		this.scenario=s;
		return this;
	}

	public ExperimentBuilder addScenarioAction(Action a) {
		scenario.addAction(a);
		return this;
	}
	
	public ExperimentBuilder withSeed(int seed) {
		this.seed=seed;
		return this;
	}

	public ExperimentBuilder withNodeProtos(Map<String,Class<? extends NodeProto>> nodeProtoClasses ) {
		this.nodeProtoClasses=nodeProtoClasses;
		return this;
	}
	
	public ExperimentBuilder withSystemSize(int nb) {
		this.systemsize=nb;
		return this;
	}
	
	public ExperimentBuilder addNodeProto(String name, Class<? extends NodeProto> cl) {
		nodeProtoClasses.put(name, cl);
		return this;
	}
	
	public ExperimentBuilder addProperties(Map<String,Object> p){
		properties.putAll(p);
		return this;
	}
	
	public ExperimentBuilder addProperty(String name,Object value) {
		properties.put(name, value);
		return this;
	}
	
	public ExperimentBuilder withReportDirectory(File reportdirectory) {
		this.reportdirectory=reportdirectory;
		return this;
	}
	
	public ExperimentBuilder withReportFactory(ReportFactory reportfactory) {
		this.reportfactory=reportfactory;
		return this;
	}
	
	public ExperimentBuilder withLogEventSelector(LogEventSelector selector) {
		this.logEventSelectors.add(selector);
		return this;
	}
	
	public ExperimentBuilder withReportWatcher(ReportWatcher watcher) {
		this.reportWatchers.add(watcher);
		return this;
	}
	
	private boolean progress=false;
	
	
	public ExperimentBuilder withProgressIndicator() {
		progress=true;
		return this;
	}
	
	public ExperimentBuilder withoutProgressIndicator() {
		progress=false;
		return this;
	}
	
	public Scenario getScenario() {
		return scenario;
	}
	
	public File getReportdirectory() {
		return reportdirectory;
	}

	public int getSeed() {
		return seed;
	}


	public Map<String, Class<? extends NodeProto>> getNodeProtoClasses() {
		return nodeProtoClasses;
	}


	public int getSystemsize() {
		return systemsize;
	}


	public Map<String, Object> getProperties() {
		return properties;
	}


	public long getExperimentDuration() {
		return experimentDuration;
	}


	public Class<? extends ExperimentController> getExperimentControllerClass() {
		return experimentControllerClass;
	}
	
	

	public Experiment build() {
		long timestamp = System.currentTimeMillis();
		File definitive_report_dir;
		if(reportdirectory != null) {
			definitive_report_dir=reportdirectory;
		}else {
			definitive_report_dir=new File(new File(System.getProperty("java.io.tmpdir")), "diplab_expe"+timestamp);
		}
		if(definitive_report_dir.exists()) {
			throw new IllegalStateException("The file "+definitive_report_dir.getAbsolutePath()+" already exists");
		}
		if(progress) {
			reportWatchers.add(new ExperimentProgressIndicator());
		}
		try {
			Files.createDirectories(definitive_report_dir.toPath());
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		
		return new Experiment(timestamp,scenario, 
				seed, 
				experimentControllerClass, 
				new HashMap<>(nodeProtoClasses), 
				systemsize, 
				experimentDuration,
				new HashMap<>(properties),
				new HashMap<>(deploymentmap),
				definitive_report_dir,
				reportfactory,
				new HashSet<>(logEventSelectors),
				new HashSet<>(reportWatchers));
	}
	
}
