package diplab.core.experiment.scenario;

import java.io.Serializable;
import java.util.Random;

/**
 * An interface for any value depending on a random object. 
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 * @param <T> the type of the value
 */
public interface Value<T> extends Serializable {

	
	/**
	 * 
	 * @param r the required random object to determine the value 
	 * @return the new computed value
	 */
	public T get(Random r);
}
