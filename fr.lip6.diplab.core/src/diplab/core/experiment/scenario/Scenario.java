package diplab.core.experiment.scenario;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import diplab.core.experiment.scenario.action.Action;

/**
 * The scenario of an experiment. A scenario is just a 
 * collection of {@link Action} to deliver on node's system.
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public class Scenario implements Iterable<Action>, Serializable{


	private static final long serialVersionUID = -7580421635443015429L;
	private final List<Action> actions = new ArrayList<>();
	

	/**
	 * Declares a new action in the scenario
	 * @param action the new action
	 */
	public void addAction(Action action) {
		actions.add(action);
	}

	@Override
	public Iterator<Action> iterator() {
		return actions.iterator();
	}

	public boolean isEmpty() {
		return actions.isEmpty();
	}
	
	
}
