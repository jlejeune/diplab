package diplab.core.experiment.scenario.action;

import diplab.core.IDNode;
import diplab.core.experiment.scenario.Value;

/**
 * An action descriptor for a node crash which can be permanent or temporary
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public class CrashAction extends Action {

	private static final long serialVersionUID = 3878234264206939066L;
	private final Value<Long> duration;
	
	/**
	 * A constructor for an action with permanent crash
	 * @param start The date of the crash's beginning
	 * @param targets The target nodes where the action will be delivered; If this arg is null
	 * then all node in the system are considered as target. 
	 */
	public CrashAction(Value<Long> start, IDNode... targets) {
		this(start, null, targets);
	}
	
	/**
	 * @param start The date of the crash's beginning
	 * @param duration the duration where target nodes are crashing. If null, crash will be permanent
	 * @param targets The target nodes where the action will be delivered; If null
	 * then all node in the system are considered as target. 
	 */
	public CrashAction(Value<Long> start,  Value<Long> duration, IDNode... targets) {
		super(start, targets);
		this.duration=duration;
	}

	
	/**
	 * 
	 * @return the duration of the crash. null if the crash is permanent
	 */
	public Value<Long> getDuration() {
		return duration;
	}
	
}
