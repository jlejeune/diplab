package diplab.core.experiment.scenario.action;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import diplab.core.IDNode;
import diplab.core.experiment.scenario.Value;
/**
 * An abstract class which represents an action to exexute
 * on a list of target nodes at a given date. 
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */

public abstract class Action implements Serializable {

	private static final long serialVersionUID = 4489269170106374871L;

	private final Value<Long> start;
	
	private final List<IDNode> targets;

	/**
	 * 
	 * @param start The date of the action's beginning
	 * @param targets The target nodes where the action will be delivered; If this arg is null
	 * then all node in the system are considered as target. 
	 */
	public Action(Value<Long> start, IDNode... targets) {
		super();
		this.start = start;
		this.targets = Arrays.asList(targets);
	}

	/**
	 * 
	 * @return Date of the the action's beginning
	 */
	public Value<Long> getStart() {
		return start;
	}

	/**
	 * 
	 * @param id the id of any node in the system
	 * @return true if the action targets the given id, false otherwise
	 */
	public boolean containsTarget(IDNode id) {
		return targets.isEmpty() || targets.contains(id);
	}
		
}
