package diplab.core.experiment.scenario.action;

import diplab.core.IDNode;
import diplab.core.experiment.scenario.Value;
import diplab.core.experiment.scenario.action.callscheme.CallScheme;

/**
 * An action descriptor for {@link CallScheme}. 
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */

public class CallAction extends Action {

	private static final long serialVersionUID = 3191242209380282167L;
	private final CallScheme callscheme;
	
	
	/**
	 * 
	 * @param start The date of the action's beginning
	 * @param callscheme The {@link CallScheme} to execute on target nodes
	 * @param targets The target nodes where the action will be delivered; If this arg is null
	 * then all node in the system are considered as target. 
	 */
	public CallAction(Value<Long> start, CallScheme callscheme, IDNode... targets) {
		super(start, targets);
		this.callscheme=callscheme;
		
	}

	/**
	 * 
	 * @return the {@link CallScheme} of the action
	 */
	public CallScheme getCallscheme() {
		return callscheme;
	}

	
	
	
	

}
