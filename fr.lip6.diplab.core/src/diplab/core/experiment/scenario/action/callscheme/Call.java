package diplab.core.experiment.scenario.action.callscheme;

import java.util.Iterator;

import diplab.core.NodeProto;
import diplab.core.Primitive;
import diplab.core.experiment.scenario.Value;

/**
 * A leaf {@link CallScheme} with a single call.
 * A call object is a descriptor of an invocation on a given {@link NodeProto} name
 * and a given primitive name annotated {@link Primitive}. Moreover a call 
 * specifies a sleep time to respect once the invocation is finished.
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */

public final class Call implements CallScheme {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8701489915705785724L;
	
	private final String nodeprotoname;

	private final String primitivename;
	
	private final Value<?>[] args;
	
	private final Value<Long> latency;
	
	/**
	 * 
	 * @param nodeprotoname the target {@link NodeProto} 
	 * @param primitivename the target primitive annotated {@link Primitive} in the target {@link NodeProto}
	 * @param latency the time to sleep after the invocation
	 * @param args the arguments of the invocation
	 */
	public Call(String nodeprotoname, String primitivename,  Value<Long> latency, Value<?>... args) {
		this.nodeprotoname=nodeprotoname;
		this.primitivename=primitivename;
		this.args=args;
		this.latency=latency;
	}
	
	/**
	 * 
	 * @return the target {@link NodeProto} of the call
	 */
	public String getNodeProtoName() {
		return nodeprotoname;
	}
	
	
	/**
	 * 
	 * @return the target primitive annotated {@link Primitive} in the target {@link NodeProto}
	 */
	public String getPrimitiveName() {
		return primitivename;
	}

	/**
	 * 
	 * @return the arguments of the invocation
	 */
	public Value<?>[] getArgs() {
		return args;
	}

	/**
	 * 
	 * @return the time to sleep after the invocation
	 */
	public Value<Long> getLatency() {
		return latency;
	}

	@Override
	public Iterator<Call> iterator() {
		return new Iterator<Call>() {
			
			boolean read=true;
			
			@Override
			public boolean hasNext() {
				return read;
			}

			@Override
			public Call next() {
				read=false;
				return Call.this;
			}
			
		};
	}
}
