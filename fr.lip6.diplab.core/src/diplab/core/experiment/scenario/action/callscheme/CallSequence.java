package diplab.core.experiment.scenario.action.callscheme;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


/**
 * A composite call scheme containing a sequence of {@link CallScheme}
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public class CallSequence implements CallScheme, Iterable<Call> {

	private static final long serialVersionUID = 196008928273002485L;
	private final List<CallScheme> children = new ArrayList<>();
	
	
	public CallSequence() {	}
	
	/**
	 * Adds a new {@link CallScheme} in the sequences
	 * @param c the new {@link CallScheme} to add
	 * @return the current instance after adding
	 */
	public CallSequence addCallScheme(CallScheme c) {
		children.add(c);
		return this;
	}

	/**
	 * Adds one or several {@link CallScheme} in the sequences
	 * @param c the new {@link CallScheme} set to add
	 * @return the current instance after adding
	 */
	public CallSequence addCallScheme(CallScheme... c) {
		children.addAll(Arrays.asList(c));
		return this;
	}
	
	@Override
	public Iterator<Call> iterator() {
		return new Iterator<Call>() {
			
			Iterator<CallScheme> it = children.iterator();
			Iterator<Call> cur = Collections.emptyIterator();
			
			@Override
			public boolean hasNext() {
				
				return it.hasNext() || cur.hasNext();
			}

			@Override
			public Call next() {
				if(!cur.hasNext()) {
					cur=it.next().iterator();
				}
				return cur.next();				
			}
			
		};
	}

	

}
