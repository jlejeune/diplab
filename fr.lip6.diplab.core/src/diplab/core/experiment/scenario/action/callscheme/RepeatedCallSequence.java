package diplab.core.experiment.scenario.action.callscheme;

import java.util.Collections;
import java.util.Iterator;

/**
 * A composite {@link CallScheme} containing a sequence of {@link CallScheme}
 * which can be repeated a given number of times.
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */

public class RepeatedCallSequence extends CallSequence {
	
	private static final long serialVersionUID = -6809371320896131665L;
	private final int nb;
	
	/**
	 * 
	 * @param nb the number of times to repeat the sequence
	 */
	public RepeatedCallSequence(int nb) {
		this.nb=nb;
	}
	
	/**
	 * A repeated call scheme sequence with  {@link Integer#MAX_VALUE} as value.
	 */
	public RepeatedCallSequence() {
		this(Integer.MAX_VALUE);
	}

	/**
	 * 
	 * @return the number of times to repeat the sequence
	 */
	public int getNb() {
		return nb;
	}
	
	@Override
	public Iterator<Call> iterator(){
		return new Iterator<Call>() {
			
			int i=0;
			Iterator<Call> cur = Collections.emptyIterator();
			
			@Override
			public boolean hasNext() {
				return i<nb || cur.hasNext();
			}

			@Override
			public Call next() {
				if(!cur.hasNext()) {
					cur=RepeatedCallSequence.super.iterator();
					i++;
				}				
				return cur.next();
			}
			
		};
	}
	
}
