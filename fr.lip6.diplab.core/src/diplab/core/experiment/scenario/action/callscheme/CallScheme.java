package diplab.core.experiment.scenario.action.callscheme;

import java.io.Serializable;


/**
 * An interface for any call scheme. A call scheme is a sequence of a {@link Call}.
 * It extends {@link Iterable} allowing to iterates and to invoke sequentially all {@link Call} 
 * in the scheme.
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public interface CallScheme extends Iterable<Call>,Serializable{

	
	
}
