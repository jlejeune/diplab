package diplab.core.experiment.scenario;

import java.util.Random;

public class ConstanteValue<T> implements Value<T> {
	
	private static final long serialVersionUID = 1L;
	private final T value;
	
	public ConstanteValue(T value) {
		this.value=value;
	}

	@Override
	public T get(Random r) {
		return value;
	}

}
