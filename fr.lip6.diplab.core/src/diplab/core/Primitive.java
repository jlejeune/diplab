package diplab.core;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.List;

import diplab.core.experiment.scenario.Value;



/**
 * Indicate if a method of a protocol is a primitive. A primitive
 * method is a method that can be invoked from a call event.
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
@Retention(RUNTIME)
@Target(METHOD)
public @interface Primitive {
	
	
	/**
	 * Indicate the type of a primitive method of a protocol
	 * If the type is equals to CLASSICAL, then the method is just 
	 * called by the action in the scenario.
	 * If the type is equals to STARTUP, then the method is called once
	 * at the starting of the node by the system.
	 * If the type is equals to LOOP, the method is called 
	 * at the starting of the node and recalled again indefinitely by the system 
	 * once it returns. It is possible to set a waiting period before two
	 * successive invocations by indicates in the system property
	 * {protoname}.{methodname}.{@link NodeSystem#PAR_LOOP_SLEEPING_TIME}}. The
	 * Default sleeping time is 1.	 * 
	 * For STARTUP and LOOP , it is possible to set up arguments via a {@link List} of {@link Value}
	 * by using the system property {protoname}.{methodname}.{@link NodeSystem#PAR_LOOP_ARGS}}
	 * 
	 */
	Type type() default Type.CLASSICAL;
	
	public static enum Type{CLASSICAL,STARTUP,LOOP}
}
