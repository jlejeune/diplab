package diplab.core;

import java.io.File;
import java.io.PrintStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.BooleanSupplier;
import java.util.stream.Collectors;

import diplab.core.experiment.reporting.logevent.LogUserEvent;
import diplab.core.messaging.Message;

/**
 * The class that any protocol class must extends. This class links the 
 * protocol with the underlying node system. An instance of a NodeProto
 * exists for each node. This class provides the main API that a node is able
 * to use in the system model.
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public abstract class NodeProto {


	private final NodeSystem system;

	private long cpt_mess_built=0;

	/**
	 * Build a new instance of node proto for a given NodeSystem
	 * @param system the underlying NodeSystem of this protocol
	 */
	public NodeProto(NodeSystem system) { 
		this.system=system;
	}

	/**
	 * Return the number of nodes in the whole system.
	 * @return the number of nodes in the whole system.
	 */
	public final int systemsize() {
		return system.systemsize();
	}

	/**
	 * Get the id of the current node
	 * @return the id of the current node.
	 */
	public final IDNode idnode() {
		return system.idnode();
	}


	/**
	 * Get a list of direct neighbors of the current node in the communication graph. By default
	 * the communication graph is complete implying that any node is able to 
	 * communicate with any existing IDNode in the system including itself.
	 * @return the list of neighbors 
	 */
	public final List<IDNode> allnodes() {
		return system.allnodes();
	}


	/**
	 * Returns the set of nodes having deployed this protocol
	 * @return the set of nodes having deployed this protocol
	 */
	public final Collection<IDNode> members(){
		return system.membersOf(getNameProto());
	}


	/**
	 * Returns the set of nodes having deployed this protocol excepted the current node
	 * @return the set of nodes having deployed this protocol excepted the current node
	 */
	public final Collection<IDNode> membersWithoutMe(){
		return members().stream().filter(id->!id.equals(idnode())).collect(Collectors.toList());
	}

	/**
	 * Returns the random generator of the current node
	 * @return the random generator object of the current node
	 */
	public final Random random() {
		return system.random();
	}


	/**
	 * Return the current clock time of the current node
	 * @return the current clock time of the current node
	 */
	public final long getCurrentTime() {
		return system.getCurrentTime();
	}

	/**
	 * Blocks the current event executor until the given condition is verified.
	 * If the condition is already verified, this method is not blocking
	 * @param c the condition to wait
	 */
	public final void wait(BooleanSupplier c) {
		system.wait(c);
	}

	/**
	 * Blocks the current event executor until the given condition is verified 
	 * or if the given delay is expired.
	 * @param c the condition to wait
	 * @param delay the maximum delay to wait
	 * @return true if the delay has expired, true otherwise
	 */
	public final boolean wait(BooleanSupplier c, Long delay) {
		return system.wait(c, getCurrentTime()+delay);
	}

	/**
	 * Blocks the current event executor during the given delay
	 * @param delay the delay to wait
	 */
	public final void  sleep(Long delay) {
		wait(()->false,delay);
	}


	/**
	 * Return the name of the current protocol.
	 * @return the name of the current protocol
	 */
	public final String getNameProto() {
		return system.getNameof(this);
	}

	/**
	 * Returns a custom parameter defined in the current experiment 
	 * @param name the name of the parameter to retrieve
	 * @return the value of the parameter, null if it does not exist
	 */
	public final Object getExperimentProperty(String name) {
		return system.getExperimentProperty(name);
	}

	/**
	 * Returns a custom mandatory parameter defined in the current experiment 
	 * @param name the name of the parameter to retrieve
	 * @return the value of the parameter
	 * @throws NotExistPropertyException if the parameter does not exist
	 */
	public final Object requireExperimentProperty(String name) {
		Object res = getExperimentProperty(name);
		if(res == null) {
			throw new NotExistPropertyException(name);
		}
		return res;
	}

	/**
	 * Returns a custom mandatory parameter defined in the current experiment
	 * as a variable of a given type T
	 * @param <T> the expected type of the object
	 * @param name the name of the parameter to retrieve
	 * @param type the class object associated with T
	 * @return the value of the parameter as a variable T
	 * @throws NotExistPropertyException if the parameter does not exist or if it can be casted to type T
	 */
	
	@SuppressWarnings("unchecked")
	public final <T> T requireExperimentProperty(String name, Class<T> type) {
		Object obj = requireExperimentProperty(name);
		try {
			return (T) obj;
		}catch(ClassCastException e) {
			throw new NotExistPrimitiveException(e);
		}
		
		
	}
 	
	
	/**
	 * Build a new message for a given destination and a given content. The src node is
	 * the current node. 
	 * @param <T> the type of the message content
	 * @param class_mess the class of the message
	 * @param dest the destination node
	 * @param content the message content
	 * @return the message build by this method
	 */
	public final <T extends Serializable> Message<T> buildMessage(Class<? extends Message<T>> class_mess, IDNode dest, T content){
		try {
			Message<T> mess = class_mess.getConstructor(class_mess.getConstructors()[0].getParameterTypes())
					.newInstance(
							idnode(),
							dest,
							getNameProto(),
							cpt_mess_built++,
							content);
			return mess;
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}

	/**
	 * Build a new message and send it to a node. The destination node is assumed to be 
	 * presents in the neighbors list of the current node. 
	 * @param <T>  the type of the message content
	 * @param class_mess the class of the message
	 * @param dest the destination node
	 * @param content the message content
	 * @throws IllegalArgumentException if the destination node does not exist in the system. 
	 */
	public final <T extends Serializable> void send(Class<? extends Message<T>> class_mess, IDNode dest, T content) {
		if(!members().contains(dest))
			throw new IllegalArgumentException(idnode()+"tries to send a message to an unreachable node ("+dest+")");
		system.send(buildMessage(class_mess, dest, content));		
	}

	/**
	 * Send a message to set of destination nodes. 
	 * @param <T> the type of the message content
	 * @param class_mess he class of the message
	 * @param dests the destination nodes of the message
	 * @param content the message content
	 * @throws IllegalArgumentException if it exists a destination node which does not exist in the system
	 */
	public final <T extends Serializable> void sendBroadcast(Class<? extends Message<T>> class_mess, Iterable<IDNode> dests, T content) {
		for(IDNode dest : dests) {
			send(class_mess, dest, content);
		}
	}

	/**
	 * Call synchronously a annoted primitive method  in another protocol instance of the current node. This use
	 * the reflective java API. 
	 * @param nameproto the name of the target protocol 
	 * @param namemethod the name of the java method in the protocol
	 * @param args the arguments of the method
	 * @return the result object of the call 
	 * @see java.lang.reflect.Method#invoke
	 */
	public final Object call(String nameproto, String namemethod, Object... args) {
		return system.call(nameproto, namemethod, args);
	}

	/**
	 * Call asynchronously a annoted primitive method  in another protocol instance of the current node. This use
	 * the reflective java API. This method allow a non blocking call to a primitive and then create
	 * a new EventExecutor for this call. 
	 * @param nameproto the name of the target protocol 
	 * @param namemethod the name of the java method in the protocol
	 * @param args the arguments of the method
	 * @see java.lang.reflect.Method#invoke
	 */
	public final void callAsync(String nameproto, String namemethod, Object... args) {
		system.callAsync(nameproto, namemethod, args);
	}


	/**
	 * Call the associated message handler in another protocol layer of the current node.  
	 * @param nameproto the protocol where the message must be delivered
	 * @param message the message to deliver
	 */
	public final void deliverMessage(String nameproto, Message<?> message) {
		system.deliverMessage(nameproto, message);
	}	

	/**
	 * The procedure to execute on the current protocol when all protocol on the
	 * current node has been instanciated and deployed on the current node. This method
	 * is called before any event handler execution at the starting of the current node. 
	 * By default this method does nothing and must be overridden to define the wished behavior.
	 */
	public void init() {}


	/**
	 * Add a message in the local report as a LogNodeProtoEvent
	 * @param message
	 */
	public void log(String message) {
		system.getLocalReport().addCarefully(new LogUserEvent(system, this, message));
	}

	/**
	 * Print a message in a new line to the standard output (without adding a new log in the local report).
	 * The message is prefixed by the timestamp and the node id of the source.
	 * @param message the message to print
	 */
	public void println(String message) {
		System.out.println("[date="+getCurrentTime()+" node="+idnode()+"] : "+message);
	}
	
	/**
	 * Defined has final to avoid overriding. The behavior is the same of {@link java.lang.Object#hashCode()}
	 * @return the default hash code value such as defined in {@link java.lang.Object}
	 */
	@Override
	public final int hashCode() {
		return super.hashCode();
	}

	/**
	 * Defined has final to avoid overriding. The behavior is the same of {@link java.lang.Object#hashCode()}
	 * @return the default hash code value such as defined in {@link java.lang.Object}
	 */
	@Override
	public final boolean equals(Object o) {
		return super.equals(o);
	}


	/**
	 * Ensure that a given primitive in a given instance of protocol exists
	 * @param nameProto the protocol name
	 * @param namePrimitive the primitive name
	 * @param parametertypes the expected parameter types
	 * @throws NotExistPrimitiveException if the property is not verified
	 */
	public final void ensurePrimitivePresent(String nameProto, String namePrimitive, Class<?>... parametertypes) {
		system.ensurePrimitivePresent(nameProto, namePrimitive, parametertypes);
	}

	@Override
	public String toString() {
		return idnode() + " proto = "+getNameProto();
	}


	/**
	 * Return a proxy object of a local protocol designed by a name according to a given interface. 
	 * The returned proxy object implements itf and each method is a delegation of the {@link NodeProto#call(String, String, Object...)}
	 * The programmer can use directly the java interface itf to call a tagged {@link Primitive} method 
	 * of another {@link NodeProto}. It is assumed that each method of itf is tagged  {@link Primitive}
	 * in the target {@link NodeProto}.
	 * @param <I> the static type of the target interface
	 * @param itf the {@link Class} object of the target interface
	 * @param nameproto the name of the target protocol
	 * @return a proxy object implementing itf and calling the local protocol nameproto
	 */

	private final Map<String,Object> proxies = new HashMap<>();
	private static final Map<String,Class<?>> proxyclasses = new HashMap<>();


	@SuppressWarnings("unchecked")
	public <I> I getProxyOfProto(Class<I> itf, String nameproto) {
		try {
			if(! proxies.containsKey(nameproto)) {
				String canonicalnameproxyclass= "fr.lip6.diplab.proxies.Proxy"+nameproto;
				if(!proxyclasses.containsKey(canonicalnameproxyclass)) {
					if(!itf.isInterface()) throw new IllegalArgumentException(itf.getCanonicalName()+" must be an interface");
					File root = Files.createTempDirectory("proxy"+nameproto).toFile();
					File dir_fjava= new File(root, "fr/lip6/diplab/proxies");

					dir_fjava.mkdirs();
					File fjava = new File(dir_fjava, "Proxy"+nameproto+".java");

					try(PrintStream pos = new PrintStream(fjava)) {
						pos.println("package fr.lip6.diplab.proxies;");
						pos.println("public class Proxy"+nameproto+" implements "+itf.getCanonicalName()+"{");

						pos.println("private final "+NodeProto.class.getCanonicalName()+" nodeproto;");
						pos.println(" public Proxy"+nameproto+"("+NodeProto.class.getCanonicalName()+" nodeproto){");
						pos.println("   this.nodeproto=nodeproto;");
						pos.println(" }");


						for(Method m: itf.getMethods()) {
							pos.print(" public "+m.getReturnType().getCanonicalName()+" "+m.getName()+"(");
							boolean first=true;
							for(Parameter p: m.getParameters()) {
								if(!first)pos.print(",");
								pos.print(p.getType().getCanonicalName()  +" "+p.getName());
								first=false;
							}
							pos.println("){");
							if(!m.getReturnType().equals(void.class)) {
								pos.print(" return ("+m.getReturnType().getCanonicalName()+")");
							}
							pos.print("  nodeproto.call(\""+nameproto+"\",\""+m.getName()+"\"");
							for(Parameter p: m.getParameters()) {
								pos.print(",");					
								pos.print(p.getName());
							}
							pos.println(");");

							pos.println("}");
						}
						pos.println("}");
					}
					///compilation java
					ProcessBuilder pb = new ProcessBuilder("javac","-cp",System.getProperty("java.class.path"),fjava.getAbsolutePath());
					pb.inheritIO();
					Process proc = pb.start();
					int ret = proc.waitFor();
					if(ret!=0) throw new IllegalStateException("Impossible de compiler "+fjava.getAbsolutePath());
					URL[] urls = new URL[]{root.toURI().toURL()};
					try(URLClassLoader cl_loader = new URLClassLoader(urls)) {
						Class<? extends I> cl =  cl_loader.loadClass("fr.lip6.diplab.proxies.Proxy"+nameproto).asSubclass(itf);
						proxyclasses.put(canonicalnameproxyclass, cl);
					} 

				}
				Class<? extends I> cl = proxyclasses.get(canonicalnameproxyclass).asSubclass(itf);
				I res = cl.getConstructor(NodeProto.class).newInstance(this);
				proxies.put(nameproto, res);
			}
		}catch(Exception e) {
			throw new IllegalStateException(e);
		}
		return (I) proxies.get(nameproto);		
	}
}
