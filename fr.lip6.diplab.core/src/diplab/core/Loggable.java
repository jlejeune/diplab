package diplab.core;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Indicates that the value of a protocol attribute must be logged
 * in the event reporting. Any attribute which is not tagged by this annotation
 * will be ignored in the reporting.
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */

@Retention(RUNTIME)
@Target(FIELD)
public @interface Loggable {

}
