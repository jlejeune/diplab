package diplab.core;

public class NotExistPrimitiveException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NotExistPrimitiveException() {
		super();
	}

	public NotExistPrimitiveException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotExistPrimitiveException(String message) {
		super(message);
	}

	public NotExistPrimitiveException(Throwable cause) {
		super(cause);
	}
	
}
