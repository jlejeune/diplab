package diplab.core.event;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Optional;

import diplab.core.NodeProto;
import diplab.core.Primitive;

/**
 * An object representing a call primitive event. The handler is initialized 
 * by looking up a method annotated {@link Primitive} in the associated NodeProto
 * instance with a given name and arguments list.
 *   
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public class PrimitiveCallEvent extends CallEvent {

	
	
	private final Method handler;
	private final Object[] args;
	
	public PrimitiveCallEvent(NodeProto nodeproto, String nameprimitive, Object[] args) {
		super(nodeproto);
		this.args=args;
		Optional<Method> option=Arrays.stream(getNodeProto().getClass().getMethods())
				  .filter(method->method.isAnnotationPresent(Primitive.class))
				  .filter(method->method.getName().equals(nameprimitive))				  
				  .filter(method-> method.getParameterCount() == args.length)
				  .filter(method->{
					  Parameter[] params = method.getParameters();
					  for(int i =0 ; i < params.length ; i++) {	  
						  if(!params[i].getType().isAssignableFrom(args[i].getClass())) {
							 return false;
						  }
					  }
					  return true;
				  })
				  .findFirst();
		if(!option.isPresent()){
			final StringBuilder args_type= new StringBuilder("(");
			Arrays.stream(args).forEach(o-> args_type.append(o.getClass().getName()+","));
			args_type.replace(args_type.length()-1, args_type.length(), "");
			args_type.append(")");
			throw new IllegalArgumentException("Method "+nameprimitive+args_type+" not found in "+nodeproto.getClass());
		}
		handler=option.get();
	}

	/**
	 * Returns the correct primitive handler in the NodeProto instance
	 * @return the correct primitive handler in the NodeProto instance
	 */
	@Override
	public Method getHandler() {
		return handler;
	}

	
	/**
	 * Returns a java tabular containing arguments of handler invocation
	 * @return a java tabular containing arguments of handler invocation
	 */
	@Override
	public Object[] getArgs() {
		return args;
	}
}
