package diplab.core.event;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.experiment.reporting.logevent.LogMessageReceiveEvent;
import diplab.core.messaging.Message;
import diplab.core.messaging.MessageHandler;

/**
 * An object representing a message reception event. The handler is initialized 
 * by looking up a method annotated {@link MessageHandler} with the good message type
 * in the associated NodeProto instance. Argument list contains only the message to deliver.
 * The message reception is logged into the {@link NodeSystem} report. 
 * 
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public  class  MessageReceptionEvent extends CallEvent {

	private final Message<?> message;
	private final Method handler;
	private final Object[] args;
	
	public MessageReceptionEvent(Message<?> m,  NodeSystem nodesystem, NodeProto nodeproto) {
		super(nodeproto);
		this.message=m;
		
		Optional<Method> option=Arrays.stream(getNodeProto().getClass().getMethods())
				  .filter(method->method.isAnnotationPresent(MessageHandler.class))
				  .filter(method->method.getAnnotation(MessageHandler.class).value().equals(message.getClass()))
				  .findFirst();
		args=new Object[] {message};
		
		if(!option.isPresent())
			throw new IllegalArgumentException("Handler for "+m.getClass()+" not found in "+nodeproto.getClass());
		handler= option.get();		
		nodesystem.getLocalReport().addCarefully(
				new LogMessageReceiveEvent(nodesystem,
											nodeproto,
											message.getSrc(),
											message.getId(), 
											message));
		
		
	}

	/**
	 * Returns the correct message handler in the NodeProto instance
	 * @return the correct message handler in the NodeProto instance
	 */
	@Override
	public Method getHandler() {
		return handler;
	}

	/**
	 * Returns a java tabular with a unique element which is the message to deliver
	 * @return a java tabular with a unique element which is the message to deliver
	 */
	@Override
	public Object[] getArgs() {
		return args;
	}
}
