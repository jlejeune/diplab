package diplab.core.event;

/**
 * 
 * The root type of any event that can be delivered on any node. 
 * 
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public interface Event {

}
