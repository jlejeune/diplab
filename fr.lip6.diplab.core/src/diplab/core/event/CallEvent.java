package diplab.core.event;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import diplab.core.NodeProto;

/**
 * A event object delivered on a target NodeProto. An event is characterized
 * by an id and the associated NodeProto instance. 
 * The event delivery processing consists to invoke a handling method with
 * the associated arguments. These features need to be definde in the 
 * concrete implementation of this class.  
 *  
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public abstract class CallEvent implements Event {
	private static Map<NodeProto,Long> cpt_event=new HashMap<>();


	private final NodeProto nodeproto;
	private final long id;
	
	private int nb_inter=0;

	public CallEvent(NodeProto nodeproto) {
		synchronized(cpt_event) {
			id=cpt_event.getOrDefault(nodeproto, 0L);
			cpt_event.put(nodeproto, id+1);
		}
		this.nodeproto=nodeproto;

	}

	/**
	 * Returns the target NodeProto of this event
	 * @return the target NodeProto of this event
	 */
	public NodeProto getNodeProto() {
		return nodeproto;
	}

	/**
	 * Declare a new interruption due to blocking with the {@link NodeProto#wait()}
	 * invocation by the corresponding Event Executor.
	 * . This returns a new interruption id. 
	 * @return a new interruption id. 
	 */
	public int newinterruption(){
		return nb_inter++;
	}

	/**
	 * Returns the target handler for the associated NodeProto
	 * @return the target handler for the associated NodeProto
	 */
	public abstract Method getHandler();

	/**
	 * Returns the arguments list of the handler invocation
	 * @return the arguments list of the handler invocation
	 */
	public abstract Object[] getArgs();

	
	/**
	 * Returns the id of this event
	 * @return the id of this event
	 */
	public long getId() {
		return id;
	}

	



}
