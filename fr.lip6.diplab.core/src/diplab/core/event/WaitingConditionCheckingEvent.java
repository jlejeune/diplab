package diplab.core.event;


/**
 * A singleton class representing a event which indicate to check if 
 * the blocked event executors can be unblocked. This kind of event 
 * is usually delivered when a deadline of a waiting condition  is reached. 
 * 
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */

public class WaitingConditionCheckingEvent implements Event{
	
	private static WaitingConditionCheckingEvent instance = null;
		
	public static WaitingConditionCheckingEvent get() {
		if(instance == null) {
			instance = new WaitingConditionCheckingEvent();
		}
		return instance;
	}

	
	
}