package diplab.core.messaging;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * An method annotation to mark a method in a protocol class
 * as a message handler. The value is equals to the class of the message
 * associated with the method. The method must have a unique parameter (a message)
 * where the type corresponds with the annotation value.
 *  
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */

@Retention(RUNTIME)
@Target(METHOD)
public @interface MessageHandler {

	Class<? extends Message<?>> value();
}
