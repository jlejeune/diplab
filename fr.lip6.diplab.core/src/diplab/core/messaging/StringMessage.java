package diplab.core.messaging;

import diplab.core.IDNode;

/**
 * A message with a single String content
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public class StringMessage extends Message<String> {

	
	public StringMessage(IDNode src, IDNode dest, String nameproto, long id, String content) {
		super(src, dest, nameproto, id, content);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3175157856266441890L;

}
