package diplab.core.messaging;

import diplab.core.IDNode;

/**
 * A message with a single integer content. 
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public class IntMessage extends Message<Integer> {

	

	public IntMessage(IDNode src, IDNode dest, String nameproto, long id, Integer content) {
		super(src, dest, nameproto, id, content);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8936298878555875341L;

}
