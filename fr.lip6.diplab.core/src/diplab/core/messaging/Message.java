package diplab.core.messaging;

import java.io.Serializable;

import diplab.core.IDNode;

/**
 * An abstract object representing any message being sent in the system. 
 * It contains a header storing the id of the source node (the message's sender), 
 * the id of the target node (the message's receiver), the protocol name where the 
 * message has to be delivered and a long id. A message is identified in the system 
 * and in the time by the triplet (source node, protocol name, id). 
 * The message content is an object with a parameterizable type. 
 * 
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 * @param <T> the type of the content object. Must be {@link Serializable}. 
 */
public abstract class Message<T extends Serializable> implements Serializable  {


	private static final long serialVersionUID = 1L;
	private final IDNode src;
	private final IDNode dest;
	private final String nameproto;
	private final long id;
	private final T content;
		
	
	public Message(IDNode src, IDNode dest, String nameproto, long id, T content) {
		this.src=src;
		this.dest=dest;
		this.nameproto=nameproto;
		this.id=id;
		this.content=content;
	}
	
	/**
	 * Returns the id of the source node
	 * @return the id of the source node
	 */
	public IDNode getSrc() {
		
		return src;
	}

	/**
	 * Returns the id of the destination node
	 * @return the id of the destination node
	 */
	public IDNode getDest() {
		return dest;
	}

	/**
	 * Returns the name of the protocol
	 * @return the name of the protocol
	 */
	public String getNameProto() {
		return nameproto;
	}

	/**
	 * Returns the id of the message for the given source node and protocol
	 * @return the id of the message for the given source node and protocol
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * Returns the content object of this message
	 * @return the content object of this message
	 */
	public T getContent() {
		return content;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName()+"(src="+src+" dest="+dest+", proto="+nameproto+", id="+id+", content="+content+")";
	}
	
	/**
	 * Returns a hash code value for the object from the triplet
	 * (source node, protocol name, id)
	 * @return a hash code value
	 */
	@Override
	public  int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((nameproto == null) ? 0 : nameproto.hashCode());
		result = prime * result + ((src == null) ? 0 : src.hashCode());
		return result;
	}

	/**
	 * Returns true if two messages are the same triplet (source node, protocol name, id),
	 * false otherwise
	 * @return true if two messages are the same triplet (source node, protocol name, id),
	 * false otherwise
	 */
	@Override
	public final boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message<?> other = (Message<?>) obj;
		if (id != other.id)
			return false;
		if (nameproto == null) {
			if (other.nameproto != null)
				return false;
		} else if (!nameproto.equals(other.nameproto))
			return false;
		if (src == null) {
			if (other.src != null)
				return false;
		} else if (!src.equals(other.src))
			return false;
		return true;
	}

	
	
	
}
