package diplab.core;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BooleanSupplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import diplab.core.event.CallEvent;
import diplab.core.event.Event;
import diplab.core.event.MessageReceptionEvent;
import diplab.core.event.PrimitiveCallEvent;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.logevent.LogEndCallEvent;
import diplab.core.experiment.reporting.logevent.LogEndInterruptionCallEvent;
import diplab.core.experiment.reporting.logevent.LogMessageSentEvent;
import diplab.core.experiment.reporting.logevent.LogStartCallEvent;
import diplab.core.experiment.reporting.logevent.LogStartInterruptionCallEvent;
import diplab.core.experiment.scenario.ConstanteValue;
import diplab.core.experiment.scenario.Value;
import diplab.core.experiment.scenario.action.Action;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.callscheme.Call;
import diplab.core.experiment.scenario.action.callscheme.CallScheme;
import diplab.core.experiment.scenario.action.callscheme.RepeatedCallSequence;
import diplab.core.messaging.Message;

/**
 * 
 * Main class centralized the main features of a node. It exists exactly
 * one NodeSystem per node. It is the bridge class between
 * instances of protocols and the network and the action processor.
 * A node system must be extended by a real system to implements real send procedure,
 * event processor and action processor. An event delivered by the network processor 
 * is a {@link MessageReceptionEvent} and an event delivered by the action processor 
 * is a {@link PrimitiveCallEvent}. In the both case each event provided by these processor 
 * is called a root event. When a root event is created, the node system create
 * and start a new thread called "event executor". We ensure that at most one thread is running
 * on a node.
 * 
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */

public abstract class NodeSystem {

	public static final String PAR_LOOP_SLEEPING_TIME="sleepingtime";
	public static final String PAR_LOOP_ARGS="args";

	private final Collection<EventExecutor> existing_event_executors = new CopyOnWriteArrayList<>();
	private EventExecutor current_running= null;
	private AtomicLong cpt_event=new AtomicLong(0);
	private Report localreport;
	private final Semaphore master_sem=new Semaphore(0);
	
	private Map<String,NodeProto> name_nodeprotos = null;
	private Map<NodeProto,String> nodeprotos_name = null;
	
	private Exception pending_exception=null;
	
	private Map<String,Collection<IDNode>> nodeprotomembers = new HashMap<>();
	
	
	
	/**
	 * Ensure that a given primitive in a given instance of protocol exists
	 * @param nameProto the protocol name
	 * @param namePrimitive the primitive name
	 * @param parametertypes the expected parameter types
	 * @throws NotExistPrimitiveException if the property is not verified
	 */
	public void ensurePrimitivePresent(String nameProto, String namePrimitive, Class<?>... parametertypes) {
		try {
			Method m =getNodeProto(nameProto).getClass().getMethod(namePrimitive, parametertypes);
			if(!m.isAnnotationPresent(Primitive.class)) {
				throw new IllegalArgumentException();
			}
		}catch(Exception e) {
			throw new NotExistPrimitiveException(e);
		}
		
	}
	
	/**
	 * Returns a custom parameter defined in the current experiment 
	 * @param name the name of the parameter to retrieve
	 * @return the value of the parametern, null if it does not exist
	 */
	public Object getExperimentProperty(String name) {
		return this.localreport.getExperiment().getProperties().get(name);
	}
	
	
	/**
	 * Returns a new event id for the current node
	 * @return a new event id for the current node
	 */
	public final long newEventNum() {
		return cpt_event.incrementAndGet();
	}
	
	
	/**
	 * Returns the report of the current node
	 * @return the report of the current node
	 */
	public Report getLocalReport() {
		return localreport;
	}
	
	
	/**
	 * Returns the collecion of nodes which have deployed the
	 * given protocol. 
	 * @param nameproto a protocol name 
	 * @return the nodes having deployed the given protocol. 
	 */
	public final Collection<IDNode> membersOf(String nameproto){
		return nodeprotomembers.get(nameproto);
	}
	
	/**
	 * Builds all protocol layers on the current node according to a deployment map.
	 *  This method must be invoked once by the concrete system during the deployment.
	 *  
	 * @param name_classesnodeproto the map associating the protocol name with its correspond implementation  class
	 * @param deploymentmap the map indicating if a protocol should be build on a node or not
	 */
	@SuppressWarnings("unchecked")
	protected void build(Experiment exp) {
		Map<String,Class<? extends NodeProto>> name_classesnodeproto = exp.getNodeProtoClasses(); 
		Map<String,Set<IDNode>> deploymentmap = exp.getDeploymentmap();
		localreport = exp.getReportfactory().createNewReport(exp, "expe"+exp.getTimestamp()+"_report_"+idnode().getVal());
		name_nodeprotos=new HashMap<>();
		nodeprotos_name=new HashMap<>();
		for(Entry<String,Class<? extends NodeProto>> kv : name_classesnodeproto.entrySet()) {
			String protoname = kv.getKey();
			Class<? extends NodeProto> protoclass = kv.getValue();
			if(!deploymentmap.containsKey(protoname)) {
				//this protocol should be deployed on any node
				nodeprotomembers.put(protoname, Collections.unmodifiableCollection(allnodes()));
			}else if(deploymentmap.get(protoname).contains(idnode())) {
				//this protocol should be deployed on this node
				nodeprotomembers.put(protoname, Collections.unmodifiableCollection(deploymentmap.get(protoname)));
			}else {
				//this protocol should not be deployed on this node 
				continue;
			}
			try {
				NodeProto nodeproto = protoclass.getConstructor(NodeSystem.class).newInstance(this);
				name_nodeprotos.put(protoname,nodeproto);
				nodeprotos_name.put(nodeproto, protoname);
				
				//schedule startup and loop primitives
				for(Method m : protoclass.getMethods()) {
					if(m.isAnnotationPresent(Primitive.class)) {
						Primitive a = m.getAnnotation(Primitive.class);
						
						switch(a.type()) {
						case CLASSICAL:
							break;
						case STARTUP:
						case LOOP:
							List<Value<?>> params = Collections.emptyList();
							if(m.getParameterCount() > 0) {
								 params = (List<Value<?>>) getExperimentProperty( protoname+"."+m.getName()+"."+PAR_LOOP_ARGS);
							}
							Value<Long> latency = (r)->1L;
							Object latencyproperty = getExperimentProperty( protoname+"."+m.getName()+"."+PAR_LOOP_SLEEPING_TIME);
							if(latencyproperty != null) {
								latency=(Value<Long>)latencyproperty;
							}
							CallScheme scheme= new Call(protoname, m.getName(),latency,params.toArray(new Value<?>[0]));
							if(a.type().equals(Primitive.Type.LOOP)) {
								scheme=new RepeatedCallSequence().addCallScheme(scheme);
							}							
							scheduleAction(new CallAction((r)->1L, scheme, idnode()),false);
							break;
						}
					}
				}
				
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}	
	}
	
	/**
	 * Sends a given message and logs it in the report. This method is only accessible from
	 * {@link diplab.core.NodeProto} class when a message sending is needed.
	 * @param message
	 */
	final void send(Message<?> message) {
		localreport.addCarefully(
				new LogMessageSentEvent(this,
						current_running.curevent().getNodeProto(), 
						message.getSrc(), 
						message.getId(), 
						message));
		sendReal(message);
	}
	
	/**
	 * Calls a primitive on a protocol instance of the current node in the context of the 
	 * current event executor. The begin and the end of the invocation are logged in the report.
	 * @param nameproto the name of the target protocol 
	 * @param nameprimitive the name of the java method in the protocol
	 * @param args the arguments of the method
	 * @return the result object of the call 
	 * @see java.lang.reflect.Method#invoke
	 */
	public final Object call(String nameproto, String nameprimitive, Object... args) {
		try {	
			return current_running.newcall(new PrimitiveCallEvent(getNodeProto(nameproto), nameprimitive, args));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
		return null;
	}
	
	/**
	 * Call asynchronously a annoted primitive method  in another protocol instance of the current node. This use
	 * the reflective java API. This method allow a non blocking call to a primitive and then create
	 * a new EventExecutor for this call. 
	 * @param nameproto the name of the target protocol 
	 * @param namemethod the name of the java method in the protocol
	 * @param args the arguments of the method
	 * @see java.lang.reflect.Method#invoke
	 */
	public void callAsync(String nameproto, String namemethod, Object[] args) {
		
		List<Value<?>> args_as_value = Stream.of(args).map(
				o -> new ConstanteValue<Object>(o)).collect(Collectors.toList());
		
		scheduleAction(
						new CallAction(
								r->getCurrentTime()
								,new Call(nameproto, namemethod, r->0L, args_as_value.toArray(new Value<?>[0]))
								, idnode()
						),
					true);
	}
	
	
	/**
	 * Calls a message handler a protocol instance of the current node in the context of the 
	 * current event executor. The message reception is logged in the report and then
	 * it behaves exactly as a classical call except that the target method is 
	 * a message handler.
	 * @param nameproto the name of the target protocol 
	 * @param message the message to deliver
	 */
	public final void deliverMessage(String nameproto, Message<?> message) {
		try {
			current_running.newcall(new MessageReceptionEvent(message, this, getNodeProto(nameproto)));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * Blocks the current event executor until the given condition is verified.
	 * If the condition is already verified, this method is not blocking. 
	 * In case of blocking, the start and the end of the event executor interruption
	 * are logged in the report. The event executor releases the node execution to wake up
	 * the master thread.
	 * @param c the condition to wait
	 */
	public final void wait(BooleanSupplier c) {
		wait(c,null);
	}
	
	/**
	 * Blocks the current event executor until the given condition is verified or if 
	 * the current time of the system reach a limit date. If the condition is already 
	 * verified, this method is not blocking.  If the date is obsolete, the
	 * method is not blocking and returns true.
	 * @param c the condition to wait
	 * @param deadline the limit date to wait
	 * @return true if the deadline has been reached, false otherwise
	 */
	public final boolean wait(BooleanSupplier c, Long deadline) {
		if(c.getAsBoolean()) return false;
		if(deadline != null && getCurrentTime() >= deadline) return true;
		
		//we are now sure that we are going to really wait
		EventExecutor executor = current_running;
		if(deadline != null) {
			executor.waitingcondition=new WaitingCondition(c, deadline);
			scheduleWaitingConditionChecking(deadline);
		}else {
			executor.waitingcondition=new WaitingCondition(c);	
		}
				
		CallEvent event = executor.curevent();
		int num_inter= event.newinterruption();
		
		localreport.addCarefully(
				new LogStartInterruptionCallEvent(
						this,event.getNodeProto(),
						event.getHandler().getName(), 
						event.getArgs(), event.getId(), num_inter
						)
				);
		
		
		
		current_running=null;
		master_sem.release();
		try {
			executor.sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		current_running=executor;
		boolean istimeout=executor.waitingcondition.isTimeout();
		localreport.addCarefully(
						new LogEndInterruptionCallEvent(
								this,event.getNodeProto(),
								event.getHandler().getName(), 
								event.getArgs(),
								event.getId(),
								num_inter,
								istimeout,
								executor.waitingcondition.checked()
						)
					);
		return istimeout;
	}
	
	
	/**
	 * Returns the number of nodes in the whole system.
	 * @return the number of nodes in the whole system.
	 */
	public abstract int systemsize();
	
	/**
	 * Gets the id of the current node
	 * @return the id of the current node.
	 */
	public abstract IDNode idnode();	
	
	/**
	 * Sends really in the network a message.
	 * @param message the message to send 
	 */
	protected abstract void sendReal(Message<?> message);
	
	/**
	 * Gets a list of direct neighbors of the current node in the communication graph. By default
	 * the communication graph is complete implying that any node is able to 
	 * communicate with any existing IDNode in the system including itself.
	 * @return the list of neighbors 
	 */
	public abstract List<IDNode> allnodes();

	/**
	 * Returns the random generator of the current node
	 * @return the random generator object of the current node
	 */
	public abstract Random random();
	
	/**
	 * Returns the current clock time of the current node
	 * @return the current clock time of the current node
	 */
	public abstract long getCurrentTime();
	
	/**
	 * Returns the  NodeProto instance of a given protocol name
	 * @param name the name of the protocol
	 * @return the associated NodeProto instance
	 * @throws IllegalArgumentException if the name refers to an unknown protocol.
	 */
	public final NodeProto getNodeProto(String name) {
		if(! name_nodeprotos.containsKey(name))
			throw new IllegalArgumentException("protocol "+name+"does not exist");
		return name_nodeprotos.get(name);
	}

	/**
	 * Returns the name of a given instance of protocol
	 * @param proto a protocol instance
	 * @return the name of the given protocol
	 */
	final String getNameof(NodeProto proto) {
		return nodeprotos_name.get(proto);
	}

	/**
	 * Returns the collection of protocol instance on the current node 
	 * @return the collection of protocol instance on the current node
	 */
	public final Collection<NodeProto> getNodeProtos(){
		return name_nodeprotos.values();
	}
	
	/**
	 * Call once a new root event is created, i.e. a new event processor is started. 
	 * This does nothing by default. It should be implemented in the concrete node system 
	 * @param rootevent the root event beginning its performing. 
	 */
	protected void beginRootEvent(CallEvent rootevent) {}
	
	/**
	 * Call once a new root event is finished. 
	 * This does nothing by default. It should be implemented in the concrete node system 
	 * @param rootevent the root event ending its performing. 
	 */
	protected void endRootEvent(CallEvent rootevent) {}
	
	private class WaitingCondition{
		private final BooleanSupplier cond;
		private final Long deadline;
		
		public WaitingCondition(BooleanSupplier cond, Long deadline) {
			this.cond=cond;
			this.deadline=deadline;
		}
		
		public WaitingCondition(BooleanSupplier cond) {
			this(cond,Long.MAX_VALUE);
		}
		
				
		public boolean checked() {
			return cond.getAsBoolean() || isTimeout();
		}
		
		public boolean isTimeout() {
			return getCurrentTime()>=deadline;
		}
	}
	
	private class EventExecutor implements Runnable {
				
		private final Stack<CallEvent> events = new Stack<>();
		private final CallEvent rootevent;
		private final Semaphore sem = new Semaphore(0); 
		private WaitingCondition waitingcondition = null;
		public EventExecutor(CallEvent rootevent) {
			this.rootevent=rootevent;
		}
		
		
		public Object newcall(CallEvent event)  {
			
			events.push(event);
			
			localreport.addCarefully(new LogStartCallEvent(NodeSystem.this,
					event.getNodeProto(), event.getHandler().getName(), event.getArgs(), event.getId()));
			
			Object res=null;
			try {
				 res = event.getHandler().invoke(event.getNodeProto(), event.getArgs());
			}catch(Exception e) {
				throw new IllegalStateException(e);
			}
			
			
			localreport.addCarefully(
					new LogEndCallEvent(NodeSystem.this,
							event.getNodeProto(),  event.getHandler().getName(), event.getArgs(), event.getId(),res)
					);
			
			events.pop();
			return res;
		}
		
		public CallEvent curevent() {
			return events.peek();
		}
		
		@Override
		public void run() {
			try {
				
				sem.acquire();
				current_running=this;
				existing_event_executors.add(this);
				
				beginRootEvent(rootevent);
								
				newcall(rootevent);
								
				endRootEvent(rootevent);
								
				existing_event_executors.remove(this);
				current_running=null;
				
				
			} catch (Exception e) {
				pending_exception=e;
			}finally {			
				master_sem.release();
			}
		}
		
	}
	
	@Override
	public String toString() {
		return idnode().toString();
	}
	
	
	/**
	 * Schedules the execution of an action. 
	 * @param a the action to schedule
	 * @param fornow true if the action must be executed at the current time. In this case
	 * no test is done for the action obsolescence. 
	 */
	public abstract void scheduleAction(Action a, boolean fornow);
	
	/**
	 * Schedule checking of waiting condition. This is usually called 
	 * when a waiting condition with a deadline has been set. 
	 * @param date the checking date
	 */
	public abstract void scheduleWaitingConditionChecking(long date);
	
	
	
	/*
	 * Method executed by one of the node event processor
	 */
	/**
	 * Called by the concrete NodeSystem when one of the event processors 
	 * delivers a new root event. The method creates and starts a new event executor if it is 
	 * {@link CallEvent} instance. In this case it waits until the later ends 
	 * or is blocked by the method {@link NodeSystem#wait()}.
	 * Before ending it call {@link NodeSystem#checkWaitingCondition()}}
	 * @param rootevent the event to deliver to the node
	 */
	public final void processRootEvent(final Event rootevent) {
		try {
			if(rootevent instanceof CallEvent) {
				processRootCallEvent((CallEvent)rootevent);
			}
			checkWaitingCondition();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}					
	}
	
	private static ExecutorService threadpool = Executors.newCachedThreadPool(new ThreadFactory() {
		@Override
		public Thread newThread(Runnable r) {
			Thread res = new Thread(r);
			res.setDaemon(true);
			res.setName("Thread-Pool");
			res.setPriority(Thread.NORM_PRIORITY);
			return res;
		}
	});
	
	private void processRootCallEvent(final CallEvent rootevent) throws InterruptedException {
		EventExecutor eventexecutor = new EventExecutor(rootevent);
		threadpool.execute(eventexecutor);
		eventexecutor.sem.release();
		master_sem.acquire();
		if(pending_exception!=null) {
			throw new IllegalStateException(pending_exception);
		}
	}
	
	
	/**
	 * Called when we want to check if some blocked event processors has
	 * their waiting condition now true. If it is the case, the same principle is applied :
	 * it waits until the unlocked event executor ends or become blocked again by the method
	 * {@link NodeSystem#wait()}.
	 * @throws InterruptedException 
	 */
	private void checkWaitingCondition() throws InterruptedException {
		
		for(EventExecutor executor : existing_event_executors) {
			if(executor.waitingcondition.checked()) {
				executor.sem.release();
				master_sem.acquire();
				if(pending_exception!=null) {
					throw new IllegalStateException(pending_exception);
				}
			}
		}
		
	}
	
}
