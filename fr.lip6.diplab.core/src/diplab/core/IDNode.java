package diplab.core;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
/**
 * An object to identify a node in the system. It is assumed that the system
 * ensures that a node never changes its ID during all the execution. A IDNode is 
 * affected for a unique instance of a system node. 
 * The value of an ID is a non negative long value. The value -1L represents any node
 * in the system. 
 * 
 * @author Jonathan Lejeune (jonathan.lejeune@lip6.fr)
 *
 */
public final class IDNode implements Serializable, Comparable<IDNode>{

	private static final long serialVersionUID = 1L;
	
	private static final Map<Long,IDNode> cache = new HashMap<>();
	
	private final long val;
	
	/**
	 * Return an instance of an IDnode for a given value. 
	 * A caching table is used ensuring to return the same instance
	 * for the same long value. The given value must not be negative.
	 * @param val the long value of the id to return
	 * @return an IDNode instance with the long value
	 * @throws IllegalArgumentException if the parameter is negative
	 */

	public static IDNode get(long val) throws IllegalArgumentException {
		if(val < 0) throw new IllegalArgumentException("Trying to get a negative IDNode");
		if(!cache.containsKey(val)) {
			cache.put(val, new IDNode(val));
		}
		return cache.get(val);
	}
	
	/**
	 * A special IDNode representing any node in the system. Useful for broadcast operations
	 */
	public static final IDNode ANY = new IDNode(-1);
	
	private IDNode(long val) {
		this.val=val;
	}
	
	
	
	/**
	 * A getter for the long value of the id
	 * @return the long value of the id
	 */
	public long getVal() {
		return val;
	}
	
	
	/**
	 * Return a string representation of the IDNode with the format = IDNode(val)
	 * @return the string representation
	 */
	@Override
	public String toString() {
		return "IDNode(" + val + ")";
	}

	/**
     * Returns the hash code value for this IdNode.  The hash code of an idnode 
     * depends directly of the long value.
     * @return the hash code value for this id
 	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (val ^ (val >>> 32));
		return result;
	}

	/**
	 * 
	 * @param obj  the reference object with which to compare.
	 * @return true if the object is an IDnode and have the same long value
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IDNode other = (IDNode) obj;
		if (val != other.val)
			return false;
		return true;
	}



	@Override
	public int compareTo(IDNode o) {
		return Long.compare(val, o.val);
	}


}
