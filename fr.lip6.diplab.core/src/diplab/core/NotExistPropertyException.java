package diplab.core;

public class NotExistPropertyException extends RuntimeException {


	private static final long serialVersionUID = 1L;

	public NotExistPropertyException() {
	}

	public NotExistPropertyException(String message) {
		super(message);
	}

	public NotExistPropertyException(Throwable cause) {
		super(cause);
	}

	public NotExistPropertyException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotExistPropertyException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
