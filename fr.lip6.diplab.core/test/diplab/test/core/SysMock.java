package diplab.test.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import diplab.core.IDNode;
import diplab.core.NodeSystem;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.scenario.action.Action;
import diplab.core.messaging.Message;

public  class SysMock extends NodeSystem{


	static long time=0;
	static Random r = new Random();
	static final List<Message<?>> messages_in_network = new ArrayList<>();
	
	final static List<IDNode> allnodes = new ArrayList<IDNode>();
	
	
	private final IDNode me;
	public SysMock(Experiment expe, IDNode me) {
		super();
		this.me=me;
	}

	@Override
	public void build(Experiment expe) {
		super.build(expe);
	}

	@Override
	public int systemsize() {
		return allnodes.size();
	}

	@Override
	public IDNode idnode() {
		return me;
	}

	@Override
	protected void sendReal(Message<?> message) {
		messages_in_network.add(message);
	}

	
	
	@Override
	public List<IDNode> allnodes() {
		return allnodes;
	}

	@Override
	public Random random() {
		return r;
	}

	@Override
	public long getCurrentTime() {
		return time;
	}

	List<Action> scheduledactions=new ArrayList<>();
	
	@Override
	public void scheduleAction(Action a, boolean fornow) {
		scheduledactions.add(a);
	}

	
	List<Long> datestocheck=new ArrayList<>();
	@Override
	public void scheduleWaitingConditionChecking(long date) {
		datestocheck.add(date);
	}
	
}