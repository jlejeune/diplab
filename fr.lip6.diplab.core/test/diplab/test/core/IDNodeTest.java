package diplab.test.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;

import org.junit.Test;

import diplab.core.IDNode;

public class IDNodeTest {

	
	@Test
	public void test() {
		IDNode id1 = IDNode.get(3);
		IDNode id2 = IDNode.get(2);
		IDNode id3 = IDNode.get(3);
		assertThrows(IllegalArgumentException.class, ()->IDNode.get(-2));
		
		assertEquals(3,id1.getVal());
		assertEquals(3,id3.getVal());
		assertEquals(2,id2.getVal());
		
		assertEquals(id1, id1);
		assertNotEquals(id1, id2);
		assertNotEquals(id1, null);
		assertNotEquals(id1, "3");
		assertEquals(id2, id2);
		assertEquals(id1, id3);
		assertEquals(id3, id3);		
		
		assertTrue(id1==id3);
		assertFalse(id1==id2);
		
		HashSet<IDNode> set = new HashSet<>();
		assertTrue(set.add(id1));
		assertTrue(set.add(id2));
		assertFalse(set.add(id3));
		
	}

}
