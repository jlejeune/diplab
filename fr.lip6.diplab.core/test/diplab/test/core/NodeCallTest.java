package diplab.test.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.Primitive;
import diplab.core.event.PrimitiveCallEvent;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.ExperimentBuilder;
import diplab.core.experiment.scenario.action.Action;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.callscheme.Call;

public class NodeCallTest {

	final Map<SysMock,IDNode> nodes = new HashMap<>();
	final int nb_nodes=5;
		
	@Before
	public void setup() throws InterruptedException {
		Thread.sleep(2);
		SysMock.r=new Random();
		SysMock.time=0;	
		SysMock.messages_in_network.clear();
		SysMock.allnodes.clear();
				
		Map<String,Object> props = new HashMap<>();
		
		ExperimentBuilder builder = new ExperimentBuilder(null, Long.MAX_VALUE);
		builder.addProperties(props);
		builder.addNodeProto("protoup",NodeProtoUp.class);
		builder.addNodeProto("protodown",NodeProtoDown.class);
		
		Experiment experiment= builder.build();
				
		for(int i=0;i< nb_nodes;i++) {
			IDNode cur = IDNode.get(i);
			SysMock.allnodes.add(cur);
			nodes.put(new SysMock(experiment,cur), cur);
		}
								
		for(SysMock ns : nodes.keySet()) {
			ns.build(experiment);
		}
		
	}

	
	public static class NodeProtoUp extends NodeProto{

		public NodeProtoUp(NodeSystem system) {
			super(system);
		}
		String s ="";
		
		@Primitive
		public void sync() {
			call("protodown", "fdown", 5,"Hello");
		}
		
		@Primitive
		public void async() {
			callAsync("protodown", "fdown", 5,"Hello");
		}
		
		@Primitive
		public void callWithProxy() {
			getProxyOfProto(ItfProtoDown.class,"protodown").fdown(5, "Hello");
		}
		
		@Primitive
		public void callWithProxy2() {
			getProxyOfProto(ItfProtoDown.class,"protodown").fdown(10, "Bye");
		}
		
		@Primitive
		public void callWithProxy3() {
			s=getProxyOfProto(ItfProtoDown.class,"protodown").fdown2(3.0);
		}
		
	}
	
	
	public static class NodeProtoDown extends NodeProto implements ItfProtoDown{

		public NodeProtoDown(NodeSystem system) {
			super(system);
		}
		
		Integer i=null;
		String s=null;
		
		@Primitive
		public void fdown(Integer i, String s) {
			this.i=i;
			this.s=s;
		}

		@Primitive
		public String fdown2(Double x) {
			return x+"";
		}
	}
	
	public static interface ItfProtoDown{
		void fdown(Integer i, String s);
		String fdown2(Double x);
	}
	
	
	@Test
	public void testSync() {
		for(SysMock ns : nodes.keySet()) {
			NodeProtoUp nup=null;
			NodeProtoDown ndown=null;
			for(NodeProto n : ns.getNodeProtos()) {
				if(n.getNameProto().equals("protoup")) {
					assertTrue(n instanceof NodeProtoUp);
					nup=(NodeProtoUp)n;
				}
				if(n.getNameProto().equals("protodown")) {
					assertTrue(n instanceof NodeProtoDown);
					ndown=(NodeProtoDown)n;
				}
			}
			assertNotNull(nup);
			assertNotNull(ndown);
			ns.processRootEvent(new PrimitiveCallEvent(nup, "sync", new Object[0]));
			assertEquals(Integer.valueOf(5),ndown.i);
			assertEquals("Hello",ndown.s);
			assertTrue(ns.scheduledactions.isEmpty());
		}

	}
	
	@Test
	public void testAsync() {
		for(SysMock ns : nodes.keySet()) {
			NodeProtoUp nup=null;
			NodeProtoDown ndown=null;
			for(NodeProto n : ns.getNodeProtos()) {
				if(n.getNameProto().equals("protoup")) {
					assertTrue(n instanceof NodeProtoUp);
					nup=(NodeProtoUp)n;
				}
				if(n.getNameProto().equals("protodown")) {
					assertTrue(n instanceof NodeProtoDown);
					ndown=(NodeProtoDown)n;
				}
			}
			assertNotNull(nup);
			assertNotNull(ndown);
			ns.processRootEvent(new PrimitiveCallEvent(nup, "async", new Object[0]));
			assertNull(ndown.i);
			assertNull(ndown.s);
			assertEquals(1,ns.scheduledactions.size());
			Action a = ns.scheduledactions.remove(0);
			assertTrue(a instanceof CallAction);
			CallAction ca = (CallAction)a;
			assertEquals(Long.valueOf(0),ca.getStart().get(SysMock.r));
			assertTrue(ca.containsTarget(ns.idnode()));
			for(SysMock ns2 : nodes.keySet()) {
				if(!ns2.equals(ns)) {
					assertFalse(ca.containsTarget(ns2.idnode()));
				}
			}
			assertTrue(ca.getCallscheme() instanceof Call);
			Call call = (Call)ca.getCallscheme();
			assertEquals("fdown",call.getPrimitiveName());
			assertEquals("protodown",call.getNodeProtoName());
			assertEquals(Long.valueOf(0L),call.getLatency().get(SysMock.r));
			assertEquals(2,call.getArgs().length);
			Object[] args = new Object[call.getArgs().length];
			for(int i=0; i< args.length;i++) {
				args[i]=call.getArgs()[i].get(SysMock.r);
			}
			assertEquals(Integer.valueOf(5),args[0]);
			assertEquals("Hello",args[1]);
							
			ns.processRootEvent(new PrimitiveCallEvent(ndown, call.getPrimitiveName(), args));
						
			assertEquals(Integer.valueOf(5),ndown.i);
			assertEquals("Hello",ndown.s);
		}

	}
	
	@Test
	public void testCallWithProxy() {
		long start,stop;
		for(SysMock ns : nodes.keySet()) {
			NodeProtoUp nup=null;
			NodeProtoDown ndown=null;
			for(NodeProto n : ns.getNodeProtos()) {
				if(n.getNameProto().equals("protoup")) {
					assertTrue(n instanceof NodeProtoUp);
					nup=(NodeProtoUp)n;
				}
				if(n.getNameProto().equals("protodown")) {
					assertTrue(n instanceof NodeProtoDown);
					ndown=(NodeProtoDown)n;
				}
			}
			assertNotNull(nup);
			assertNotNull(ndown);
			
			start = System.nanoTime();
			ns.processRootEvent(new PrimitiveCallEvent(nup, "callWithProxy", new Object[0]));
			stop = System.nanoTime();
			//System.out.println(stop-start);
			assertEquals(Integer.valueOf(5),ndown.i);
			assertEquals("Hello",ndown.s);
			assertTrue(ns.scheduledactions.isEmpty());
			
			start = System.nanoTime();
			ns.processRootEvent(new PrimitiveCallEvent(nup, "callWithProxy2", new Object[0]));
			stop = System.nanoTime();
			//System.out.println(stop-start);
			assertEquals(Integer.valueOf(10),ndown.i);
			assertEquals("Bye",ndown.s);
			assertTrue(ns.scheduledactions.isEmpty());
			
			start = System.nanoTime();
			ns.processRootEvent(new PrimitiveCallEvent(nup, "callWithProxy3", new Object[0]));
			stop = System.nanoTime();
			//System.out.println(stop-start);
			assertEquals("3.0",nup.s);
		}
	}	
}

