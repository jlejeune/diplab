package diplab.test.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Test;

import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.Primitive;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.ExperimentBuilder;
import diplab.core.experiment.ExperimentController;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.scenario.Scenario;

public class ExperimentTest {

	
	public static class DummyExperimentController implements ExperimentController{

		@Override
		public Map<IDNode,Report> execute(Experiment e) {
			Map<IDNode,Report> res = new HashMap<>();
			res.put(IDNode.get(0), e.getReportfactory().createNewReport(e, "DummyReport"));			
			return res; 
		}
	}
	
	public static class NodeProtoImpl extends NodeProto{
		
		public NodeProtoImpl(NodeSystem system) {
			super(system);
		}
		
		@Primitive
		public void f1() {
			
		}
		
		@Primitive
		public void f2() {
			
		}
	}
	
	
	@Test
	public void testExpeBuilding1() throws InterruptedException {
		long experiment_duration = 10000000;
		ExperimentBuilder builder = new ExperimentBuilder(DummyExperimentController.class, experiment_duration);
		long t1 = System.currentTimeMillis();
		Experiment e1  = builder.build();
		long t2=System.currentTimeMillis();
		
		assertEquals(experiment_duration, builder.getExperimentDuration());
		assertEquals(builder.getExperimentDuration(),e1.getExperimentDuration());
		assertEquals(builder.getSystemsize(),e1.getNbNode());
		assertTrue(e1.getDeploymentmap().isEmpty());
		assertEquals(builder.getProperties(),e1.getProperties());
		
		assertTrue(builder.getNodeProtoClasses().isEmpty());
		assertEquals(builder.getNodeProtoClasses(), e1.getNodeProtoClasses());
		
		assertEquals(builder.getSeed(),e1.getSeed());
		assertTrue(e1.getTimestamp()>=t1);
		assertTrue(e1.getTimestamp()<=t2);
		assertTrue(e1.getLogEventSelectors().isEmpty());
		assertTrue(e1.getReportWatchers().isEmpty());
		
		assertEquals(builder.getScenario(),e1.getScenario());
		
		assertNotNull(e1.getReportsdirectory());
		
		assertEquals(DummyExperimentController.class,builder.getExperimentControllerClass());
		assertEquals(DummyExperimentController.class,e1.getExperimentControllerClass());
		
		Thread.sleep(1);
		Experiment e2  = builder.build();
		assertEquals(e1, e1);
		assertEquals(e1.hashCode(), e1.hashCode());
		assertNotEquals(e1, null);
		assertNotEquals(e1.hashCode(), e2.hashCode());
		
		Report r1 = e1.execute().get(IDNode.get(0));
		Report r2 = e2.execute().get(IDNode.get(0));
		
		assertEquals(e1,r1.getExperiment());
		assertEquals(e2,r2.getExperiment());
		
		assertEquals("DummyReport",r1.getName());
		assertEquals("DummyReport",r2.getName());
	}
	
	@Test
	public void testExpeBuilding2() throws InterruptedException, IOException {
		long experiment_duration = 10000000;
		ExperimentBuilder builder = new ExperimentBuilder(DummyExperimentController.class, experiment_duration);
		
		assertThrows(IllegalArgumentException.class, ()-> builder.deployOnly("Toto", new HashSet<>(Arrays.asList(IDNode.get(0)))));
		
		builder.addNodeProto("Toto", NodeProtoImpl.class);
		
		assertThrows(IllegalArgumentException.class, ()-> builder.deployOnly("Toto", new HashSet<>(Arrays.asList(IDNode.get(Long.MAX_VALUE)))));
		assertThrows(IllegalArgumentException.class, ()-> builder.deployOnly("Toto", new HashSet<>(Arrays.asList(IDNode.get(Long.MIN_VALUE)))));
		
		builder.deployOnly("Toto", new HashSet<>(Arrays.asList(IDNode.get(0))));
				
		Scenario sc = new Scenario();
		builder.withScenario(sc);
		assertEquals(sc, builder.getScenario());
		
		
		builder.withoutProgressIndicator();
		Experiment e1  = builder.build();
		Thread.sleep(1);
		
		builder.withSeed(600);
		
		builder.withProgressIndicator();
		Experiment e2  = builder.build();
		Thread.sleep(1);
		
		
//		File outputff = Files.createTempFile("spotrun", ".txt").toFile();
//		if (DEBUG == 0) outputff.deleteOnExit();

		Path tmp = Files.createTempDirectory("TestDiplab");
		builder.withReportDirectory(tmp.toFile());
		
		assertThrows(IllegalStateException.class, ()->builder.build());
		
		builder.clear();
		
		
		
		assertEquals(1,e2.getReportWatchers().size());
		assertEquals(0,e1.getReportWatchers().size());
		assertEquals(600,e2.getSeed());
		assertEquals(builder.getSeed(),e1.getSeed());
	}
	
	
	

}
