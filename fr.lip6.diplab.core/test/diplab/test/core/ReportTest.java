package diplab.test.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.ExperimentBuilder;
import diplab.core.experiment.reporting.Report;
import diplab.core.experiment.reporting.ReportFactory;
import diplab.core.experiment.reporting.ReportInMemory;
import diplab.core.experiment.reporting.chunking.ChunkInMemory;
import diplab.core.experiment.reporting.chunking.ChunkToFile;
import diplab.core.experiment.reporting.chunking.ReportChunked;
import diplab.core.experiment.reporting.logevent.LogEvent;
import diplab.core.experiment.reporting.logevent.LogStartCallEvent;
import diplab.core.experiment.reporting.serializer.RawFileReportSerializer;
import diplab.core.experiment.reporting.visitor.selector.LogEventSelector;
import diplab.test.core.ExperimentTest.DummyExperimentController;

public class ReportTest {

	final Map<SysMock,IDNode> nodes = new HashMap<>();
	final int systemsize=3;
	Experiment experiment;
	final long experiment_duration = 1000000;
	
	public static class NodeProtoMock extends NodeProto{
		
		public NodeProtoMock(NodeSystem system) {
			super(system);
		}


	}
	
	
	@Before
	public void setup() throws InterruptedException {
		
		Thread.sleep(2);
		SysMock.r=new Random();
		SysMock.time=0;	
		SysMock.messages_in_network.clear();
		SysMock.allnodes.clear();
				
			
		
		ExperimentBuilder builder = new ExperimentBuilder(DummyExperimentController.class, Long.MAX_VALUE);
		builder.withSystemSize(systemsize);
		builder.withLogEventSelector(new LogEventSelector(IDNode.get(0)));
		builder.withLogEventSelector(new LogEventSelector(IDNode.get(1)));
		
		builder.addNodeProto("proto", NodeProtoMock.class);
		experiment= builder.build();
		for(int i=0;i< systemsize;i++) {
			IDNode cur = IDNode.get(i);
			SysMock.allnodes.add(cur);
			nodes.put(new SysMock(experiment,cur), cur);
		}
					
		for(SysMock ns : nodes.keySet()) {
			ns.build(experiment);
		}
		
	}
	
	private static final Object[] noargs = new Object[0];
	
	public void test(ReportFactory factory) throws IOException {
		int nb_iter1=50000;
		Report r1 = factory.createNewReport(experiment, "r1");
		
		List<LogEvent> expected1= new ArrayList<>();
		
		
		for(int i=0;i<nb_iter1;i++) {
			for(SysMock s : nodes.keySet()) {
				for(NodeProto proto : s.getNodeProtos()) {
					LogEvent log = new LogStartCallEvent(s, proto, "nameprim", noargs, i);
					SysMock.time++;
					if(!s.idnode().equals(IDNode.get(2))) {
						expected1.add(log);
					}
					r1.addCarefully(log);
				}
			}
		}
		Collections.sort(expected1);
		assertEquals(expected1.size(),r1.size());
		Iterator<LogEvent> expected_it1 = expected1.iterator();
		Iterator<LogEvent> report_it1 = r1.iterator();
		while(report_it1.hasNext() && expected_it1.hasNext()) {
			assertEquals(expected_it1.next(),report_it1.next());
		}
		assertFalse(report_it1.hasNext());
		assertFalse(expected_it1.hasNext());
		
		
		
		int nb_iter2=nb_iter1/2;
		Report r2 = factory.createNewReport(experiment, "r2");
		
		List<LogEvent> expected2= new ArrayList<>();
		
		SysMock.time= SysMock.time /2;
		for(int i=0;i<nb_iter2;i++) {
			for(SysMock s : nodes.keySet()) {
				for(NodeProto proto : s.getNodeProtos()) {
					LogEvent log = new LogStartCallEvent(s, proto, "nameprim", noargs, i);
					SysMock.time+=2;
					if(!s.idnode().equals(IDNode.get(2))) {
						expected2.add(log);
					}
					r2.addCarefully(log);
				}
			}
		}
		Collections.sort(expected2);
		assertEquals(expected2.size(),r2.size());
		
		Iterator<LogEvent> expected_it2 = expected2.iterator();
		Iterator<LogEvent> report_it2 = r2.iterator();
		while(report_it2.hasNext() && expected_it2.hasNext()) {
			assertEquals(expected_it2.next(),report_it2.next());
		}
		assertFalse(report_it2.hasNext());
		assertFalse(expected_it2.hasNext());
		
		

		Report r3 = r1.merge(r2, "merge_r1_r2");
		List<LogEvent> expected3= new ArrayList<>(expected1);
		
		expected3.addAll(expected2);
		Collections.sort(expected3);
		assertEquals(expected3.size(),r3.size());
		Iterator<LogEvent> expected_it3 = expected3.iterator();
		Iterator<LogEvent> report_it3 = r3.iterator();
		while(report_it3.hasNext() && expected_it3.hasNext()) {
			assertEquals(expected_it3.next(),report_it3.next());
		}
		assertFalse(report_it3.hasNext());
		assertFalse(expected_it3.hasNext());
		
		
		Path p = Files.createTempDirectory("testreport");
		
		r3.exportTo(p.toAbsolutePath().toString());
		
		Report loaded = RawFileReportSerializer.instance().load(p.toAbsolutePath().toString()+"/"+r3.getName(), factory);
	
		assertTrue(loaded.isEquivalent(r3));
		
		
		List<LogEvent> reportlist = r3 .toList();
		assertEquals(reportlist.size(), r3.size());
		
		Iterator<LogEvent> itr3 = r3.iterator();
		Iterator<LogEvent> itlist = reportlist.iterator();
		
		while(itr3.hasNext()) {
			assertTrue(itlist.hasNext());
			assertEquals(itr3.next(), itlist.next());
		}
		
	}
	
	
	@Test
	public void testReportInMemory() throws IOException {
		test((e,n)->new ReportInMemory(e, n));
	}
		
	@Test
	public void testReportChunkedInMemory1000() throws IOException {
		test((e,n)->new ReportChunked(e, n, (r)-> new ChunkInMemory(r),1000));
	}
	
	@Test
	public void testReportChunkedToFile1000() throws IOException {
		test((e,n)->new ReportChunked(e, n, (r)-> new ChunkToFile(r),1000));
	}
	
}
