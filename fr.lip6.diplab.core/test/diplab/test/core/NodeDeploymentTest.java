package diplab.test.core;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.ExperimentBuilder;
import diplab.core.messaging.StringMessage;

public class NodeDeploymentTest {

	final Map<SysMock,IDNode> nodes = new HashMap<>();
	final int nb_nodes=5;
	
	@Before
	public void setup() throws InterruptedException {
		Thread.sleep(2);
		SysMock.r=new Random();
		SysMock.time=0;	
		SysMock.messages_in_network.clear();
		SysMock.allnodes.clear();
		
		Map<String,Object> props = new HashMap<>();
		ExperimentBuilder builder = new ExperimentBuilder(null, Long.MAX_VALUE);
		builder.addProperties(props);
		builder.addNodeProto("proto1",NodeProtoMockBis.class);
		builder.addNodeProto("proto2",NodeProtoMockBis.class);
		builder.addNodeProto("proto3",NodeProtoMockBis.class);
		
		Set<IDNode> members_proto1 = new HashSet<>();
		for(int i=0;i<= nb_nodes/2;i++) {
			members_proto1.add(IDNode.get(i));
		}
		Set<IDNode> members_proto2 = new HashSet<>();
		for(int i=nb_nodes/2;i< nb_nodes;i++) {
			members_proto2.add(IDNode.get(i));
		}
		
		builder.deployOnly("proto1",members_proto1);
		builder.deployOnly("proto2",members_proto2);
		Experiment experiment= builder.build();		
		for(int i=0;i< nb_nodes;i++) {
			IDNode cur = IDNode.get(i);
			SysMock.allnodes.add(cur);
			nodes.put(new SysMock(experiment,cur), cur);
		}
						
		
					
		for(SysMock ns : nodes.keySet()) {
			ns.build(experiment);
		}
		
	}

	
	public static class NodeProtoMockBis extends NodeProto{

		public NodeProtoMockBis(NodeSystem system) {
			super(system);
		}
		
	}
	
	
	@Test
	public void test() {
		for(SysMock ns : nodes.keySet()) {
			for(NodeProto n : ns.getNodeProtos()) {
				if(n.getNameProto().equals("proto1")) {
					for(int i=0;i<= nb_nodes/2;i++) {
						assertTrue(n.members().contains(IDNode.get(i)));
					}
					for(int i=(nb_nodes/2) +1;i< nb_nodes;i++) {
						IDNode cur = IDNode.get(i);
						assertFalse(n.members().contains(cur));
						assertThrows(IllegalArgumentException.class, 
								()->n.send(StringMessage.class,cur,""));
					}
				}else if(n.getNameProto().equals("proto2")) {
					for(int i=0;i< nb_nodes/2;i++) {
						IDNode cur = IDNode.get(i);
						assertFalse(n.members().contains(cur));
						assertThrows(IllegalArgumentException.class, 
								()->n.send(StringMessage.class,cur,""));
					}
					for(int i=nb_nodes/2;i< nb_nodes;i++) {
						assertTrue(n.members().contains(IDNode.get(i)));
					}
				}else if(n.getNameProto().equals("proto3")){
					for(int i=0;i< nb_nodes;i++) {
						assertTrue(n.members().contains(IDNode.get(i)));
					}
				}else {
					assertTrue(false);
				}
			}
		}

	}

}
