package diplab.test.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import diplab.core.IDNode;
import diplab.core.NodeProto;
import diplab.core.NodeSystem;
import diplab.core.Primitive;
import diplab.core.Primitive.Type;
import diplab.core.event.MessageReceptionEvent;
import diplab.core.event.PrimitiveCallEvent;
import diplab.core.event.WaitingConditionCheckingEvent;
import diplab.core.experiment.Experiment;
import diplab.core.experiment.ExperimentBuilder;
import diplab.core.experiment.scenario.Value;
import diplab.core.experiment.scenario.action.Action;
import diplab.core.experiment.scenario.action.CallAction;
import diplab.core.experiment.scenario.action.callscheme.Call;
import diplab.core.experiment.scenario.action.callscheme.CallScheme;
import diplab.core.experiment.scenario.action.callscheme.RepeatedCallSequence;
import diplab.core.messaging.IntMessage;
import diplab.core.messaging.Message;
import diplab.core.messaging.MessageHandler;
import diplab.core.messaging.StringMessage;

public class NodeTest {

	final Map<SysMock,IDNode> nodes = new HashMap<>();
	final int nb_nodes=5;
	Experiment experiment;
	
	@Before
	public void setup() throws InterruptedException {
		
		Thread.sleep(2);
		SysMock.r=new Random();
		SysMock.time=0;	
		SysMock.messages_in_network.clear();
		SysMock.allnodes.clear();
				
		
		Map<String,Object> props = new HashMap<>();
		props.put("toto", "hi");
		List<Value<?>> args= new ArrayList<>();
		args.add((r)->3);
		args.add((r)->"x");
		props.put("proto1.lwitharg."+NodeSystem.PAR_LOOP_ARGS,args);
		Value<Long> lat1 = r->10L;
		props.put("proto1.lwitharg."+NodeSystem.PAR_LOOP_SLEEPING_TIME,lat1);
		
		
		props.put("proto2.lwitharg."+NodeSystem.PAR_LOOP_ARGS,args);
		Value<Long> lat2 = r->50L;
		props.put("proto2.lwitharg."+NodeSystem.PAR_LOOP_SLEEPING_TIME,lat2);
		
		ExperimentBuilder builder = new ExperimentBuilder(null, Long.MAX_VALUE);
		builder.addProperties(props);
		builder.addNodeProto("proto1",NodeProtoMock.class);
		builder.addNodeProto("proto2",NodeProtoMock.class);
		experiment= builder.build();
		for(int i=0;i< nb_nodes;i++) {
			IDNode cur = IDNode.get(i);
			SysMock.allnodes.add(cur);
			nodes.put(new SysMock(experiment,cur), cur);
		}
					
		for(SysMock ns : nodes.keySet()) {
			ns.build(experiment);
			
		}
		
	}
	
	
	@Test
	public void testGet() {
		for(SysMock ns : nodes.keySet()) {
			SysMock.time++;
			for(NodeProto n : ns.getNodeProtos()) {
				assertEquals(SysMock.time,n.getCurrentTime());
				assertTrue(experiment.getNodeProtoClasses().containsKey(n.getNameProto()));	
				assertEquals(experiment.getNodeProtoClasses().get(n.getNameProto()),  n.getClass());
				assertEquals(nb_nodes, n.systemsize());
				assertEquals(ns.idnode(),n.idnode());
				assertEquals(nodes.size(),n.allnodes().size());
				for(IDNode id : n.allnodes()) {
					assertTrue(nodes.values().contains(id));
				}
				
				assertEquals("hi",n.getExperimentProperty("toto"));
				assertNull(n.getExperimentProperty("titi"));
			}	
		}
	}

	@Test
	public void testInit() {
		for(SysMock ns : nodes.keySet()) {
			for(NodeProto n : ns.getNodeProtos()) {
				n.init();
				if(n instanceof NodeProtoMock) {
					assertTrue(((NodeProtoMock)n).initiated);
					
				}
				else {
					assertFalse(true);
				}
			}
		}
	}
	
	
	@Test
	public void testSend() {
		for(SysMock ns : nodes.keySet()) {
			for(NodeProto n : ns.getNodeProtos()) {
				SysMock.messages_in_network.clear();
				ns.processRootEvent(new PrimitiveCallEvent(n, "f", new Object[0]));							
				assertEquals(1,SysMock.messages_in_network.size());
				Message<?> m = SysMock.messages_in_network.get(0);
				assertEquals(m.getDest(), IDNode.get(0));
				assertEquals(StringMessage.class, m.getClass());
				assertEquals(m.getSrc(), n.idnode());
				assertEquals(m.getContent(),"Hello");				
			}
		}
	}
	
	
	@Test
	public void testCallEvent() {
		for(SysMock ns : nodes.keySet()) {
			for(NodeProto n : ns.getNodeProtos()) {
				ns.processRootEvent(new PrimitiveCallEvent(n, "f", new Object[0]));
				assertThrows(IllegalArgumentException.class, ()->ns.processRootEvent(new PrimitiveCallEvent(n, "g", new Object[0])));
				ns.processRootEvent(new PrimitiveCallEvent(n, "f", new Object[] {"hello"}));
				ns.processRootEvent(new PrimitiveCallEvent(n, "f", new Object[] {new BigInteger("3")}));
				assertThrows(IllegalArgumentException.class, ()->ns.processRootEvent(new PrimitiveCallEvent(n, "f", new Object[] {new Object()})));
			}
		}
	}
	
	@Test
	public void testInnerCall() {
		for(SysMock ns : nodes.keySet()) {
			for(NodeProto n : ns.getNodeProtos()) {
				SysMock.messages_in_network.clear();
				if(n.getNameProto().equals("proto1")) {
					int value=500;
					ns.processRootEvent(new PrimitiveCallEvent(n, "i", new Object[] {value}));
					assertEquals(1,SysMock.messages_in_network.size());
					Message<?> m = SysMock.messages_in_network.get(0);
					assertEquals(m.getDest(), IDNode.get(0));
					assertEquals(StringMessage.class, m.getClass());
					assertEquals(m.getSrc(), n.idnode());
					assertEquals(m.getContent(),"Hello");			
					assertEquals("proto2",m.getNameProto());
					
					NodeProtoMock npm = (NodeProtoMock)ns.getNodeProto("proto2");
					assertEquals(1,npm.received_messages.size());
					Message<?> m2 = npm.received_messages.get(0);
					assertEquals(npm.idnode(),m2.getDest());
					assertEquals(IntMessage.class, m2.getClass());
					assertEquals(npm.idnode(),m2.getSrc());
					assertEquals(m2.getContent(),value);			
					assertEquals("proto2",m2.getNameProto());

				}
				
			}
		}
	}
	
	@Test
	public void testReceive() {
		for(SysMock ns : nodes.keySet()) {
			for(NodeProto n : ns.getNodeProtos()) {
				NodeProtoMock npm = (NodeProtoMock) n;
				IntMessage im  = new IntMessage(IDNode.get(0), n.idnode(), n.getNameProto(), 1000, 50);
				ns.processRootEvent(new MessageReceptionEvent(im, ns, n));
				assertThrows(IllegalArgumentException.class, ()->ns.processRootEvent(new MessageReceptionEvent(new StringMessage(IDNode.get(0), n.idnode(), n.getNameProto(), 2000, "hi"), ns, n)));
				assertTrue(npm.received_messages.contains(im));
			}
		}
	}
		
	
	@Test
	public void testWait() {
		for(SysMock ns : nodes.keySet()) {
			for(NodeProto n : ns.getNodeProtos()) {
				if(n.getNameProto().equals("proto1")) {
					NodeProtoMock npm1 = (NodeProtoMock)n;
					NodeProtoMock npm2 = (NodeProtoMock)ns.getNodeProto("proto2");
					assertFalse(npm1.step1);
					assertFalse(npm2.step1);
					assertFalse(npm1.step2);
					assertFalse(npm2.step2);
					ns.processRootEvent(new PrimitiveCallEvent(n, "j", new Object[] {}));
					assertFalse(npm1.step1);
					assertFalse(npm2.step1);
					assertFalse(npm1.step2);
					assertFalse(npm2.step2);
					ns.processRootEvent(new PrimitiveCallEvent(npm1, "setVal", new Object[] {40}));
					assertFalse(npm1.step1);
					assertFalse(npm2.step1);
					assertFalse(npm1.step2);
					assertFalse(npm2.step2);
					ns.processRootEvent(new PrimitiveCallEvent(npm1, "setVal", new Object[] {100}));
					assertFalse(npm1.step1);
					assertFalse(npm2.step1);
					assertFalse(npm1.step2);
					assertFalse(npm2.step2);
					ns.processRootEvent(new PrimitiveCallEvent(npm2, "setVal", new Object[] {100}));
					assertTrue(npm1.step1);
					assertFalse(npm2.step1);
					assertFalse(npm1.step2);
					assertFalse(npm2.step2);
					ns.processRootEvent(new PrimitiveCallEvent(npm2, "setVal", new Object[] {500}));
					assertTrue(npm1.step1);
					assertFalse(npm2.step1);
					assertFalse(npm1.step2);
					assertFalse(npm2.step2);
					ns.processRootEvent(new PrimitiveCallEvent(npm1, "setVal", new Object[] {600}));
					assertTrue(npm1.step1);
					assertFalse(npm2.step1);
					assertTrue(npm1.step2);
					assertFalse(npm2.step2);
				}
				
			}
		}
	}
	
	
	@Test
	public void testWrongDest() {
		for(SysMock ns : nodes.keySet()) {
			for(NodeProto n : ns.getNodeProtos()) {
				assertThrows(IllegalStateException.class, ()->ns.processRootEvent(new PrimitiveCallEvent(n, "k", new Object[] {})));
			}
		}
	}
	
	
	@Test
	public void testLoop() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		for(SysMock ns : nodes.keySet()) {
			assertEquals(4,ns.scheduledactions.size());	
			for(Action a : ns.scheduledactions) {
				assertTrue(a.containsTarget(ns.idnode()));
				assertEquals(Long.valueOf(1),a.getStart().get(ns.random()));
				assertTrue(a instanceof CallAction);
				CallAction ca = (CallAction)a;
				CallScheme scheme = ca.getCallscheme();
				assertTrue(scheme instanceof RepeatedCallSequence);
				Call call = scheme.iterator().next();
				
				List<Class<?>> class_args = new ArrayList<>();
				List<Object> args = new ArrayList<>();
				for(Value<?> v : call.getArgs()) {
					Object arg = v.get(ns.random());
					args.add(arg);
					class_args.add(arg.getClass());
				}
				NodeProto n = ns.getNodeProto(call.getNodeProtoName());
				Object res = n .getClass()
							   .getMethod(call.getPrimitiveName(), class_args.toArray(new Class<?>[0]))
							   .invoke(n, args.toArray());
				if(call.getPrimitiveName().equals("l")) {
					assertEquals(5, res);
				}else if(call.getPrimitiveName().equals("lwitharg")) {
					assertEquals("3.x", res);
					if(n.getNameProto().equals("proto1")) {
						assertEquals(Long.valueOf(10L), call.getLatency().get(ns.random()));
					}else {
						assertEquals(Long.valueOf(50L), call.getLatency().get(ns.random()));
					}
				}else {
					assertTrue(false);
				}
			}
		}
	}
	
	@Test
	public void testSleep() {
		SysMock.time=0;				
		for(SysMock ns : nodes.keySet()) {
			for(NodeProto n : ns.getNodeProtos()) {
				NodeProtoMock npm = (NodeProtoMock)n;
				int min=10;
				int max=50;
				long delay =  SysMock.r.nextInt(max-min)+min;
				assertFalse(npm.aftersleep);
				assertFalse(npm.aftersleepcond);
				assertNull(npm.res);
				assertEquals(0, ns.datestocheck.size());
				ns.processRootEvent(new PrimitiveCallEvent(n, "fsleep", new Object[] {delay}));
				assertEquals(1, ns.datestocheck.size());
				long deadline=ns.datestocheck.get(0);
				assertEquals(delay+SysMock.time,deadline);
				while(SysMock.time<deadline) {
					assertFalse(npm.aftersleep);
					assertFalse(npm.aftersleepcond);
					assertNull(npm.res);
					ns.processRootEvent(WaitingConditionCheckingEvent.get());
					SysMock.time++;
					assertFalse(npm.aftersleep);
					assertFalse(npm.aftersleepcond);
					assertNull(npm.res);
				}
				ns.processRootEvent(WaitingConditionCheckingEvent.get());
				while(SysMock.time<deadline/2) {
					assertTrue(npm.aftersleep);
					assertFalse(npm.aftersleepcond);
					assertNull(npm.res);
					ns.processRootEvent(WaitingConditionCheckingEvent.get());
					SysMock.time++;
					assertTrue(npm.aftersleep);
					assertFalse(npm.aftersleepcond);
					assertNull(npm.res);
				}
				npm.cond=true;
				ns.processRootEvent(WaitingConditionCheckingEvent.get());
				assertTrue(npm.aftersleep);
				assertTrue(npm.aftersleepcond);
				assertNotNull(npm.res);
				assertEquals(Boolean.FALSE,npm.res);
				
				ns.datestocheck.clear();
				
			}
		}
	}
	
	
	public static class NodeProtoMock extends NodeProto{

		final List<Message<?>> received_messages = new ArrayList<>();
		boolean initiated=false;
		
		public NodeProtoMock(NodeSystem system) {
			super(system);
		}

		@Override
		public void init() {
			super.init();
			initiated=true;
		}
		
		@Primitive
		public void f() {
			send(StringMessage.class, IDNode.get(0), "Hello");
		}
		
		
		@Primitive
		public BigInteger f(BigInteger a) {
			return a;
		}
		
		
		String my_string="";
		@Primitive
		public String f(String a) {
			my_string=a;
			return a;
		}
				
		public void g() {}
		
		
		@MessageHandler(IntMessage.class)
		public void h(IntMessage m) {
			received_messages.add(m);
		}
		
		@Primitive
		public void i(Integer x) {
			if(getNameProto().equals("proto1")) {
				call("proto2", "f", new Object[0]);
				deliverMessage("proto2", new IntMessage(idnode(), idnode(), "proto2", 60, x));
			}
		}
		
		
		
		int val=0;
		@Primitive
		public void setVal(Integer val) {
			this.val=val;
		}
		
		boolean step1=false;
		boolean step2=false;
		@Primitive
		public void j() {
			if(getNameProto().equals("proto1")) {
				call("proto2", "ensureValEquals", 100);
			}
			step1=true;
			ensureValGreatThan(500);
			step2=true;
		}
		
		@Primitive
		public void ensureValEquals(Integer x) {
			wait(()-> val==x);
		}
		
		@Primitive
		public void ensureValGreatThan(Integer x) {
			wait(()-> val>x);
		}
		
		
		@Primitive
		public void k() {
			send(StringMessage.class, IDNode.get(Long.MAX_VALUE), "Hello");
		}
		
				
		@Primitive(type=Type.LOOP)
		public Integer l() {
			return 5;
		}
		
		@Primitive(type=Type.LOOP)
		public String lwitharg(Integer a, String s) {
			return a+"."+s;
		}
		
		boolean aftersleep=false;
		boolean aftersleepcond=false;
		boolean cond=false;
		Object res=null;
		@Primitive
		public void fsleep(Long delay) {
			sleep(delay);
			aftersleep=true;
			res = wait(()->cond,delay);
			aftersleepcond=true;
		}
		
	}
	
	
		
	
}
