package diplab.test.core;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import org.junit.BeforeClass;
import org.junit.Test;

import diplab.core.experiment.scenario.action.callscheme.Call;
import diplab.core.experiment.scenario.action.callscheme.CallScheme;
import diplab.core.experiment.scenario.action.callscheme.CallSequence;
import diplab.core.experiment.scenario.action.callscheme.RepeatedCallSequence;

public class CallSchemeTest {

	static Call[] calls = new Call[10];
	
	private static final String proto="proto";
	
	private static final String getCall(int i) {
		return "c"+i;
	}
	
	@BeforeClass
	public static void setupclass() {
		for(int i=0;i < calls.length ; i++) {
			int tmp=i;
			calls[i]=new Call(proto,getCall(i), (r)-> Long.valueOf(tmp));
		}
	}
	
	
	
	@Test
	public void testGetter() {
		
		for(int i=0;i < calls.length ; i++) {
			
			Call c= calls[i];
			assertEquals(proto, c.getNodeProtoName());
			assertEquals(getCall(i), c.getPrimitiveName());
			assertEquals(0,c.getArgs().length);
			assertEquals(Long.valueOf(i),c.getLatency().get(null));
		}
		
	}
	
	
	@Test
	public void testsingle() {
		CallScheme scheme = calls[0];
		Iterator<Call> it = scheme.iterator();
		
		assertTrue(it.hasNext());
		assertEquals(calls[0], it.next());
		assertFalse(it.hasNext());
		
	}

	
	@Test
	public void testsequence() {
		
		CallSequence callsequence= new CallSequence();
		for(int i=0;i<calls.length;i++) {
			callsequence.addCallScheme(calls[i]);
		}
		CallScheme scheme = callsequence;
		Iterator<Call> it = scheme.iterator();
		for(int i=0;i<calls.length;i++) {
			assertTrue(it.hasNext());
			assertEquals(calls[i], it.next());
		}
				
		assertFalse(it.hasNext());
	}
	
	@Test
	public void testnestedsequence() {
		
		CallSequence callsequence= new CallSequence();
		for(int i=0;i<calls.length;i++) {
			CallSequence seqi = new CallSequence();
			callsequence.addCallScheme(seqi);
			for(int j=i;j<calls.length;j++) {
				seqi.addCallScheme(calls[j]);
			}			
		}
		for(int i=0;i<calls.length;i++) {
			callsequence.addCallScheme(calls[i]);
		}
		CallScheme scheme = callsequence;
		
		
		Iterator<Call> it = scheme.iterator();
		for(int i=0;i<calls.length;i++) {
			for(int j=i;j<calls.length;j++) {
				assertTrue(it.hasNext());
				assertEquals(calls[j], it.next());
			}			
		}
		for(int i=0;i<calls.length;i++) {
			assertTrue(it.hasNext());
			assertEquals(calls[i], it.next());
		}		
		assertFalse(it.hasNext());
	}
	
	@Test
	public void testrepeated() {
		int nb_loop=50;
		RepeatedCallSequence callsequence= new RepeatedCallSequence(nb_loop);
		assertEquals(nb_loop, callsequence.getNb());
				
		for(int i=0;i<calls.length;i++) {
			CallSequence seqi = new CallSequence();
			callsequence.addCallScheme(seqi);
			for(int j=i;j<calls.length;j++) {
				seqi.addCallScheme(calls[j]);
			}			
		}
		for(int i=0;i<calls.length;i++) {
			callsequence.addCallScheme(calls[i]);
		}
		CallScheme scheme = callsequence;
		
		Iterator<Call> it = scheme.iterator();
		
		for(int k=0;k< nb_loop;k++) {
			for(int i=0;i<calls.length;i++) {
				for(int j=i;j<calls.length;j++) {
					assertTrue(it.hasNext());
					assertEquals(calls[j], it.next());
				}			
			}
			for(int i=0;i<calls.length;i++) {
				assertTrue(it.hasNext());
				assertEquals(calls[i], it.next());
			}	
		}
		assertFalse(it.hasNext());
		
		RepeatedCallSequence callsequence2= new RepeatedCallSequence();
		assertEquals(Integer.MAX_VALUE, callsequence2.getNb());
	}
	
}
